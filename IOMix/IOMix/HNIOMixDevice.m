//
//  HNIOMixDevice.m
//  IOMix
//
//  Created by YuHeng_Antony on 11/9/15.
//  Copyright © 2015 Homni Enterprises Co., Ltd. All rights reserved.
//

#import "HNIOMixDevice.h"
#import "GCDAsyncSocket.h"
#import "HNBaseCommand.h"
#import "HNChannelStore.h"
#import "HNChannel.h"

/**
 *  滑动slider时发送指令的时间间隔(200ms发送一次)
 */
static float intervalTimeForSlider = 0.2;

// TODO:需要和轩工确认此间隔时间硬件是否能收到两条指令
/**
 *  SYNC状态下发送第二条指令的间隔时间
 */
static float delayTimeForSyncCommand = 0.15;

@interface HNIOMixDevice()<GCDAsyncSocketDelegate>

@property (nonatomic, strong)GCDAsyncSocket *tcpSocket;
@property (nonatomic) dispatch_queue_t socketDelegateQueue;
@property (nonatomic, strong) NSTimer *sendTimer;
@property (nonatomic) BOOL isSendState;
@property (nonatomic) BOOL hadSendCommand;

@end

@implementation HNIOMixDevice

+ (instancetype)deviceWithIP:(NSString *)ip {
    HNIOMixDevice *device = [[HNIOMixDevice alloc] init];
    device.targetIp = [ip copy];
    
    // 链接tcp
    NSError *error = nil;
    if (![device.tcpSocket connectToHost:device.targetIp  onPort:HN_TCP_COMMAND_PORT error:&error]) {
        NSLog(@"Connect is failed: %@", [error localizedDescription]);
    }
    return device;
}

- (instancetype)init {
    self = [super init];
    
    if (self) {
        self.socketDelegateQueue = dispatch_queue_create("com.yuheng.iomix.device", NULL);
        _tcpSocket = [[GCDAsyncSocket alloc] initWithDelegate:self delegateQueue:self.socketDelegateQueue socketQueue:dispatch_get_main_queue()];
        _sendTimer = [NSTimer scheduledTimerWithTimeInterval:intervalTimeForSlider target:self selector:@selector(changeSendState) userInfo:nil repeats:YES];
    }
    return self;
}

- (void)changeSendState {
    if (_isSendState) {
        _isSendState = NO;
        _hadSendCommand = NO;
    } else {
        _isSendState = YES;
    }
}

#pragma mark - 手动断开链接
- (void)disconnect {
    [_tcpSocket disconnect];
}

#pragma mark - Command Implementation
#pragma mark Monitor页面
- (void)setupOutputChannel:(NSInteger)outputChannelIndex inputChannel:(NSInteger)inputCannelIndex {
    M2DControlOutputChannelCommand cmd;
    cmd.cmd = HN_CONTROL_INPUT_CHANNEL;
    cmd.outputChannelIndex = outputChannelIndex;
    cmd.inputChannelIndex = inputCannelIndex;
    cmd.reserved = 0;
    cmd.checksum = HNChecksumChar55;
    [self execute:[NSData dataWithBytes:&cmd length:sizeof(M2DControlOutputChannelCommand)]];
    NSLog(@"发送了0x%x指令:outputChannel的索引是%ld, inputChannel的索引是%ld", cmd.cmd, (long)cmd.outputChannelIndex, (long)cmd.inputChannelIndex);
}

#pragma mark Home页面
- (void)setupVolumeForChannel:(NSUInteger)channelIndex volumeValue:(NSInteger)volumeValue immediately:(BOOL)immediately {
    if (immediately) {
        // 即刻发送(用于手指离开slider后最后一次发送)
        M2DControlVolumeCommand cmd;
        cmd.cmd = HN_CONTROL_VOLUME;
        cmd.channelIndex = channelIndex;
        cmd.volumeValue = volumeValue;
        cmd.reserved = 0;
        cmd.checksum = HNChecksumChar55;
        [self execute:[NSData dataWithBytes:&cmd length:sizeof(M2DControlVolumeCommand)]];
        NSLog(@"发送了_即时/无延时_0x%x指令:给Channel%lu发送了音量:%d", cmd.cmd, (unsigned long)cmd.channelIndex, cmd.volumeValue);
    } else {
        if (_isSendState && !_hadSendCommand) {
            M2DControlVolumeCommand cmd;
            cmd.cmd = HN_CONTROL_VOLUME;
            cmd.channelIndex = channelIndex;
            cmd.volumeValue = volumeValue;
            cmd.reserved = 0;
            cmd.checksum = HNChecksumChar55;
            [self execute:[NSData dataWithBytes:&cmd length:sizeof(M2DControlVolumeCommand)]];
            _hadSendCommand = YES;
            NSLog(@"发送了0x%x指令:给Channel%lu发送了音量:%d", cmd.cmd, (unsigned long)cmd.channelIndex, cmd.volumeValue);
        }
    }

}

// slider在sync状态下的指令方法
- (void)setupSyncVolumeForChannelA:(NSInteger)channelA channelB:(NSInteger)channelB volumeValue:(NSInteger)volumeValue immediately:(BOOL)immediately {
    if (immediately) {
        // 即刻发送(用于手指离开slider后最后一次发送)
        M2DControlVolumeCommand cmd;
        cmd.cmd = HN_CONTROL_VOLUME;
        cmd.channelIndex = channelA;
        cmd.volumeValue = volumeValue;
        cmd.reserved = 0;
        cmd.checksum = HNChecksumChar55;
        [self execute:[NSData dataWithBytes:&cmd length:sizeof(M2DControlVolumeCommand)]];
        NSLog(@"发送了_即时/无延时_0x%x指令:给Channel%lu发送了音量:%d", cmd.cmd, (unsigned long)cmd.channelIndex, cmd.volumeValue);
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayTimeForSyncCommand * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            M2DControlVolumeCommand cmd;
            cmd.cmd = HN_CONTROL_VOLUME;
            cmd.channelIndex = channelB;
            cmd.volumeValue = volumeValue;
            cmd.reserved = 0;
            cmd.checksum = HNChecksumChar55;
            [self execute:[NSData dataWithBytes:&cmd length:sizeof(M2DControlVolumeCommand)]];
            NSLog(@"发送了_即时/无延时_0x%x指令:给Channel%lu发送了音量:%d", cmd.cmd, (unsigned long)cmd.channelIndex, cmd.volumeValue);
        });

    } else {
        if (_isSendState && !_hadSendCommand) {
            M2DControlVolumeCommand cmd;
            cmd.cmd = HN_CONTROL_VOLUME;
            cmd.channelIndex = channelA;
            cmd.volumeValue = volumeValue;
            cmd.reserved = 0;
            cmd.checksum = HNChecksumChar55;
            [self execute:[NSData dataWithBytes:&cmd length:sizeof(M2DControlVolumeCommand)]];
            _hadSendCommand = YES;
            NSLog(@"发送了0x%x指令:给Channel%lu发送了音量:%d", cmd.cmd, (unsigned long)cmd.channelIndex, cmd.volumeValue);
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayTimeForSyncCommand * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                M2DControlVolumeCommand cmd;
                cmd.cmd = HN_CONTROL_VOLUME;
                cmd.channelIndex = channelB;
                cmd.volumeValue = volumeValue;
                cmd.reserved = 0;
                cmd.checksum = HNChecksumChar55;
                [self execute:[NSData dataWithBytes:&cmd length:sizeof(M2DControlVolumeCommand)]];
                _hadSendCommand = YES;
                NSLog(@"发送了0x%x指令:给Channel%lu发送了音量:%d", cmd.cmd, (unsigned long)cmd.channelIndex, cmd.volumeValue);
            });
        }
    }
}

// MUTE在非sync状态下的指令方法(MUTE下没有延时，所以单独写)
- (void)setupNonDelayVolumeForChannel:(NSUInteger)channelIndex volumeValue:(NSInteger)volumeValue {
    M2DControlVolumeCommand cmd;
    cmd.cmd = HN_CONTROL_VOLUME;
    cmd.channelIndex = channelIndex;
    cmd.volumeValue = volumeValue;
    cmd.reserved = 0;
    cmd.checksum = HNChecksumChar55;
    [self execute:[NSData dataWithBytes:&cmd length:sizeof(M2DControlVolumeCommand)]];
    NSLog(@"发送了0x%x指令:给Channel%lu发送了音量:%d", cmd.cmd, (unsigned long)cmd.channelIndex, cmd.volumeValue);
}

// MUTE在sync状态下的指令方法
- (void)setupNonDelayVolumeForChannelA:(NSUInteger)channelA channelB:(NSInteger)channelB volumeValue:(NSInteger)volumeValue {
    M2DControlVolumeCommand cmd;
    cmd.cmd = HN_CONTROL_VOLUME;
    cmd.channelIndex = channelA;
    cmd.volumeValue = volumeValue;
    cmd.reserved = 0;
    cmd.checksum = HNChecksumChar55;
    [self execute:[NSData dataWithBytes:&cmd length:sizeof(M2DControlVolumeCommand)]];
    NSLog(@"发送了0x%x指令:给Channel%lu发送了音量:%d", cmd.cmd, (unsigned long)cmd.channelIndex, cmd.volumeValue);
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayTimeForSyncCommand * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        M2DControlVolumeCommand cmd;
        cmd.cmd = HN_CONTROL_VOLUME;
        cmd.channelIndex = channelB;
        cmd.volumeValue = volumeValue;
        cmd.reserved = 0;
        cmd.checksum = HNChecksumChar55;
        [self execute:[NSData dataWithBytes:&cmd length:sizeof(M2DControlVolumeCommand)]];
        NSLog(@"发送了0x%x指令:给Channel%lu发送了音量:%d", cmd.cmd, (unsigned long)cmd.channelIndex, cmd.volumeValue);
    });
}

#pragma mark EQ页面
- (void)setupEQForChannel:(NSUInteger)channelIndex frequencyIndex:(NSUInteger)frequencyIndex gainValue:(NSInteger)gainValue immediately:(BOOL)immediately {
    if (immediately) {
        // 即刻发送(用于手指离开slider后最后一次发送)
        M2DControlEQCommand cmd;
        cmd.cmd = HN_CONTROL_EQ;
        cmd.channelIndex = channelIndex;
        cmd.frequencyIndex = frequencyIndex;
        cmd.gainValue = gainValue;
        cmd.reserved = 0;
        cmd.checksum = HNChecksumChar55;
        [self execute:[NSData dataWithBytes:&cmd length:sizeof(M2DControlEQCommand)]];
        NSLog(@"发送了_即时/无延时_0x%x指令:给Channel%lu的第%lu个slider发送了EQ设置值:%ld", cmd.cmd, (unsigned long)cmd.channelIndex, (unsigned long)cmd.frequencyIndex, (long)cmd.gainValue);
    } else {
        if (_isSendState && !_hadSendCommand) {
            M2DControlEQCommand cmd;
            cmd.cmd = HN_CONTROL_EQ;
            cmd.channelIndex = channelIndex;
            cmd.frequencyIndex = frequencyIndex;
            cmd.gainValue = gainValue;
            cmd.reserved = 0;
            cmd.checksum = HNChecksumChar55;
            [self execute:[NSData dataWithBytes:&cmd length:sizeof(M2DControlEQCommand)]];
            _hadSendCommand = YES;
            NSLog(@"发送了0x%x指令:给Channel%lu的第%lu个slider发送了EQ设置值:%ld", cmd.cmd, (unsigned long)cmd.channelIndex, (unsigned long)cmd.frequencyIndex, (long)cmd.gainValue);
        }
    }
}

- (void)setupEffectForChannel:(NSUInteger)channelIndex effectIndex:(NSUInteger)effectIndex {
    M2DControlEffectCommand cmd;
    cmd.cmd = HN_CONTROL_EFFECT;
    cmd.channelIndex = channelIndex;
    cmd.effectIndex = effectIndex;
    cmd.reserved = 0;
    cmd.checksum = HNChecksumChar55;
    [self execute:[NSData dataWithBytes:&cmd length:sizeof(M2DControlEffectCommand)]];
    NSLog(@"发送了0x%x指令:给Channel%lu的选择了第%lu个预设音效", cmd.cmd, (unsigned long)cmd.channelIndex, (unsigned long)cmd.effectIndex);
}

#pragma mark Fileter页面
- (void)setupBassForChannel:(NSInteger)channelIndex bassGainValue:(NSInteger)bassGainValue {
    if (_isSendState && !_hadSendCommand) {
        M2DControlBassCommand cmd;
        cmd.cmd = HN_CONTROL_BASS;
        cmd.channelIndex = channelIndex;
        cmd.bassGainValue = bassGainValue + 12;// command定义值范围为0-24
        cmd.reserved = 0;
        cmd.checksum = HNChecksumChar55;
        [self execute:[NSData dataWithBytes:&cmd length:sizeof(M2DControlBassCommand)]];
        _hadSendCommand = YES;
        NSLog(@"发送了0x%x指令:给Channel%lu发送了低音设置指令,值为%lu", cmd.cmd, (unsigned long)cmd.channelIndex, (unsigned long)cmd.bassGainValue);
    }
}

- (void)setupTrebleForChannel:(NSInteger)channelIndex trebleGainValue:(NSInteger)trebleGainValue {
    if (_isSendState && !_hadSendCommand) {
        M2DControlTrebleCommand cmd;
        cmd.cmd = HN_CONTROL_TREBLE;
        cmd.channelIndex = channelIndex;
        cmd.trebleGainValue = trebleGainValue + 12;
        cmd.reserved = 0;
        cmd.checksum = HNChecksumChar55;
        [self execute:[NSData dataWithBytes:&cmd length:sizeof(M2DControlTrebleCommand)]];
        _hadSendCommand = YES;
        NSLog(@"发送了0x%x指令:给Channel%lu发送了高音设置指令,值为%lu", cmd.cmd, (unsigned long)cmd.channelIndex, (unsigned long)cmd.trebleGainValue);
    }
}

- (void)setupHighPassForChannel:(NSInteger)channelIndex filterSubtypeMode:(NSInteger)filterSubtypeMode frequencyValue:(NSInteger)frequencyValue immediately:(BOOL)immediately {
    if (immediately) {
        // 两种马上发送的情况
        if (frequencyValue == 199) {
            // 点击了allPass按钮，直接发送，不需要等200ms
            M2DControlHighPassCommand cmd;
            cmd.cmd = HN_CONTROL_HIGH_PASS;
            cmd.channelIndex = channelIndex;
            cmd.filterSubtypeMode = filterSubtypeMode;
            cmd.highPassFrequencyValue = 199;
            cmd.reserved = 0;
            cmd.checksum = HNChecksumChar55;
            [self execute:[NSData dataWithBytes:&cmd length:sizeof(M2DControlHighPassCommand)]];
            _hadSendCommand = YES;
            NSLog(@"发送了_即时_0x%x指令:给Channel%lu发送了高通设置指令,Subtype是%d, 值为%lu", cmd.cmd, (unsigned long)cmd.channelIndex, cmd.filterSubtypeMode, (unsigned long)cmd.highPassFrequencyValue);
        } else {
            // 切换了自类型，马上发送
            NSInteger commandValue = [self transformValue:frequencyValue];
            M2DControlHighPassCommand cmd;
            cmd.cmd = HN_CONTROL_HIGH_PASS;
            cmd.channelIndex = channelIndex;
            cmd.filterSubtypeMode = filterSubtypeMode;
            cmd.highPassFrequencyValue = commandValue;
            cmd.reserved = 0;
            cmd.checksum = HNChecksumChar55;
            [self execute:[NSData dataWithBytes:&cmd length:sizeof(M2DControlHighPassCommand)]];
            _hadSendCommand = YES;
            NSLog(@"发送了_即时_0x%x指令:给Channel%lu发送了高通设置指令,Subtype是%d, 值为%lu", cmd.cmd, (unsigned long)cmd.channelIndex, cmd.filterSubtypeMode, (unsigned long)cmd.highPassFrequencyValue);
        }
    } else {
        // 滑动slider，需要设置200ms发送一次
        NSInteger commandValue = [self transformValue:frequencyValue];
        if (_isSendState && !_hadSendCommand) {
            M2DControlHighPassCommand cmd;
            cmd.cmd = HN_CONTROL_HIGH_PASS;
            cmd.channelIndex = channelIndex;
            cmd.filterSubtypeMode = filterSubtypeMode;
            cmd.highPassFrequencyValue = commandValue;
            cmd.reserved = 0;
            cmd.checksum = HNChecksumChar55;
            [self execute:[NSData dataWithBytes:&cmd length:sizeof(M2DControlHighPassCommand)]];
            NSLog(@"发送了0x%x指令:给Channel%lu发送了高通设置指令,Subtype是%d, 值为%lu", cmd.cmd, (unsigned long)cmd.channelIndex, cmd.filterSubtypeMode, (unsigned long)cmd.highPassFrequencyValue);
        }
    }
}

- (void)setupLowPassForChannel:(NSInteger)channelIndex filterSubtypeMode:(NSInteger)filterSubtypeMode frequencyValue:(NSInteger)frequencyValue immediately:(BOOL)immediately {
    if (immediately) {
        if (frequencyValue == 199) {
            // 点击了allPass按钮，直接发送，不需要等200ms
            M2DControlLowPassCommand cmd;
            cmd.cmd = HN_CONTROL_LOW_PASS;
            cmd.channelIndex = channelIndex;
            cmd.filterSubtypeMode = filterSubtypeMode;
            cmd.lowPassFrequencyValue = 199;
            cmd.reserved = 0;
            cmd.checksum = HNChecksumChar55;
            [self execute:[NSData dataWithBytes:&cmd length:sizeof(M2DControlLowPassCommand)]];
            _hadSendCommand = YES;
            NSLog(@"发送了_即时_0x%x指令:给Channel%lu发送了低通设置指令,Subtype是%d, 值为%lu", cmd.cmd, (unsigned long)cmd.channelIndex, cmd.filterSubtypeMode, (unsigned long)cmd.lowPassFrequencyValue);
        } else {
            NSInteger commandValue = [self transformValue:frequencyValue];
            M2DControlLowPassCommand cmd;
            cmd.cmd = HN_CONTROL_LOW_PASS;
            cmd.channelIndex = channelIndex;
            cmd.filterSubtypeMode = filterSubtypeMode;
            cmd.lowPassFrequencyValue = commandValue;
            cmd.reserved = 0;
            cmd.checksum = HNChecksumChar55;
            [self execute:[NSData dataWithBytes:&cmd length:sizeof(M2DControlLowPassCommand)]];
            NSLog(@"发送了_即时_0x%x指令:给Channel%lu发送了低通设置指令,Subtype是%d, 值为%lu", cmd.cmd, (unsigned long)cmd.channelIndex, cmd.filterSubtypeMode, (unsigned long)cmd.lowPassFrequencyValue);
        }
    } else {
        // 滑动slider，需要设置200ms发送一次
        NSInteger commandValue = [self transformValue:frequencyValue];
        if (_isSendState && !_hadSendCommand) {
            M2DControlLowPassCommand cmd;
            cmd.cmd = HN_CONTROL_LOW_PASS;
            cmd.channelIndex = channelIndex;
            cmd.filterSubtypeMode = filterSubtypeMode;
            cmd.lowPassFrequencyValue = commandValue;
            cmd.reserved = 0;
            cmd.checksum = HNChecksumChar55;
            [self execute:[NSData dataWithBytes:&cmd length:sizeof(M2DControlLowPassCommand)]];
            _hadSendCommand = YES;
            NSLog(@"发送了0x%x指令:给Channel%lu发送了低通设置指令,Subtype是%d, 值为%lu", cmd.cmd, (unsigned long)cmd.channelIndex, cmd.filterSubtypeMode, (unsigned long)cmd.lowPassFrequencyValue);
        }
    
    }
}

// Helper Method(将20-20K转换为硬件需要的0-199)
- (NSInteger)transformValue:(NSInteger)frequencyRawValue {
    NSInteger commandValue;
    if (frequencyRawValue <= 1100) {
        commandValue = (frequencyRawValue - 20) / 10;
    } else if (frequencyRawValue >= 1200 && frequencyRawValue <= 10000) {
        commandValue = (frequencyRawValue - 1200) / 100 + 100;
    } else {
        commandValue = (frequencyRawValue - 10000) / 1000 + 188;
    }
    return commandValue;
}

#pragma mark Setup页面
- (void)setupSSID:(NSString *)ssidString password:(NSString *)passwordString {
    NSMutableData *ssidData = [NSMutableData data];
    
    M2DModifySSIDCommandHeader cmdHeader;
    cmdHeader.cmd = HN_MODIFY_SSID;
    cmdHeader.wifiMode = 0;// 现在只有一种模式，固定发0
    cmdHeader.wifiNameLength = ssidString.length;
    [ssidData appendBytes:&cmdHeader length:sizeof(M2DModifySSIDCommandHeader)];
    
    [ssidData appendBytes:[ssidString UTF8String] length:ssidString.length];
    
    M2DModifySSIDCommandMiddle cmdMiddle;
    cmdMiddle.segmentationCharacter = 0xff;
    cmdMiddle.wifiPasswordLength = passwordString.length;
    [ssidData appendBytes:&cmdMiddle length:sizeof(M2DModifySSIDCommandMiddle)];
    
    [ssidData appendBytes:[passwordString UTF8String] length:passwordString.length];
    
    M2DModifySSIDCommandTail cmdTail;
    cmdTail.reserved = 0;
    cmdTail.checksum = HNChecksumChar55;
    [ssidData appendBytes:&cmdTail length:sizeof(M2DModifySSIDCommandTail)];
    
    [self execute:ssidData];
    NSLog(@"发送了0x%x指令:发送了ssid长度为:%d, 内容为:%@, 密码长度为:%d, 内容为:%@ 给硬件", cmdHeader.cmd, cmdHeader.wifiNameLength, ssidString, cmdMiddle.wifiPasswordLength, passwordString);
}

#pragma mark Command Implementation Help Method
- (void)execute:(NSData *)param {
    [_tcpSocket writeData:param withTimeout:-1 tag:HN_TCP_COMMAND_COMMNONLY_TAG];
}

#pragma mark 同步指令
- (void)syncLrInputValue:(NSInteger)lrInputValue mainInputValue:(NSInteger)mainInputValue auxInputValue:(NSInteger)auxInputValue {
    M2DSyncInputChannelCommand cmd;
    cmd.cmd            = HN_SYNC_INPUT_CHANNLE;
    cmd.lrInputValue   = lrInputValue;
    cmd.mainInputValue = mainInputValue;
    cmd.auxInputValue  = auxInputValue;
    cmd.reserved       = 0;
    cmd.checksum       = HNChecksumChar55;
    [self execute:[NSData dataWithBytes:&cmd length:sizeof(M2DSyncInputChannelCommand)]];
    NSLog(@"发送了Input同步指令0x%x。lrInputValue:%d; mainInputValue:%d; auxInputValue:%d", cmd.cmd, cmd.lrInputValue, cmd.mainInputValue, cmd.auxInputValue);
}

- (void)syncVolume:(NSArray *)volumes {
    M2DSyncVolumeCommand cmd;
    cmd.cmd = HN_SYNC_VOLUME;
    cmd.channel_1_Volume  = [volumes[0] integerValue];
    cmd.channel_2_Volume  = [volumes[1] integerValue];
    cmd.channel_3_Volume  = [volumes[2] integerValue];
    cmd.channel_4_Volume  = [volumes[3] integerValue];
    cmd.channel_5_Volume  = [volumes[4] integerValue];
    cmd.channel_6_Volume  = [volumes[5] integerValue];
    cmd.channel_7_Volume  = [volumes[6] integerValue];
    cmd.channel_8_Volume  = [volumes[7] integerValue];
    cmd.channel_9_Volume  = [volumes[8] integerValue];
    cmd.channel_10_Volume = [volumes[9] integerValue];
    [self execute:[NSData dataWithBytes:&cmd length:sizeof(M2DSyncVolumeCommand)]];
    NSLog(@"发送了音量同步指令0x%x。channel1:%d; channel2:%d; channel3:%d; channel4:%d; channel5:%d; channel6:%d; channel7:%d; channel8:%d; channel9:%d; channel10:%d", cmd.cmd, cmd.channel_1_Volume, cmd.channel_2_Volume, cmd.channel_3_Volume, cmd.channel_4_Volume, cmd.channel_5_Volume, cmd.channel_6_Volume, cmd.channel_7_Volume, cmd.channel_8_Volume, cmd.channel_9_Volume, cmd.channel_10_Volume);
}

- (void)syncEQ:(NSInteger)eq1 eq2:(NSInteger)eq2 eq3:(NSInteger)eq3 eq4:(NSInteger)eq4 eq5:(NSInteger)eq5 eq6:(NSInteger)eq6 eq7:(NSInteger)eq7 eq8:(NSInteger)eq8 eq9:(NSInteger)eq9 eq10:(NSInteger)eq10 forChannel:(NSInteger)channelIndex {
    M2DSyncEQCommand cmd;
    cmd.cmd = HN_SYNC_EQ;
    cmd.channelIndex = channelIndex;
    cmd.eq1  = eq1;
    cmd.eq2  = eq2;
    cmd.eq3  = eq3;
    cmd.eq4  = eq4;
    cmd.eq5  = eq5;
    cmd.eq6  = eq6;
    cmd.eq7  = eq7;
    cmd.eq8  = eq8;
    cmd.eq9  = eq9;
    cmd.eq10 = eq10;
    cmd.checksum = HNChecksumChar55;
    [self execute:[NSData dataWithBytes:&cmd length:sizeof(M2DSyncEQCommand)]];
    NSLog(@"发送了EQ同步指令0x%x。第%d个channel: eq1:%d; eq2:%d; eq3:%d; eq4:%d; eq5:%d; eq6:%d; eq7:%d; eq8:%d; eq9:%d; eq10:%d", cmd.cmd, cmd.channelIndex, cmd.eq1, cmd.eq2, cmd.eq3, cmd.eq4, cmd.eq5, cmd.eq6, cmd.eq7, cmd.eq8, cmd.eq9, cmd.eq10);
}

- (void)syncBass:(NSArray *)BassValues {
    M2DSyncBassCommand cmd;
    cmd.cmd = HN_SYNC_BASS;
    cmd.channel_1_Bass  = [BassValues[0] integerValue] + 12;// command定义值范围为0-24
    cmd.channel_2_Bass  = [BassValues[1] integerValue] + 12;
    cmd.channel_3_Bass  = [BassValues[2] integerValue] + 12;
    cmd.channel_4_Bass  = [BassValues[3] integerValue] + 12;
    cmd.channel_5_Bass  = [BassValues[4] integerValue] + 12;
    cmd.channel_6_Bass  = [BassValues[5] integerValue] + 12;
    cmd.channel_7_Bass  = [BassValues[6] integerValue] + 12;
    cmd.channel_8_Bass  = [BassValues[7] integerValue] + 12;
    cmd.channel_9_Bass  = [BassValues[8] integerValue] + 12;
    cmd.channel_10_Bass = [BassValues[9] integerValue] + 12;
    cmd.checksum = HNChecksumChar55;
    [self execute:[NSData dataWithBytes:&cmd length:sizeof(M2DSyncBassCommand)]];
    NSLog(@"发送了低音同步指令0x%x。channel1:%d; channel2:%d; channel3:%d; channel4:%d; channel5:%d; channel6:%d; channel7:%d; channel8:%d; channel9:%d; channel10:%d", cmd.cmd, cmd.channel_1_Bass, cmd.channel_2_Bass, cmd.channel_3_Bass, cmd.channel_4_Bass, cmd.channel_5_Bass, cmd.channel_6_Bass, cmd.channel_7_Bass, cmd.channel_8_Bass, cmd.channel_9_Bass, cmd.channel_10_Bass);
}

- (void)syncTreble:(NSArray *)TrebleValues {
    M2DSyncTrebleCommand cmd;
    cmd.cmd = HN_SYNC_TREBLE;
    cmd.channel_1_Treble  = [TrebleValues[0] integerValue] + 12;
    cmd.channel_2_Treble  = [TrebleValues[1] integerValue] + 12;
    cmd.channel_3_Treble  = [TrebleValues[2] integerValue] + 12;
    cmd.channel_4_Treble  = [TrebleValues[3] integerValue] + 12;
    cmd.channel_5_Treble  = [TrebleValues[4] integerValue] + 12;
    cmd.channel_6_Treble  = [TrebleValues[5] integerValue] + 12;
    cmd.channel_7_Treble  = [TrebleValues[6] integerValue] + 12;
    cmd.channel_8_Treble  = [TrebleValues[7] integerValue] + 12;
    cmd.channel_9_Treble  = [TrebleValues[8] integerValue] + 12;
    cmd.channel_10_Treble = [TrebleValues[9] integerValue] + 12;
    cmd.checksum = HNChecksumChar55;
    [self execute:[NSData dataWithBytes:&cmd length:sizeof(M2DSyncTrebleCommand)]];
    NSLog(@"发送了高音同步指令0x%x。channel1:%d; channel2:%d; channel3:%d; channel4:%d; channel5:%d; channel6:%d; channel7:%d; channel8:%d; channel9:%d; channel10:%d", cmd.cmd, cmd.channel_1_Treble, cmd.channel_2_Treble, cmd.channel_3_Treble, cmd.channel_4_Treble, cmd.channel_5_Treble, cmd.channel_6_Treble, cmd.channel_7_Treble, cmd.channel_8_Treble, cmd.channel_9_Treble, cmd.channel_10_Treble);
}
- (void)syncHighPass:(NSArray *)HighpassValues highPassSubTypes:(NSArray *)highPassSubTypes {
    M2DSyncHighpassCommand cmd;
    cmd.cmd = HN_SYNC_HIGH_PASS;
    cmd.channel_1_HighpassSubType  = [highPassSubTypes[0] integerValue];
    cmd.channel_1_Highpass         = [self transformValue:[HighpassValues[0] integerValue]];
    cmd.channel_2_HighpassSubType  = [highPassSubTypes[1] integerValue];
    cmd.channel_2_Highpass         = [self transformValue:[HighpassValues[1] integerValue]];
    cmd.channel_3_HighpassSubType  = [highPassSubTypes[2] integerValue];
    cmd.channel_3_Highpass         = [self transformValue:[HighpassValues[2] integerValue]];
    cmd.channel_4_HighpassSubType  = [highPassSubTypes[3] integerValue];
    cmd.channel_4_Highpass         = [self transformValue:[HighpassValues[3] integerValue]];
    cmd.channel_5_HighpassSubType  = [highPassSubTypes[4] integerValue];
    cmd.channel_5_Highpass         = [self transformValue:[HighpassValues[4] integerValue]];
    cmd.channel_6_HighpassSubType  = [highPassSubTypes[5] integerValue];
    cmd.channel_6_Highpass         = [self transformValue:[HighpassValues[5] integerValue]];
    cmd.channel_7_HighpassSubType  = [highPassSubTypes[6] integerValue];
    cmd.channel_7_Highpass         = [self transformValue:[HighpassValues[6] integerValue]];
    cmd.channel_8_HighpassSubType  = [highPassSubTypes[7] integerValue];
    cmd.channel_8_Highpass         = [self transformValue:[HighpassValues[7] integerValue]];
    cmd.channel_9_HighpassSubType  = [highPassSubTypes[8] integerValue];
    cmd.channel_9_Highpass         = [self transformValue:[HighpassValues[8] integerValue]];
    cmd.channel_10_HighpassSubType = [highPassSubTypes[9] integerValue];
    cmd.channel_10_Highpass        = [self transformValue:[HighpassValues[9] integerValue]];
    cmd.checksum = HNChecksumChar55;
    [self execute:[NSData dataWithBytes:&cmd length:sizeof(M2DSyncHighpassCommand)]];
    NSLog(@"发送了高通同步指令0x%x。channel1:%d; channel2:%d; channel3:%d; channel4:%d; channel5:%d; channel6:%d; channel7:%d; channel8:%d; channel9:%d; channel10:%d", cmd.cmd, cmd.channel_1_Highpass, cmd.channel_2_Highpass, cmd.channel_3_Highpass, cmd.channel_4_Highpass, cmd.channel_5_Highpass, cmd.channel_6_Highpass, cmd.channel_7_Highpass, cmd.channel_8_Highpass, cmd.channel_9_Highpass, cmd.channel_10_Highpass);
}

-(void)syncLowPass:(NSArray *)LowpassValues lowPassSubType:(NSArray *)lowPassSubTypes {
    M2DSyncLowpassCommand cmd;
    cmd.cmd = HN_SYNC_LOW_PASS;
    cmd.channel_1_LowpassSubType  = [lowPassSubTypes[0] integerValue];
    cmd.channel_1_Lowpass         = [self transformValue:[LowpassValues[0] integerValue]];
    cmd.channel_2_LowpassSubType  = [lowPassSubTypes[1] integerValue];
    cmd.channel_2_Lowpass         = [self transformValue:[LowpassValues[1] integerValue]];
    cmd.channel_3_LowpassSubType  = [lowPassSubTypes[2] integerValue];
    cmd.channel_3_Lowpass         = [self transformValue:[LowpassValues[2] integerValue]];
    cmd.channel_4_LowpassSubType  = [lowPassSubTypes[3] integerValue];
    cmd.channel_4_Lowpass         = [self transformValue:[LowpassValues[3] integerValue]];
    cmd.channel_5_LowpassSubType  = [lowPassSubTypes[4] integerValue];
    cmd.channel_5_Lowpass         = [self transformValue:[LowpassValues[4] integerValue]];
    cmd.channel_6_LowpassSubType  = [lowPassSubTypes[5] integerValue];
    cmd.channel_6_Lowpass         = [self transformValue:[LowpassValues[5] integerValue]];
    cmd.channel_7_LowpassSubType  = [lowPassSubTypes[6] integerValue];
    cmd.channel_7_Lowpass         = [self transformValue:[LowpassValues[6] integerValue]];
    cmd.channel_8_LowpassSubType  = [lowPassSubTypes[7] integerValue];
    cmd.channel_8_Lowpass         = [self transformValue:[LowpassValues[7] integerValue]];
    cmd.channel_9_LowpassSubType  = [lowPassSubTypes[8] integerValue];
    cmd.channel_9_Lowpass         = [self transformValue:[LowpassValues[8] integerValue]];
    cmd.channel_10_LowpassSubType = [lowPassSubTypes[9] integerValue];
    cmd.channel_10_Lowpass        = [self transformValue:[LowpassValues[9] integerValue]];
    cmd.checksum = HNChecksumChar55;
    [self execute:[NSData dataWithBytes:&cmd length:sizeof(M2DSyncHighpassCommand)]];
    NSLog(@"发送了低通同步指令0x%x。channel1:%d; channel2:%d; channel3:%d; channel4:%d; channel5:%d; channel6:%d; channel7:%d; channel8:%d; channel9:%d; channel10:%d", cmd.cmd, cmd.channel_1_Lowpass, cmd.channel_2_Lowpass, cmd.channel_3_Lowpass, cmd.channel_4_Lowpass, cmd.channel_5_Lowpass, cmd.channel_6_Lowpass, cmd.channel_7_Lowpass, cmd.channel_8_Lowpass, cmd.channel_9_Lowpass, cmd.channel_10_Lowpass);
}

#pragma mark - GCDAsyncSocketDelegate
- (void)socket:(GCDAsyncSocket *)sock didConnectToHost:(NSString *)host port:(uint16_t)port {
    [_tcpSocket readDataWithTimeout:-1 tag:0];
    // 通知委托对象，socket已经链接上了
    if ([self.delegate respondsToSelector:@selector(deviceDidConnect)]) {
        [self.delegate deviceDidConnect];
    }
}

- (void)socketDidDisconnect:(GCDAsyncSocket *)sock withError:(NSError *)err {
    if ([self.delegate respondsToSelector:@selector(deviceDidDisconnect)]) {
        [self.delegate deviceDidDisconnect];
    }
}

@end
