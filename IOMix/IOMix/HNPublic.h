//
//  HNPublic.h
//  IOMix
//
//  Created by YuHeng_Antony on 11/11/15.
//  Copyright © 2015 Homni Enterprises Co., Ltd. All rights reserved.
//

#ifndef HNPublic_h
#define HNPublic_h


#endif /* HNPublic_h */

#pragma mark - 
// 判断系统版本是否为iOS8以后
#define IS_OS_8_OR_LATER ([UIDevice currentDevice].systemVersion.floatValue >= 8.0)

#pragma mark - TabBarIndex
typedef NS_ENUM(NSUInteger, HN_TabBarIndex) {
    HN_TabBarIndex_HOME = 0,
    HN_TabBarIndex_EQ,
    HN_TabBarIndex_FILTER,
    HN_TabBarIndex_MONITOR,
    HN_TabBarIndex_SETUP,
};
static NSString *previousPageTag = @"previousPageTag";

#pragma mark - 曲线图类型
// 绘图方法要判断是EQ还是Filter页面，绘制不同曲线
typedef NS_ENUM(NSInteger, HN_CURVE) {
    HN_CURVE_EQ,
    HN_CURVE_FILTER,
};

#pragma mark - EQ曲线图相关
// 绘制EQ曲线图定义的常量
#define HN_LINE_WIDTH                                          1.5
#define HN_POINT_A_X_SCALE                                     0.02877// 起点x值比例 29/1008
#define HN_POINT_A_Y_SCALE                                     0.09016// 起点y值比例 22/244
#define HN_RIGHT_SPACE_SCALE                                   0.01587// 背景最右边边线条与背景图右边边距比例 16/244
#define HN_HORIZONTAL_SPACE self.bounds.size.height *          0.10246// 中间水平线条边距比例 25/244
#define HN_V_GROUP_SPACE_VIEW self.bounds.size.width *         0.32507// 每组垂直线条间距比例(一共3组) 313/1008(视图类(绘制背景)用到)
#define HN_V_GROUP_SPACE_VC self.curveView.bounds.size.width * 0.32507// 每组垂直线条间距比例(一共3组) 313/1008(控制器类用到)

// 背景图最左边的X值、最右边的X值；最顶的Y值、最底的Y值
#define HN_BG_LEADING_VALUE self.bounds.size.width * HN_POINT_A_X_SCALE
#define HN_BG_TRAILING_VALUE HN_BG_LEADING_VALUE + (self.bounds.size.width - HN_BG_LEADING_VALUE - (self.bounds.size.width * HN_RIGHT_SPACE_SCALE))
#define HN_BG_TOP_VALUE self.bounds.size.height * HN_POINT_A_Y_SCALE
#define HN_BG_BOTTOM_VALUE self.bounds.size.height - HN_BG_TOP_VALUE

// 9条竖线的间距比例
typedef NS_ENUM(NSInteger, HN_V_SPACE_L_TYPES) {
    HN_V_SPACE_L1toL2 = 0,
    HN_V_SPACE_L2toL3,
    HN_V_SPACE_L3toL4,
    HN_V_SPACE_L4toL5,
    HN_V_SPACE_L5toL6,
    HN_V_SPACE_L6toL7,
    HN_V_SPACE_L7toL8,
    HN_V_SPACE_L8toL9,
};
static const float gVerticalSpaceLTypes[] = {0.05456, 0.03969, 0.02976, 0.02381, 0.02183, 0.01786, 0.01786, 0.01290};
static const float gVerticalSpaceLTypesSwift[] = {0.12401,0.21827,0.31052,0.43453,0.52879,0.62104,0.74505,0.83951,0.93156};
#define HN_GET_V_SPACE_WITH_VC(index) (gVerticalSpaceLTypes[index] * self.curveView.bounds.size.width)
#define HN_GET_V_SPACE_WITH_VIEW(index) (gVerticalSpaceLTypes[index] * self.bounds.size.width)

// 9条竖线X值的计算
#define HN_LINE1_X HN_BG_LEADING_VALUE
#define HN_LINE2_X HN_BG_LEADING_VALUE + HN_GET_V_SPACE_WITH_VIEW(HN_V_SPACE_L1toL2)
#define HN_LINE3_X HN_BG_LEADING_VALUE + HN_GET_V_SPACE_WITH_VIEW(HN_V_SPACE_L1toL2) + HN_GET_V_SPACE_WITH_VIEW(HN_V_SPACE_L2toL3)
#define HN_LINE4_X HN_BG_LEADING_VALUE + HN_GET_V_SPACE_WITH_VIEW(HN_V_SPACE_L1toL2) + HN_GET_V_SPACE_WITH_VIEW(HN_V_SPACE_L2toL3) + HN_GET_V_SPACE_WITH_VIEW(HN_V_SPACE_L3toL4)
#define HN_LINE5_X HN_BG_LEADING_VALUE + HN_GET_V_SPACE_WITH_VIEW(HN_V_SPACE_L1toL2) + HN_GET_V_SPACE_WITH_VIEW(HN_V_SPACE_L2toL3) + HN_GET_V_SPACE_WITH_VIEW(HN_V_SPACE_L3toL4) + HN_GET_V_SPACE_WITH_VIEW(HN_V_SPACE_L4toL5)
#define HN_LINE6_X HN_BG_LEADING_VALUE + HN_GET_V_SPACE_WITH_VIEW(HN_V_SPACE_L1toL2) + HN_GET_V_SPACE_WITH_VIEW(HN_V_SPACE_L2toL3) + HN_GET_V_SPACE_WITH_VIEW(HN_V_SPACE_L3toL4) + HN_GET_V_SPACE_WITH_VIEW(HN_V_SPACE_L4toL5) + HN_GET_V_SPACE_WITH_VIEW(HN_V_SPACE_L5toL6)
#define HN_LINE7_X HN_BG_LEADING_VALUE + HN_GET_V_SPACE_WITH_VIEW(HN_V_SPACE_L1toL2) + HN_GET_V_SPACE_WITH_VIEW(HN_V_SPACE_L2toL3) + HN_GET_V_SPACE_WITH_VIEW(HN_V_SPACE_L3toL4) + HN_GET_V_SPACE_WITH_VIEW(HN_V_SPACE_L4toL5) + HN_GET_V_SPACE_WITH_VIEW(HN_V_SPACE_L5toL6) + HN_GET_V_SPACE_WITH_VIEW(HN_V_SPACE_L6toL7)
#define HN_LINE8_X HN_BG_LEADING_VALUE + HN_GET_V_SPACE_WITH_VIEW(HN_V_SPACE_L1toL2) + HN_GET_V_SPACE_WITH_VIEW(HN_V_SPACE_L2toL3) + HN_GET_V_SPACE_WITH_VIEW(HN_V_SPACE_L3toL4) + HN_GET_V_SPACE_WITH_VIEW(HN_V_SPACE_L4toL5) + HN_GET_V_SPACE_WITH_VIEW(HN_V_SPACE_L5toL6) + HN_GET_V_SPACE_WITH_VIEW(HN_V_SPACE_L6toL7) + HN_GET_V_SPACE_WITH_VIEW(HN_V_SPACE_L7toL8)
#define HN_LINE9_X HN_BG_LEADING_VALUE + HN_GET_V_SPACE_WITH_VIEW(HN_V_SPACE_L1toL2) + HN_GET_V_SPACE_WITH_VIEW(HN_V_SPACE_L2toL3) + HN_GET_V_SPACE_WITH_VIEW(HN_V_SPACE_L3toL4) + HN_GET_V_SPACE_WITH_VIEW(HN_V_SPACE_L4toL5) + HN_GET_V_SPACE_WITH_VIEW(HN_V_SPACE_L5toL6) + HN_GET_V_SPACE_WITH_VIEW(HN_V_SPACE_L6toL7) + HN_GET_V_SPACE_WITH_VIEW(HN_V_SPACE_L7toL8) + HN_GET_V_SPACE_WITH_VIEW(HN_V_SPACE_L8toL9)


#define HN_RIGHT_LINE_POINT_X_SCALE 0.97520 // 最右边单独线条X值比例 983/1008
#define HN_SCALE_20  @"20"
#define HN_SCALE_db  @"(dB)"
#define HN_SCALE_50  @"50"
#define HN_SCALE_100 @"100"
#define HN_SCALE_200 @"200"
#define HN_SCALE_500 @"500"
#define HN_SCALE_1K  @"1K"
#define HN_SCALE_2K  @"2K"
#define HN_SCALE_5K  @"5K"
#define HN_SCALE_10K @"10K"
#define HN_SCALE_20K @"20K"

// 曲线图最左边的X值、最右边的X值；最顶的Y值、最底的Y值
#define HN_LEADING_VALUE self.curveView.bounds.size.width * HN_POINT_A_X_SCALE
#define HN_TRAILING_VALUE HN_LEADING_VALUE + (self.curveView.bounds.size.width - HN_LEADING_VALUE - (self.curveView.bounds.size.width * HN_RIGHT_SPACE_SCALE))
#define HN_TOP_VALUE self.curveView.bounds.size.height * HN_POINT_A_Y_SCALE
#define HN_BOTTOM_VALUE self.curveView.bounds.size.height - HN_TOP_VALUE

#pragma mark - EQ页面
typedef NS_ENUM(NSInteger, CHANNEL) {
    CHANNEL1 = 0,
    CHANNEL2,
    CHANNEL3,
    CHANNEL4,
    CHANNEL5,
    CHANNEL6,
    CHANNEL7,
    CHANNEL8,
    CHANNELOutL,
    CHANNELOutR,
    CHANNEL_COUNT,
};

typedef NS_ENUM(NSInteger, EQSlider) {
    EQSlider20Hz = 211,
    EQSlider50Hz,
    EQSlider100Hz,
    EQSlider200Hz,
    EQSlider500Hz,
    EQSlider1K,
    EQSlider2K,
    EQSlider5K,
    EQSlider10K,
    EQSlider20K,
    EQSliderCount = 10,
};
static NSString *kNotificationShuoldSaveData              = @"com.Homni.IOMix.shuoldSaveData";

static NSString *kNotificationEQSliderValueChanged        = @"com.Homni.IOMix.EQSliderValueChanged";
static NSString *kNotificationEQPointControlYValueChanged = @"com.Homni.IOMix.PointControlYValueChanged";

static NSString *kNotificationFilterSliderValueChanged    = @"com.Homni.IOMix.FilterSliderValueChanged";

static NSString *EQSliderTag                              = @"EQsliderTag";
static NSString *EQSliderValue                            = @"EQsliderValue";

static NSString *EQPointControlTag                        = @"EQPointControlTag";
static NSString *EQPointControlValue                      = @"EQPointControlValue";

#pragma mark - HOME页面
typedef NS_ENUM(NSInteger, MUTE) {
    MUTE_CH1,
    MUTE_CH2,
    MUTE_CH3,
    MUTE_CH4,
    MUTE_CH5,
    MUTE_CH6,
    MUTE_CH7,
    MUTE_CH8,
    MUTE_OUTL,
    MUTE_OUTR,
};
static NSString *kNotificationVolumeDidChanged            = @"com.Homni.IOMix.VolumeDidChanged";
static NSString *kNotificationSyncDidClicked              = @"com.Homni.IOMix.SyncDidClicked";
static NSString *sliderTag                                = @"sliderTag";
static NSString *sliderValue                              = @"sliderValue";
static NSString *syncButtonTag                            = @"syncButtonTag";

#pragma mark - Command相关
static NSString *kNotificationSocketDidConnect            = @"com.Homni.IOMix.socketDidConnect";
