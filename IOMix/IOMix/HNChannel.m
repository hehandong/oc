//
//  HNChannel.m
//  IOMix
//
//  Created by YuHeng_Antony on 15/7/10.
//  Copyright (c) 2015年 Homni Enterprises Co., Ltd. All rights reserved.
//

#import "HNChannel.h"
#import "HNEqualizer.h"

@implementation HNChannel

- (instancetype)init
{
    self = [super init];
    if (self) {
        // Home
        _volume = 25.0;
        _isSYNC = NO;
        _isMUTE = NO;
        
        // EQ
        _equalizer  = [[HNEqualizer alloc] init];
        _effectMode = 8;
        
        // Filter
        _pitchType              = 0;
        _bassGainValue          = 0;
        _trebleGainValue        = 0;
        _passType               = 0;
        _highPassSubType        = 0;
        _lowPassSubType         = 0;
        _highPassfrequencyValue = 20;
        _isAllPassForHighPass   = NO;
        _lowPassfrequencyValue  = 20000;
        _isAllPassForLowhPass   = NO;
        
        // Monitor
        _lrInputValue   = 0;
        _mainInputValue = 0;
        _auxInputValue  = 0;
    }
    return self;
}

@end
