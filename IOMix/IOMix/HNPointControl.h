//
//  HNPointControl.h
//  IOMix
//
//  Created by YuHeng_Antony on 8/27/15.
//  Copyright (c) 2015 Homni Enterprises Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HNPointControl : UIControl

+ (HNPointControl *)aInstance;

@property (nonatomic,copy) void (^dragCallBack)(HNPointControl * pointView);

@end
