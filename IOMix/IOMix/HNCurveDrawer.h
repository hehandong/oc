//
//  HNCurveDrawer.h
//  IOMix
//
//  Created by YuHeng_Antony on 11/12/15.
//  Copyright © 2015 Homni Enterprises Co., Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
@import UIKit;

@interface HNCurveDrawer : NSObject

@property (weak, nonatomic) UIView *curveView;
@property (strong, nonatomic) NSArray *eqSliders;

- (void)drawCurveFor:(NSInteger)curveType filterSubType:(NSInteger)filterSubType UsePoints:(NSArray *)points;

@end
