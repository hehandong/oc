//
//  HNSetupViewController.m
//  IOMix
//
//  Created by YuHeng_Antony on 15/7/8.
//  Copyright (c) 2015年 Homni Enterprises Co., Ltd. All rights reserved.
//

#import "HNSetupViewController.h"
#import "HNIOMixManager.h"
#import "HNIOMixDevice.h"

typedef NS_ENUM(NSInteger, TEXTFIELD) {
    TEXTFIELD_SSID,
    TEXTFIELD_PASSWORD,
};

@interface HNSetupViewController ()<UITextFieldDelegate>
@property (copy, nonatomic  ) NSString    *ssidString;
@property (strong, nonatomic) UIAlertView *ssidLengthAlertView;
@property (strong, nonatomic) UIAlertView *passwordLengthAlertView;
@end

@implementation HNSetupViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

#pragma mark - UITextFieldDelegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (textField.tag == TEXTFIELD_SSID) {
        NSString *ssidString = [textField.text stringByReplacingCharactersInRange:range withString:string];
        if (ssidString.length > 21) {
            if (!_ssidLengthAlertView) {
                _ssidLengthAlertView = [[UIAlertView alloc] initWithTitle:@"NOTE" message:@"You can not type beyond 21 characters." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            }
            [_ssidLengthAlertView show];
            return NO;
        }
    } else if (textField.tag == TEXTFIELD_PASSWORD) {
        NSString *passwordString = [textField.text stringByReplacingCharactersInRange:range withString:string];
        if (passwordString.length > 11) {
            if (!_passwordLengthAlertView) {
                _passwordLengthAlertView = [[UIAlertView alloc] initWithTitle:@"NOTE" message:@"You can not type beyond 11 characters." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            }
            [_passwordLengthAlertView show];
            return NO;
        }
    }
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    if (textField.tag == TEXTFIELD_PASSWORD) {
        [[HNIOMixManager shareManager].device setupSSID: _ssidString password:textField.text];
    } else {
        _ssidString = textField.text;
    }
    return YES;
}

#pragma mark -
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
@end
