//
//  UIBezierPath+LxThroughPointsBezier.m
//  LxThroughPointsBezierDemo
//

#import "UIBezierPath+LxThroughPointsBezier.h"
#import <objc/runtime.h>
#import "IOMix-Swift.h"

@implementation UIBezierPath (LxThroughPointsBezier)

- (void)setContractionFactor:(CGFloat)contractionFactor
{
    objc_setAssociatedObject(self, @selector(contractionFactor), @(contractionFactor), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (CGFloat)contractionFactor
{
    id contractionFactorAssociatedObject = objc_getAssociatedObject(self, @selector(contractionFactor));
    if (contractionFactorAssociatedObject == nil) {
        return 0.7;
    }
    return [contractionFactorAssociatedObject floatValue];
}

#pragma mark - 绘制EQ曲线时，绘图类调用此方法
- (void)addBezierThroughPoints:(NSArray *)pointArray
{
    NSAssert(pointArray.count > 0, @"You must give at least 1 point for drawing the curve.");
    
    if (pointArray.count < 3) {
        switch (pointArray.count) {
            case 1:
            {
                NSValue * point0Value = pointArray[0];
                CGPoint point0 = [point0Value CGPointValue];
                [self addLineToPoint:point0];
            }
                break;
            case 2:
            {
                NSValue * point0Value = pointArray[0];
                CGPoint point0 = [point0Value CGPointValue];
                NSValue * point1Value = pointArray[1];
                CGPoint point1 = [point1Value CGPointValue];
                [self addQuadCurveToPoint:point1 controlPoint:ControlPointForTheBezierCanThrough3Point(self.currentPoint, point0, point1)];
            }
                break;
            default:
                break;
        }
    }
    
//    CGPoint previousPoint = CGPointZero;
//    
//    CGPoint previousCenterPoint = CGPointZero;
//    CGPoint centerPoint = CGPointZero;
//    CGFloat centerPointDistance = 0;
//    
//    CGFloat obliqueAngle = 0;
//    
//    CGPoint previousControlPoint1 = CGPointZero;
//    CGPoint previousControlPoint2 = CGPointZero;
//    CGPoint controlPoint1 = CGPointZero;
//
//    previousPoint = self.currentPoint;
    
//    for (int i = 0; i < pointArray.count; i++) {
//        
//        NSValue * pointIValue = pointArray[i];
//        CGPoint pointI = [pointIValue CGPointValue];
//        
//        if (i > 0) {
//            
//            previousCenterPoint = CenterPointOf(self.currentPoint, previousPoint);
//            centerPoint = CenterPointOf(previousPoint, pointI);
//            
//            centerPointDistance = DistanceBetweenPoint(previousCenterPoint, centerPoint);
//            
//            obliqueAngle = ObliqueAngleOfStraightThrough(centerPoint, previousCenterPoint);
//            
//            previousControlPoint2 = CGPointMake(previousPoint.x - 0.5 * self.contractionFactor * centerPointDistance * cos(obliqueAngle), previousPoint.y - 0.5 * self.contractionFactor * centerPointDistance * sin(obliqueAngle));
//            controlPoint1 = CGPointMake(previousPoint.x + 0.5 * self.contractionFactor * centerPointDistance * cos(obliqueAngle), previousPoint.y + 0.5 * self.contractionFactor * centerPointDistance * sin(obliqueAngle));
//        }
//        
//        if (i == 1) {
//            
//            [self addQuadCurveToPoint:previousPoint controlPoint:previousControlPoint2];
//        }
//        else if (i > 1 && i < pointArray.count - 1) {
//        
//            [self addCurveToPoint:previousPoint controlPoint1:previousControlPoint1 controlPoint2:previousControlPoint2];
//        }
//        else if (i == pointArray.count - 1) {
//        
//            [self addCurveToPoint:previousPoint controlPoint1:previousControlPoint1 controlPoint2:previousControlPoint2];
//            [self addQuadCurveToPoint:pointI controlPoint:controlPoint1];
//        }
//        else {
//        
//        }
//        
//        previousControlPoint1 = controlPoint1;
//        previousPoint = pointI;
//        
//        
//        
//    }
    
    
        //新方法
//        CGPoint startP = CGPointZero;
//    
//        CGPoint endP = CGPointZero;
    
        for (int i = 0; i < pointArray.count; i++) {
            if (i < pointArray.count-1){
    
    //            startP = pointArray[i];
    //            endP = pointArray[i+1];
                NSValue * startPValue = pointArray[i];
                CGPoint startP = [startPValue CGPointValue];
    
                NSValue * endPValue = pointArray[i+1];
                CGPoint endP = [endPValue CGPointValue];
    
    
                CGFloat wt = (startP.x + endP.x)/2;
    
                CGPoint controlP1 = CGPointZero;
    
                CGPoint controlP2 = CGPointZero;
    
                controlP1.y = startP.y;
                controlP1.x = wt;
    
                controlP2.y = endP.y;
                controlP2.x = wt;
    
                [self moveToPoint:CGPointMake(startP.x, startP.y)];
    
                [self addCurveToPoint:endP controlPoint1:controlP1 controlPoint2:controlP2];
            }
        }
}

CGFloat ObliqueAngleOfStraightThrough(CGPoint point1, CGPoint point2)   //  [-π/2, 3π/2)
{
    CGFloat obliqueRatio = 0;
    CGFloat obliqueAngle = 0;
    
    if (point1.x > point2.x) {
    
        obliqueRatio = (point2.y - point1.y) / (point2.x - point1.x);
        obliqueAngle = atan(obliqueRatio);
    }
    else if (point1.x < point2.x) {
    
        obliqueRatio = (point2.y - point1.y) / (point2.x - point1.x);
        obliqueAngle = M_PI + atan(obliqueRatio);
    }
    else if (point2.y - point1.y >= 0) {
    
        obliqueAngle = M_PI/2;
    }
    else {
        obliqueAngle = -M_PI/2;
    }
    
    return obliqueAngle;
}

CGPoint ControlPointForTheBezierCanThrough3Point(CGPoint point1, CGPoint point2, CGPoint point3)
{
    return CGPointMake(2 * point2.x - (point1.x + point3.x) / 2, 2 * point2.y - (point1.y + point3.y) / 2);
}

CGFloat DistanceBetweenPoint(CGPoint point1, CGPoint point2)
{
    return sqrt((point1.x - point2.x) * (point1.x - point2.x) + (point1.y - point2.y) * (point1.y - point2.y));
}

CGPoint CenterPointOf(CGPoint point1, CGPoint point2)
{
    return CGPointMake((point1.x + point2.x) / 2, (point1.y + point2.y) / 2);
}

#pragma mark - 绘制Filter曲线时，绘图类调用此方法
- (void)addBezierThroughFilterPoints:(NSArray *)pointArray filterSubType:(NSInteger)filterSubType {
    NSAssert(pointArray.count > 0, @"You must give at least 1 point for drawing the curve.");
    
    if (pointArray.count < 3) {
        switch (pointArray.count) {
            case 1:
            {
                NSValue * point0Value = pointArray[0];
                CGPoint point0 = [point0Value CGPointValue];
                [self addLineToPoint:point0];
            }
                break;
            case 2:
            {
                NSValue * point0Value = pointArray[0];
                CGPoint point0 = [point0Value CGPointValue];
                NSValue * point1Value = pointArray[1];
                CGPoint point1 = [point1Value CGPointValue];
                [self addQuadCurveToPoint:point1 controlPoint:ControlPointForTheBezierCanThrough3Point(self.currentPoint, point0, point1)];
            }
                break;
            default:
                break;
        }
    }
    
    //画曲线的4个坐标点
    NSValue * startPValue = pointArray[0];
    CGPoint startP = [startPValue CGPointValue];
    
    NSValue * secPValue = pointArray[1];
    CGPoint secP = [secPValue CGPointValue];
    
    NSValue * thrPValue = pointArray[2];
    CGPoint thrP = [thrPValue CGPointValue];
    
    NSValue * forPValue = pointArray[2];
    CGPoint forP = [forPValue CGPointValue];
    
    CGPoint controlP1 = CGPointZero;
    
    
    if (pointArray.count == 4){
        
       forPValue = pointArray[3];
       forP = [forPValue CGPointValue];
  
    }
    
    
    //ilterSubType == 2~13,分别依次是低通的几个子类型
    // BUTTER_WORTH_1,BUTTER_WORTH_2，BESSEL_2，LINKWITZ_RILEY_2，VARIABLE_Q2，CHEBYCHEV
    if (filterSubType == 2 || filterSubType == 3) {
        // HIGH_PASS_BUTTER_WORTH_1,HIGH_PASS_BUTTER_WORTH_2
        
        CGFloat wt = (startP.x + secP.x)/2;
        
        controlP1.y = secP.y - 8;
        controlP1.x = wt - 45;
        
        [self moveToPoint:startP];
        [self addQuadCurveToPoint:secP controlPoint:controlP1];
        [self addLineToPoint:thrP];
        
    }else if (filterSubType == 4 ) {
        //    HIGH_PASS_BESSEL_2
        
        CGFloat wt = (startP.x + secP.x)/2;
        
        controlP1.y = secP.y + 10;
        controlP1.x = wt - 30;
        
        [self moveToPoint:startP];
        [self addQuadCurveToPoint:secP controlPoint:controlP1];
        [self addLineToPoint:thrP];
        
    }else if (filterSubType == 5 ) {
        //  case HIGH_PASS_LINKWITZ_RILEY_2
        
        CGFloat wt = (startP.x + secP.x)/2;
        
        controlP1.y = secP.y + 10;
        controlP1.x = wt;
        
        [self moveToPoint:startP];
        [self addQuadCurveToPoint:secP controlPoint:controlP1];
        [self addLineToPoint:thrP];
        
    }else if (filterSubType == 6 ) {
        //   case HIGH_PASS_VARIABLE_Q2
        
        CGFloat wt = (secP.x + thrP.x)/2;
        
        controlP1.y = secP.y - 18;
        controlP1.x = wt;
        
        [self moveToPoint:startP];
        [self addLineToPoint:secP];
        
        [self addQuadCurveToPoint:thrP controlPoint:controlP1];
        
        [self addLineToPoint:forP];
        
    }else if (filterSubType == 7 ) {
        
        //   case HIGH_PASS_CHEBYCHEV
        
        CGFloat wt = (secP.x + thrP.x)/2;
        
        controlP1.y = secP.y - 14;
        controlP1.x = wt ;
        
        [self moveToPoint:startP];
        [self addLineToPoint:secP];
        
        [self addQuadCurveToPoint:thrP controlPoint:controlP1];
        
        [self addLineToPoint:forP];
        
    } else if (filterSubType == 8 || filterSubType == 9 ) {
        // BUTTER_WORTH_1,BUTTER_WORTH_2

        CGFloat wt = (secP.x + thrP.x)/2;
        
        controlP1.y = secP.y;
        controlP1.x = wt;
        
        [self moveToPoint:startP];
        [self addLineToPoint:secP];
        
        [self addQuadCurveToPoint:thrP controlPoint:controlP1];
        
    }else if (filterSubType == 10 || filterSubType == 11)  {
        //BESSEL_2，LINKWITZ_RILEY_2

        CGFloat wt = (secP.x + thrP.x)/2;
        
        controlP1.y = secP.y;
        controlP1.x = wt;
        
        [self moveToPoint:startP];
        [self addLineToPoint:secP];
        
        [self addQuadCurveToPoint:thrP controlPoint:controlP1];
        
        [self addLineToPoint:forP];
    
    }else if (filterSubType == 12)  {
        //VARIABLE_Q2，
        CGFloat wt = (secP.x + thrP.x)/2;
        
        controlP1.y = secP.y - 8;
        controlP1.x = wt - 10;
        
        [self moveToPoint:startP];
        [self addLineToPoint:secP];
        
        [self addQuadCurveToPoint:thrP controlPoint:controlP1];
        
        [self addLineToPoint:forP];
        
    }else if (filterSubType == 13)  {
        //CHEBYCHEV

        CGFloat wt = (secP.x + thrP.x)/2;
        
        controlP1.y = secP.y - 4;
        controlP1.x = wt - 15;
        
        [self moveToPoint:startP];
        [self addLineToPoint:secP];
        
        [self addQuadCurveToPoint:thrP controlPoint:controlP1];
        
        [self addLineToPoint:forP];
        
    }else if (filterSubType == 99) {
    
        [self moveToPoint:startP];
        [self addLineToPoint:secP];
        [self addLineToPoint:thrP];
        [self addLineToPoint:forP];
    
    
    }


//    for (int i = 0; i < pointArray.count; i++) {
//        if (i < pointArray.count-1){
//            
//            //            startP = pointArray[i];
//            //            endP = pointArray[i+1];
//            NSValue * startPValue = pointArray[i];
//            CGPoint startP = [startPValue CGPointValue];
//            
//            NSValue * endPValue = pointArray[i+1];
//            CGPoint endP = [endPValue CGPointValue];
//            
//            
//            CGFloat wt = (startP.x + endP.x)/2;
//            
//            //分几种情况，过滤器不同，控制点不一样
//            if (filterSubType == 8 || filterSubType == 9 ) {
//                
//                CGPoint controlP1 = CGPointZero;
//                
//                CGPoint controlP2 = CGPointZero;
//                
//                controlP1.y = startP.y;
//                controlP1.x = wt;
//
//                [self moveToPoint:CGPointMake(startP.x, startP.y)];
//                [self addQuadCurveToPoint:endP controlPoint:controlP1];
//
//                
//
//            }else {
//            
//                CGPoint controlP1 = CGPointZero;
//                
//                CGPoint controlP2 = CGPointZero;
//                
//                controlP1.y = startP.y;
//                controlP1.x = wt;
//                
//                controlP2.y = endP.y;
//                controlP2.x = wt;
//                
//                [self moveToPoint:CGPointMake(startP.x, startP.y)];
//                
//                [self addCurveToPoint:endP controlPoint1:controlP1 controlPoint2:controlP2];
//              
//            
//            }
//            
//           
//        }
//    }
}

@end
