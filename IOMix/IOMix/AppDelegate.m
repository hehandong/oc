//
//  AppDelegate.m
//  IOMix
//
//  Created by Chiwen on 2015/6/15.
//  Copyright (c) 2015年 Homni Enterprises Co., Ltd. All rights reserved.
//

#import "AppDelegate.h"
#import "HNTabBarViewController.h"
#import "HNIOMixManager.h"
#import "HNChannelStore.h"
#import "HNChannel.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    self.window.rootViewController = [HNTabBarViewController new];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
 
#pragma mark - 退回背景:数据本地保存
    // 保存HOME页面数据
    for (HNChannel *channel in [HNIOMixManager shareManager].allChannel) {
        [[HNIOMixManager shareManager].store updateHomeVolume:channel.volume sSYNC:channel.isSYNC eqName:channel.customEQName isMUTE:channel.isMUTE forChannel:channel.channelIndex];
    }
    // 保存EQ页面数据
    for (HNChannel *channel in [HNIOMixManager shareManager].allChannel) {
        [[HNIOMixManager shareManager].store updateEqualizerValues:channel.equalizer effectMode:channel.effectMode forChannel:channel.channelIndex];
    }
    // 保存FLITER页面数据
    for (HNChannel *channel in [HNIOMixManager shareManager].allChannel) {
        [[HNIOMixManager shareManager].store updateFilterPitchType:channel.pitchType bassGainValue:channel.bassGainValue trebleGainValue:channel.trebleGainValue passType:channel.passType highPassSubType:channel.highPassSubType highPassfrequencyValue:channel.highPassfrequencyValue isAllpassForHighpass:channel.isAllPassForHighPass lowPassSubType:channel.lowPassSubType lowPassfrequencyValue:channel.lowPassfrequencyValue isAllpassForLowpass:channel.isAllPassForLowhPass forChannel:channel.channelIndex];
    }
    // 保存MONITOR页面数据
    HNChannel *channelOne = [HNIOMixManager shareManager].allChannel[0];
    [[HNIOMixManager shareManager].store updateMonitorLrInputValue:channelOne.lrInputValue mainInputValue:channelOne.mainInputValue auxInputValue:channelOne.auxInputValue forChannel:channelOne.channelIndex];
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
#pragma mark - 程序终止:数据本地保存
    // 保存HOME页面数据
    for (HNChannel *channel in [HNIOMixManager shareManager].allChannel) {
        [[HNIOMixManager shareManager].store updateHomeVolume:channel.volume sSYNC:channel.isSYNC eqName:channel.customEQName isMUTE:channel.isMUTE forChannel:channel.channelIndex];
    }
    // 保存EQ页面数据
    for (HNChannel *channel in [HNIOMixManager shareManager].allChannel) {
        [[HNIOMixManager shareManager].store updateEqualizerValues:channel.equalizer effectMode:channel.effectMode forChannel:channel.channelIndex];
    }
    // 保存FLITER页面数据
    for (HNChannel *channel in [HNIOMixManager shareManager].allChannel) {
        [[HNIOMixManager shareManager].store updateFilterPitchType:channel.pitchType bassGainValue:channel.bassGainValue trebleGainValue:channel.trebleGainValue passType:channel.passType highPassSubType:channel.highPassSubType highPassfrequencyValue:channel.highPassfrequencyValue isAllpassForHighpass:channel.isAllPassForHighPass lowPassSubType:channel.lowPassSubType lowPassfrequencyValue:channel.lowPassfrequencyValue isAllpassForLowpass:channel.isAllPassForLowhPass forChannel:channel.channelIndex];
    }
    // 保存MONITOR页面数据
    HNChannel *channelOne = [HNIOMixManager shareManager].allChannel[0];
    [[HNIOMixManager shareManager].store updateMonitorLrInputValue:channelOne.lrInputValue mainInputValue:channelOne.mainInputValue auxInputValue:channelOne.auxInputValue forChannel:channelOne.channelIndex];
}

@end
