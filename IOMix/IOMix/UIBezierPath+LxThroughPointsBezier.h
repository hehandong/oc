//
//  UIBezierPath+LxThroughPointsBezier.h
//  LxThroughPointsBezierDemo
//

#import <UIKit/UIKit.h>

@interface UIBezierPath (LxThroughPointsBezier)

/**
 *  The curve‘s bend level. The good value is about 0.6 ~ 0.8. The default and recommended value is 0.7.
 */
@property (nonatomic) CGFloat contractionFactor;

/**
 *  You must wrap CGPoint struct to NSValue object.
 *
 *  @param pointArray Points you want to through. You must give at least 1 point for drawing curve.
 */
- (void)addBezierThroughPoints:(NSArray *)pointArray;


/**
 *  为Filter页面定制的绘图方法
 *
 *  @param pointArray 传入的点坐标数组
 */
- (void)addBezierThroughFilterPoints:(NSArray *)pointArray filterSubType:(NSInteger)filterSubType;

@end
