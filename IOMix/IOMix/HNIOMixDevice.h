//
//  HNIOMixDevice.h
//  IOMix
//
//  Created by YuHeng_Antony on 11/9/15.
//  Copyright © 2015 Homni Enterprises Co., Ltd. All rights reserved.
//
/// 负责和硬件通讯
#import <Foundation/Foundation.h>

// 协议/protocol
@protocol HNIOMixDeviceDelegate <NSObject>

@optional
/**
 *  通知delegate tcp已经连上
 */
- (void)deviceDidConnect;

/**
 *  通知delegate tcp已经断开
 */
- (void)deviceDidDisconnect;

// TODO:如果APP不需要硬件返回的数据，下面这个方法都可以删除
/// 收到从硬件传回的home页面数据,要更新到界面上(command表暂时没有实现这一部分)
- (void)homeParamDidUpdate:(NSData *)param;

/// 收到从硬件传回的EQ页面数据
- (void)EQParamDidUpdate:(NSData *)param;

/// 收到从硬件传回的filter页面数据
- (void)filterParamDidUpdate:(NSData *)param;

/// 收到从硬件传回的monitor页面数据
- (void)monitorParamDidUpdate:(NSData *)param;

@end

#pragma mark -
@interface HNIOMixDevice : NSObject

#pragma mark 属性
// IOMix's ip
@property (nonatomic, copy) NSString *targetIp;

// 声明委托对象
@property (weak) id<HNIOMixDeviceDelegate> delegate;

#pragma mark 方法
+ (instancetype)deviceWithIP:(NSString *)ip;

/**
 *  手动断开链接
 */
- (void)disconnect;

#pragma mark 指令方法
/**
 *  设置Monitor界面输出通道
 */
- (void)setupOutputChannel:(NSInteger)outputChannelIndex inputChannel:(NSInteger)inputCannelIndex;

/**
 *  设置Home界面音量
 */
- (void)setupVolumeForChannel:(NSUInteger)channelIndex volumeValue:(NSInteger)volumeValue immediately:(BOOL)immediately;

/**
 *  sync状态发送音量的指令
 *
 *  @param channelA    被sync的channel之一
 *  @param channelB    被sync的channel之一
 *  @param volumeValue 音量值
 *  @param immediately 是否马上发送
 */
- (void)setupSyncVolumeForChannelA:(NSInteger)channelA channelB:(NSInteger)channelB volumeValue:(NSInteger)volumeValue immediately:(BOOL)immediately;

/// 无延时的音量指令(用于MUTE打开、关闭时即刻发送)(非sync状态)
- (void)setupNonDelayVolumeForChannel:(NSUInteger)channelIndex volumeValue:(NSInteger)volumeValue;

/// 无延时的音量指令(用于MUTE打开、关闭时即刻发送)(sync状态)
- (void)setupNonDelayVolumeForChannelA:(NSUInteger)channelA channelB:(NSInteger)channelB volumeValue:(NSInteger)volumeValue;

/**
 *  设置EQ页面数值
 */
- (void)setupEQForChannel:(NSUInteger)channelIndex frequencyIndex:(NSUInteger)frequencyIndex gainValue:(NSInteger)gainValue immediately:(BOOL)immediately;

/**
 *  设置EQ页面音效(8种选择一种)
 */
- (void)setupEffectForChannel:(NSUInteger)channelIndex effectIndex:(NSUInteger)effectIndex;

/**
 *  设置低音(Filter页面)
 */
- (void)setupBassForChannel:(NSInteger)channelIndex bassGainValue:(NSInteger)bassGainValue;

/**
 *  设置高音(Filter页面)
 */
- (void)setupTrebleForChannel:(NSInteger)channelIndex trebleGainValue:(NSInteger)trebleGainValue;

/**
 *  设置高通(Filter页面)
 */
- (void)setupHighPassForChannel:(NSInteger)channelIndex filterSubtypeMode:(NSInteger)filterSubtypeMode frequencyValue:(NSInteger)frequencyValue immediately:(BOOL)immediately;

/**
 *  设置低通(Filter页面)
 */
- (void)setupLowPassForChannel:(NSInteger)channelIndex filterSubtypeMode:(NSInteger)filterSubtypeMode frequencyValue:(NSInteger)frequencyValue immediately:(BOOL)immediately;

/**
 *  设置SSID
 */
- (void)setupSSID:(NSString *)ssidString password:(NSString *)passwordString;

/// 同步输入通道(Monitor)
- (void)syncLrInputValue:(NSInteger)lrInputValue mainInputValue:(NSInteger)mainInputValue auxInputValue:(NSInteger)auxInputValue;

/// 同步音量
- (void)syncVolume:(NSArray *)volumes;

/// 同步EQ
- (void)syncEQ:(NSInteger)eq1
           eq2:(NSInteger)eq2
           eq3:(NSInteger)eq3
           eq4:(NSInteger)eq4
           eq5:(NSInteger)eq5
           eq6:(NSInteger)eq6
           eq7:(NSInteger)eq7
           eq8:(NSInteger)eq8
           eq9:(NSInteger)eq9
          eq10:(NSInteger)eq10
    forChannel:(NSInteger)channelIndex;

/// 同步低音
- (void)syncBass:(NSArray *)BassValues;

/// 同步高音
- (void)syncTreble:(NSArray *)TrebleValues;

/// 同步高通
- (void)syncHighPass:(NSArray *)HighpassValues highPassSubTypes:(NSArray *)highPassSubTypes;

/// 同步低通
- (void)syncLowPass:(NSArray *)LowpassValues lowPassSubType:(NSArray *)lowPassSubTypes;

@end
