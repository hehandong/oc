//
//  HNEqualizer.h
//  IOMix
//
//  Created by YuHeng_Antony on 15/7/10.
//  Copyright (c) 2015年 Homni Enterprises Co., Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HNEqualizer : NSObject<NSCoding>
@property (nonatomic) NSInteger channelTag;
@property (nonatomic) NSInteger Hz20;
@property (nonatomic) NSInteger Hz50;
@property (nonatomic) NSInteger Hz100;
@property (nonatomic) NSInteger Hz200;
@property (nonatomic) NSInteger Hz500;
@property (nonatomic) NSInteger K1;
@property (nonatomic) NSInteger K2;
@property (nonatomic) NSInteger K5;
@property (nonatomic) NSInteger K10;
@property (nonatomic) NSInteger K20;
@end
