//
//  transformUtil.swift
//  IOMix
//
//  Created by homni_rd01 on 16/1/4.
//  Copyright © 2016年 Homni Enterprises Co., Ltd. All rights reserved.
//

import Foundation

// Helper Method(将20-20K转换为硬件需要的0-199)
//- (NSInteger)transformValue:(NSInteger)frequencyRawValue {
//    NSInteger commandValue;
//    if (frequencyRawValue <= 1100) {
//        commandValue = (frequencyRawValue - 20) / 10;
//    } else if (frequencyRawValue >= 1200 && frequencyRawValue <= 10000) {
//        commandValue = (frequencyRawValue - 1200) / 100 + 100;
//    } else {
//        commandValue = (frequencyRawValue - 10000) / 1000 + 188;
//    }
//    return commandValue;
//}


// Helper Method(将20-20K转换为硬件需要的0-199)
func fcTransformInt(frequencyRawValue:Int)->Int{
    var commandValue:Int = 0
    if (frequencyRawValue < 1100) {
            commandValue = (frequencyRawValue - 20) / 10;
        } else if (frequencyRawValue >= 1200 && frequencyRawValue <= 10000) {
            commandValue = (frequencyRawValue - 1200) / 100 + 100;
        } else {
            commandValue = (frequencyRawValue - 10000) / 1000 + 188;
        }
    return commandValue;
}


// Helper Method(将硬件需要的0-199转换为20-20K)
func intTransformFc(value:Int)->Int{
    var fcValue:Int = 0
    
    if (value < 108) {
        fcValue = value * 10 + 20
    } else if (value >= 108 && value <= 188) {
        fcValue = (value - 100) * 100 + 1200
    } else {
//        commandValue = (frequencyRawValue - 10000) / 1000 + 188;
        fcValue = (value - 188) * 1000 + 10000
        
        if fcValue > 20000{
        fcValue = 20000
        }
    }
    return fcValue;
}
