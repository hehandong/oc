//
//  HNIOMixManager.h
//  IOMix
//
//  Created by YuHeng_Antony on 11/9/15.
//  Copyright © 2015 Homni Enterprises Co., Ltd. All rights reserved.
//
/**
 *  负责桥接硬件和控制器的通讯
 */
#import <Foundation/Foundation.h>
@import UIKit;
@class HNIOMixDevice;
@class HNChannelStore;
@class HNChannel;

@interface HNIOMixManager : NSObject

/**
 *  负责和硬件的通讯
 */
@property (nonatomic, strong) HNIOMixDevice *device;

/**
 *  其他类从这里获取硬件是否链接
 */
@property (nonatomic) BOOL isConnected;

/**
 *  负责本地数据的保存
 */
@property (nonatomic, strong) HNChannelStore *store;

/**
 *  所有的channel
 */
@property (nonatomic, copy) NSArray *allChannel;

/**
 *  获取当前正在使用、编辑的channel
 */
@property (nonatomic, strong) HNChannel *channelInUse;


+ (instancetype)shareManager;

@end
