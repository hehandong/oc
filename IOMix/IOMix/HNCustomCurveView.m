//
//  BNRHypnosisView.m
//  Hypnosister
//
//  Created by John Gallagher on 1/6/14.
//  Copyright (c) 2014 John Gallagher. All rights reserved.
//

#import "HNCustomCurveView.h"

@interface HNCustomCurveView ()
@property (nonatomic, strong) UIBezierPath *path;
@property (nonatomic, assign) CGPoint location;
@end

@implementation HNCustomCurveView

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    [[UIColor colorWithRed:0.96 green:0.6 blue:0.08 alpha:1.0] setStroke];// 橙色
    
    // 獲取「繪圖」區域
    CGRect bounds = self.bounds;

    // 實例化UIBezierPath對象
    _path = [[UIBezierPath alloc] init];

    [_path moveToPoint:CGPointMake(bounds.origin.x + 28, bounds.size.height / 2 )];// 起点
    [_path addLineToPoint:CGPointMake(bounds.size.width - 10, bounds.size.height / 2)];// 终点
    
//    [_path addCurveToPoint:CGPointMake(bounds.size.width - 10, bounds.size.height / 2)
//             controlPoint1:CGPointMake(bounds.size.width - 10, bounds.size.height / 2 )
//             controlPoint2:CGPointMake(bounds.size.width - 10, bounds.size.height / 2 )];
    
    _path.lineWidth = 2;// 线宽
    
    // Draw the line!
    [_path stroke];// 提交，畫出線條
    
    
    // 計算出中心點
    // Figure out the center of the bounds rectangle
    CGPoint center;
    center.x = bounds.origin.x + bounds.size.width / 2.0;
    center.y = bounds.origin.y + bounds.size.height / 2.0;
}

//- (void)setCircleColor:(UIColor *)circleColor
//{
//    _circleColor = circleColor;
//    [self setNeedsDisplay];
//}

// When a finger touches the screen
//- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
//{
//    UITouch *touch = [touches anyObject];
//    CGPoint touchPoint = [touch locationInView:self];
//    touchPoint.y = self.bounds.size.height - touchPoint.y;
//    if ([_path containsPoint:touchPoint])
//    {
//        // do stuff
//        NSLog(@"接触到了线");
//    }
//}

- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    self.location = [touch locationInView:self];
}

@end
