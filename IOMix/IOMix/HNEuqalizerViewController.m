//
//  HNEuqalizerViewController.m
//  IOMix
//
//  Created by YuHeng_Antony on 15/7/8.
//  Copyright (c) 2015年 Homni Enterprises Co., Ltd. All rights reserved.
//

#import "HNEuqalizerViewController.h"
#import "HNChannelStore.h"
#import "HNChannel.h"
#import "HNEqualizer.h"
#import "HNPublic.h"
#import "HNCurveDrawer.h"
#import "HNIOMixManager.h"
#import "HNIOMixDevice.h"

typedef NS_ENUM(NSInteger, EQ_EFFECT) {
    EQ_EFFECT_ROCK = 0,
    EQ_EFFECT_POP,
    EQ_EFFECT_JAZZ,
    EQ_EFFECT_BLUES,
    EQ_EFFECT_CLASSICAL,
    EQ_EFFECT_REGGAE,
    EQ_EFFECT_HIPHOP,
    EQ_EFFECT_RAP,
    EQ_EFFECT_FLAT,
    EQ_EFFECT_NON,
};

@interface HNEuqalizerViewController ()
@property (strong, nonatomic) IBOutletCollection(UISlider) NSArray *eqSliders;
@property (strong, nonatomic) IBOutletCollection(UILabel ) NSArray *dBLabels;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *channelButtons;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *eqEffectButtons;
@property (weak, nonatomic) IBOutlet UIView *curveView;
@property (copy, nonatomic) NSArray *allChannels;
@property (strong, nonatomic) HNChannel *channelInUse;
// 音效预设值数组
@property (copy, nonatomic) NSArray *eqEffectss;
@end

@implementation HNEuqalizerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // 從Model獲取數據
    _allChannels = [HNIOMixManager shareManager].allChannel;
    [self layoutSliders];
    [self layoutCurve];
    // 默认CH1点亮
    [self toggleChannel:_channelButtons[0]];
    
    // 预设的9个音效值
    [self setupEqModelsData];
}

- (void)setupEqModelsData {
    _eqEffectss = @[@[@8, @10, @12, @15, @16, @10, @11, @12, @20, @12],
                  @[@12, @16, @20, @17, @9, @7, @13, @15, @18, @15],
                  @[@12, @12, @12, @12, @16, @16, @12, @16, @18, @16],
                  @[@9, @10, @13, @15, @13, @12, @12, @14, @10, @8],
                  @[@12, @12, @16, @20, @14, @12, @12, @12, @14, @14],
                  @[@10, @11, @12, @12, @9, @14, @17, @14, @16, @12],
                  @[@12, @14, @16, @16, @12, @10, @12, @14, @14, @12],
                  @[@10, @10, @13, @15, @14, @10, @12, @15, @20, @18],
                  @[@12, @12, @12, @12, @12, @12, @12, @12, @12, @12]];
}

#pragma mark - 绘制曲线
- (void)layoutCurve {
    // 调用绘图类，传入10个点进行绘图(10个点要自己根据curveView及相关比例计算出来)
    HNCurveDrawer *drawer = [[HNCurveDrawer alloc] init];
    drawer.curveView = self.curveView;
    drawer.eqSliders = self.eqSliders;
    
    NSMutableArray *points = [NSMutableArray new];
    CGRect bounds = _curveView.bounds;
    
//    for (int i = 0; i < 3; i++) {
//        CGPoint pointA = CGPointMake(HN_LEADING_VALUE +
//                                     HN_V_GROUP_SPACE_VC * i,
//                                     bounds.size.height / 2);
//        NSValue *pointAValue = [NSValue valueWithCGPoint:pointA];
//        [points addObject:pointAValue];
//        
//        CGPoint pointB = CGPointMake(HN_LEADING_VALUE +
//                                     HN_GET_V_SPACE_WITH_VC(HN_V_SPACE_L1toL2) +
//                                     HN_GET_V_SPACE_WITH_VC(HN_V_SPACE_L2toL3) +
//                                     HN_GET_V_SPACE_WITH_VC(HN_V_SPACE_L3toL4) +
//                                     HN_V_GROUP_SPACE_VC * i,
//                                     bounds.size.height / 2);
//        NSValue *pointBValue = [NSValue valueWithCGPoint:pointB];
//        [points addObject:pointBValue];
//        
//        CGPoint pointC = CGPointMake(HN_LEADING_VALUE +
//                                     HN_GET_V_SPACE_WITH_VC(HN_V_SPACE_L1toL2) +
//                                     HN_GET_V_SPACE_WITH_VC(HN_V_SPACE_L2toL3) +
//                                     HN_GET_V_SPACE_WITH_VC(HN_V_SPACE_L3toL4) +
//                                     HN_GET_V_SPACE_WITH_VC(HN_V_SPACE_L4toL5) +
//                                     HN_GET_V_SPACE_WITH_VC(HN_V_SPACE_L5toL6) +
//                                     HN_GET_V_SPACE_WITH_VC(HN_V_SPACE_L6toL7) +
//                                     HN_GET_V_SPACE_WITH_VC(HN_V_SPACE_L7toL8) +
//                                     HN_GET_V_SPACE_WITH_VC(HN_V_SPACE_L8toL9) +
//                                     HN_V_GROUP_SPACE_VC * i,
//                                     bounds.size.height / 2);
//        NSValue *pointCValue = [NSValue valueWithCGPoint:pointC];
//        [points addObject:pointCValue];
//    }
//    
//    CGPoint pointZ = CGPointMake(HN_V_GROUP_SPACE_VC * 3, bounds.size.height / 2);
//    NSValue *pointZValue = [NSValue valueWithCGPoint:pointZ];
//    [points addObject:pointZValue];
    
    
    
    // 正确的20K的X点,Y点
//    CGPoint pointA = CGPointMake(HN_LEADING_VALUE + HN_V_GROUP_SPACE_VC * 3, bounds.size.height / 2);
    
    // 因为原来设计图没有画正确，导致容纳不下10个点
    // 平均分配10个点,取前面9个点
    for (int i = 0; i < 10; i++) {
        CGPoint point = CGPointMake(HN_LEADING_VALUE + ((HN_LEADING_VALUE + HN_V_GROUP_SPACE_VC * 3 - HN_LEADING_VALUE ) / 9) * i, bounds.size.height / 2);
        
        if (i < 9) {
            NSValue *pointAValue = [NSValue valueWithCGPoint:point];
            [points addObject:pointAValue];
        }
    }
    
    CGPoint pointZ = CGPointMake(HN_V_GROUP_SPACE_VC * 3, bounds.size.height / 2);
    NSValue *pointZValue = [NSValue valueWithCGPoint:pointZ];
    [points addObject:pointZValue];
    
    
    // 平均分配10个点
//    for (int i = 0; i < 10; i++) {
//        CGPoint point = CGPointMake(HN_LEADING_VALUE + ((HN_V_GROUP_SPACE_VC * 3 - HN_LEADING_VALUE ) / 9) * i, bounds.size.height / 2);
//        
//        NSValue *pointAValue = [NSValue valueWithCGPoint:point];
//        [points addObject:pointAValue];
//    }
    
    // 调用绘图类绘制曲线(传入要绘制的点坐标即可)
    [drawer drawCurveFor:HN_CURVE_EQ filterSubType:0 UsePoints:points];
    
    // TODO:应该不用枚举，直接用穿过来的tag索引到对应的slider即可
    // 触摸点的Y值改变后，要重新确定slider的值(通过触摸点来控制slider的值)
    [[NSNotificationCenter defaultCenter] addObserverForName:kNotificationEQPointControlYValueChanged object:nil queue:nil usingBlock:^(NSNotification * _Nonnull note) {
        [_eqSliders enumerateObjectsUsingBlock:^(UISlider *_Nonnull slider, NSUInteger idx, BOOL * _Nonnull stop) {
            if (slider.tag == [note.userInfo[EQPointControlTag] integerValue]) {
#pragma mark 触摸点的Y值转换slider的值
                // 算出比列
                float scale = ([note.userInfo[EQPointControlValue] floatValue] - HN_TOP_VALUE) / (HN_BOTTOM_VALUE - HN_TOP_VALUE);
                //
                float sliderValue = 24 * (1.0 - scale);
                [slider setValue:sliderValue animated:YES];
                [_dBLabels[idx] setText:[NSString stringWithFormat:@"%ddB", (int)sliderValue - 12]];

#pragma mark 发送0x12EQ控制指令
                [self sendCommandFor:slider immediately:NO];
                // 更新数据，否则手绘的曲线不能保存
                [self saveEQData:(NSInteger)sliderValue ForEQSlider:slider.tag];
                
                // 触摸点滑动了，预设音效按钮都要灭掉
                _channelInUse.effectMode = EQ_EFFECT_NON;
                [self setupEffectButtonState];
            }
        }];
    }];
}

#pragma mark - layoutSliders
- (void)layoutSliders {

    // 初始化slider
    for (UISlider *slider in _eqSliders) {
        [slider removeConstraints:slider.constraints];
        slider.transform = CGAffineTransformMakeRotation(M_PI * 1.5);
        [slider setThumbImage:[UIImage imageNamed:@"sliderButton"] forState:UIControlStateNormal];
        [slider setMaximumTrackImage:[UIImage new] forState:UIControlStateNormal];
        [slider setMinimumTrackImage:[UIImage new] forState:UIControlStateNormal];
        [slider addTarget:self action:@selector(sliderValueDidChange:) forControlEvents:UIControlEventValueChanged];
        slider.minimumValue = 0.0;
        slider.maximumValue = 24.0;
        // 手指离开slider时的动作
        [slider addTarget:self action:@selector(sliderTouchUpOutsideEvent:) forControlEvents:UIControlEventTouchUpOutside];
        [slider addTarget:self action:@selector(sliderTouchUpInsideEvent:) forControlEvents:UIControlEventTouchUpInside];
        
        slider.translatesAutoresizingMaskIntoConstraints = NO;
        NSArray *sliderVerticalConstraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[slider]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(slider)];
        // 注意：要設置翻轉90度slider的「高度」，其實就是設置未翻轉前slider的「寬度」
        NSLayoutConstraint *sliderHeightConstraint = [NSLayoutConstraint constraintWithItem:slider attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:slider.superview attribute:NSLayoutAttributeHeight multiplier:1 constant:0.0];
        NSLayoutConstraint *sliderCenterXConstraint = [NSLayoutConstraint constraintWithItem:slider attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:slider.superview attribute:NSLayoutAttributeCenterX multiplier:1 constant:0.0];
        [slider.superview addConstraints:sliderVerticalConstraints];
        [slider.superview addConstraint:sliderHeightConstraint];
        [slider.superview addConstraint:sliderCenterXConstraint];
    }
}

#pragma mark - sliderValueChange
- (void)sliderValueDidChange:(UISlider *)sender {
    
    // slider值滑动了，预设音效按钮都要灭掉
    _channelInUse.effectMode = EQ_EFFECT_NON;
    [self setupEffectButtonState];
    
    // 绑定dBLabel的值
    // dB值只能是整数，所以强转为整数
    for (UILabel *dbLabel in _dBLabels) {
        if (dbLabel.tag == sender.tag) {
            dbLabel.text = [NSString stringWithFormat:@"%@dB", [@((int)sender.value - 12) stringValue]];
        }
    }
    
    // 发送通告，重新绘曲线图
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationEQSliderValueChanged object:nil userInfo:@{EQSliderValue:[NSNumber numberWithInt:(int)sender.value], EQSliderTag:[NSNumber numberWithInteger:sender.tag]}];
    
    // 储存dB值
    [self saveEQData:(NSInteger)sender.value ForEQSlider:sender.tag];
#pragma mark 发送0x12EQ控制指令
    [self sendCommandFor:sender immediately:NO];
}

#pragma mark sliderValueChange Helper Method
- (void)sliderTouchUpOutsideEvent:(UISlider *)sender {
#pragma mark 发送0x11音量控制指令
    // 即刻发送(用于手指离开slider后最后一次发送)
    [self sendCommandFor:sender immediately:YES];
    
}

- (void)sliderTouchUpInsideEvent:(UISlider *)sender {
    // 即刻发送(用于手指离开slider后最后一次发送)
#pragma mark 发送0x11音量控制指令
    [self sendCommandFor:sender immediately:YES];
}

- (void)sendCommandFor:(UISlider *)sender immediately:(BOOL)immediately {
    // TODO:将slider的tag值设置回0-9(xib中)，减少这种减211的运算
    [[HNIOMixManager shareManager].device setupEQForChannel:[HNIOMixManager shareManager].channelInUse.channelIndex frequencyIndex:sender.tag - 211 gainValue:sender.value immediately:immediately];
}

- (void)saveEQData:(NSInteger)eqValue ForEQSlider:(NSInteger)sliderIndex {
    switch (sliderIndex) {
        case EQSlider20Hz:
            _channelInUse.equalizer.Hz20  = eqValue;
            break;
        case EQSlider50Hz:
            _channelInUse.equalizer.Hz50  = eqValue;
            break;
        case EQSlider100Hz:
            _channelInUse.equalizer.Hz100 = eqValue;
            break;
        case EQSlider200Hz:
            _channelInUse.equalizer.Hz200 = eqValue;
            break;
        case EQSlider500Hz:
            _channelInUse.equalizer.Hz500 = eqValue;
            break;
        case EQSlider1K:
            _channelInUse.equalizer.K1    = eqValue;
            break;
        case EQSlider2K:
            _channelInUse.equalizer.K2    = eqValue;
            break;
        case EQSlider5K:
            _channelInUse.equalizer.K5    = eqValue;
            break;
        case EQSlider10K:
            _channelInUse.equalizer.K10   = eqValue;
            break;
        case EQSlider20K:
            _channelInUse.equalizer.K20   = eqValue;
            break;
        default:
            break;
    }
}

#pragma mark - toggleChannel
- (IBAction)toggleChannel:(UIButton *)sender {
    
    // 切换channel,设置channelInUse
    [HNIOMixManager shareManager].channelInUse = [HNIOMixManager shareManager].allChannel[sender.tag];
    _channelInUse = [HNIOMixManager shareManager].channelInUse;
    
    // 配置effectButton状态
    [self setupEffectButtonState];
    
    // 切换channel后,正确配置slider及曲线图
    [self setupSliderAndCurve];
    
    // 被点击的channle按钮高亮，其他灭掉
    for (UIButton *button in _channelButtons) {
        if (sender.tag == button.tag) {
            sender.selected = YES;
        } else {
            button.selected = NO;
        }
    }
}

#pragma mark toggleChannel Helper Method(setup sliders value)
// 1、切换channel后绘制曲线、更改slidre值、更改db值
// 2、选择预设音效后绘制曲线、更改slidre值、更改db值
- (void)setupSliderAndCurve {
    [self setupSlider:0 value:_channelInUse.equalizer.Hz20 tag:EQSlider20Hz];
    [self setupSlider:1 value:_channelInUse.equalizer.Hz50 tag:EQSlider50Hz];
    [self setupSlider:2 value:_channelInUse.equalizer.Hz100 tag:EQSlider100Hz];
    [self setupSlider:3 value:_channelInUse.equalizer.Hz200 tag:EQSlider200Hz];
    [self setupSlider:4 value:_channelInUse.equalizer.Hz500 tag:EQSlider500Hz];
    [self setupSlider:5 value:_channelInUse.equalizer.K1 tag:EQSlider1K];
    [self setupSlider:6 value:_channelInUse.equalizer.K2 tag:EQSlider2K];
    [self setupSlider:7 value:_channelInUse.equalizer.K5 tag:EQSlider5K];
    [self setupSlider:8 value:_channelInUse.equalizer.K10 tag:EQSlider10K];
    [self setupSlider:9 value:_channelInUse.equalizer.K20 tag:EQSlider20K];
}

- (void)setupSlider:(NSInteger)index value:(NSInteger)value tag:(NSInteger)tag {
    // 设置slider的值
    [_eqSliders[index] setValue:value animated:YES];
    // 设置db值(dB值范围为-12到12)
    [_dBLabels[index] setText:[NSString stringWithFormat:@"%@dB", [@(value - 12) stringValue]]];
    // 设置曲线
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationEQSliderValueChanged
                                                        object:nil
                                                      userInfo:@{EQSliderValue:[NSNumber numberWithInt:(int)[(UISlider *)_eqSliders[index] value]], EQSliderTag:[NSNumber numberWithInteger:tag]}];
    
}

#pragma mark toggleChannel Helper Method(setup effectButton State)
- (void)setupEffectButtonState {
    for (UIButton *effectButton in _eqEffectButtons) {
        if (effectButton.tag == _channelInUse.effectMode) {
            effectButton.selected = YES;
        } else {
            effectButton.selected = NO;
        }
    }
}

#pragma mark - EQEffectToggle
- (IBAction)toggleEQModel:(UIButton *)sender {
    // 设置按钮状态
    for (UIButton *button in _eqEffectButtons) {
        if (sender.tag == button.tag) {
            sender.selected = YES;
        } else {
            button.selected = NO;
        }
    }
    
    // 更新数据(记录选择的音效索引)(旧指令用于发送0x13指令，现在用来记录选择的音效按钮)
    _channelInUse.effectMode = sender.tag;

#pragma mark 发送0x52音效控制指令
    // Refactor with simple arguments
    NSArray *eqValues = [NSMutableArray array];
    switch (sender.tag) {
        case EQ_EFFECT_ROCK:
            eqValues = _eqEffectss[0];
            break;
        case EQ_EFFECT_POP:
            eqValues = _eqEffectss[1];
            break;
        case EQ_EFFECT_JAZZ:
            eqValues = _eqEffectss[2];
            break;
        case EQ_EFFECT_BLUES:
            eqValues = _eqEffectss[3];
            break;
        case EQ_EFFECT_CLASSICAL:
            eqValues = _eqEffectss[4];
            break;
        case EQ_EFFECT_REGGAE:
            eqValues = _eqEffectss[5];
            break;
        case EQ_EFFECT_HIPHOP:
            eqValues = _eqEffectss[6];
            break;
        case EQ_EFFECT_RAP:
            eqValues = _eqEffectss[7];
            break;
        case EQ_EFFECT_FLAT:
            eqValues = _eqEffectss[8];
            break;
        default:
            return;
    }
    // 发送指令
    [self syncEQs:eqValues forChannel:_channelInUse.channelIndex];
    // 数据本地保存/更新界面
    [self updateDataAndUI:eqValues];
}

- (void)syncEQs:(NSArray *)eqValues forChannel:(NSUInteger)channel
{
    [[HNIOMixManager shareManager].device syncEQ:[eqValues[0] integerValue]
                                             eq2:[eqValues[1] integerValue]
                                             eq3:[eqValues[2] integerValue]
                                             eq4:[eqValues[3] integerValue]
                                             eq5:[eqValues[4] integerValue]
                                             eq6:[eqValues[5] integerValue]
                                             eq7:[eqValues[6] integerValue]
                                             eq8:[eqValues[7] integerValue]
                                             eq9:[eqValues[8] integerValue]
                                            eq10:[eqValues[9] integerValue]
                                      forChannel:channel];
}

- (void)updateDataAndUI:(NSArray *)eqValues {
    _channelInUse.equalizer.Hz20  = [eqValues[0] integerValue];
    _channelInUse.equalizer.Hz50  = [eqValues[1] integerValue];
    _channelInUse.equalizer.Hz100 = [eqValues[2] integerValue];
    _channelInUse.equalizer.Hz200 = [eqValues[3] integerValue];
    _channelInUse.equalizer.Hz500 = [eqValues[4] integerValue];
    _channelInUse.equalizer.K1    = [eqValues[5] integerValue];
    _channelInUse.equalizer.K2    = [eqValues[6] integerValue];
    _channelInUse.equalizer.K5    = [eqValues[7] integerValue];
    _channelInUse.equalizer.K10   = [eqValues[8] integerValue];
    _channelInUse.equalizer.K20   = [eqValues[9] integerValue];
    // 根据预设值更新界面
    [self setupSliderAndCurve];
}

@end
