//
//  HNChannelStore.m
//  IOMix
//
//  Created by YuHeng_Antony on 15/7/10.
//  Copyright (c) 2015年 Homni Enterprises Co., Ltd. All rights reserved.
//

#import "HNChannelStore.h"
#import "HNChannel.h"
#import "HNEqualizer.h"
#import "FMDB.h"
#import "HNBaseCommand.h"

#pragma mark sql语句常量值
static NSString *kTableName              = @"channle";
static NSString *kChannelID              = @"channelID";
// 18个栏位
static NSString *kVolume                 = @"volume";
static NSString *kIsSYNC                 = @"isSYNC";
static NSString *kIsMUTE                 = @"isMUTE";
static NSString *kCustomEQName           = @"customEQName";
static NSString *kEqualizer              = @"equalizer";
static NSString *kEffectMode             = @"effectMode";
static NSString *kPitchType              = @"pitchType";
static NSString *kBassGainValue          = @"bassGainValue";
static NSString *kTrebleGainValue        = @"trebleGainValue";
static NSString *kPassType               = @"passType";
static NSString *kHighPassSubType        = @"highPassSubType";
static NSString *kLowPassSubType         = @"lowPassSubType";
static NSString *kHighPassfrequencyValue = @"highPassfrequencyValue";
static NSString *kIsAllPassForHighPass   = @"isAllPassForHighPass";
static NSString *kLowPassfrequencyValue  = @"lowPassfrequencyValue";
static NSString *kIsAllPassForLowhPass   = @"isAllPassForLowhPass";
static NSString *kLrInputValue           = @"lrInputValue";
static NSString *kMainInputValue         = @"mainInputValue";
static NSString *kAuxInputValue          = @"auxInputValue";

@interface HNChannelStore ()

@property (nonatomic, strong) FMDatabase *database;
@property (nonatomic) NSMutableArray *privateChannels;

@end

@implementation HNChannelStore

+ (instancetype)sharedStore {
    static id sharedStore = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedStore = [[[self class] alloc] init];
    });

    return sharedStore;
}

- (instancetype)init {
    self = [super init];
    if(self) {
        _privateChannels = [NSMutableArray new];
        // 从沙盒取回数据(看是否存在「IOMixData.sqlite」文件)
        BOOL dataExist = [[NSFileManager defaultManager] fileExistsAtPath:[self dbPath]];
        if (!dataExist) {
            [self creatDatabase];
            [self creatTable];
            // 固定10個channel(table中插10條數據)
            for (int i = 0; i < 10; i++) {
                [self creatChannelWithIndex:i];
            }
            NSLog(@"沒有數據庫，创建據完畢，现在有%lu个channel", (unsigned long)_privateChannels.count);
        }else{
            NSLog(@"已經有數據庫了，從數據庫返回數據");
            [self getData];
        }
    }
    return self;
}

#pragma mark - FMDB
- (NSString *)dbPath {
    NSArray *documentDirectories = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDirectory = [documentDirectories firstObject];
    return [documentDirectory stringByAppendingPathComponent:@"IOMixData.sqlite"];
}

// 创建数据库
- (void)creatDatabase {
    _database = [FMDatabase databaseWithPath:[self dbPath]];
}

// 创建表
- (void)creatTable {
    [_database open];
    NSString *createTableString =  [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS '%@' ('%@' INTEGER PRIMARY KEY AUTOINCREMENT, '%@' INTEGER, '%@' BOOL, '%@' BOOL, '%@' TEXT, '%@' BLOB, '%@' INTEGER, '%@' INTEGER, '%@' INTEGER, '%@' INTEGER, '%@' INTEGER, '%@' INTEGER, '%@' INTEGER, '%@' INTEGER, '%@' BOOL, '%@' INTEGER, '%@' BOOL, '%@' INTEGER, '%@' INTEGER, '%@' INTEGER)",
                                    kTableName,
                                    kChannelID,
                                    kVolume,
                                    kIsSYNC,
                                    kIsMUTE,
                                    kCustomEQName,
                                    kEqualizer,
                                    kEffectMode,
                                    kPitchType,
                                    kBassGainValue,
                                    kTrebleGainValue,
                                    kPassType,
                                    kHighPassSubType,
                                    kLowPassSubType,
                                    kHighPassfrequencyValue,
                                    kIsAllPassForHighPass,
                                    kLowPassfrequencyValue,
                                    kIsAllPassForLowhPass,
                                    kLrInputValue,
                                    kMainInputValue,
                                    kAuxInputValue];
    [_database executeUpdate:createTableString];
    [_database close];
}

// 插入/新增一个Channel(一共10个)
- (void)creatChannelWithIndex:(NSInteger)index {
    HNChannel *newChannel = [HNChannel new];
    newChannel.channelIndex = index;
    [_database open];
    
    NSString *insertChannelSql = [NSString stringWithFormat:@"INSERT INTO '%@' ('%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@') VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
                           kTableName,
                           kVolume,
                           kIsSYNC,
                           kIsMUTE,
                           kCustomEQName,
                           kEqualizer,
                           kEffectMode,
                           kPitchType,
                           kBassGainValue,
                           kTrebleGainValue,
                           kPassType,
                           kHighPassSubType,
                           kLowPassSubType,
                           kHighPassfrequencyValue,
                           kIsAllPassForHighPass,
                           kLowPassfrequencyValue,
                           kIsAllPassForLowhPass,
                           kLrInputValue,
                           kMainInputValue,
                           kAuxInputValue];
    
    [_database executeUpdate:insertChannelSql,
     [NSNumber numberWithInteger:newChannel.volume],
     [NSNumber numberWithBool:newChannel.isSYNC],
     [NSNumber numberWithBool:newChannel.isMUTE],
     [newChannel customEQName],
     [NSKeyedArchiver archivedDataWithRootObject:newChannel.equalizer],
     [NSNumber numberWithInteger:newChannel.effectMode],
     [NSNumber numberWithInteger:newChannel.pitchType],
     [NSNumber numberWithInteger:newChannel.bassGainValue],
     [NSNumber numberWithInteger:newChannel.trebleGainValue],
     [NSNumber numberWithInteger:newChannel.passType],
     [NSNumber numberWithInteger:newChannel.highPassSubType],
     [NSNumber numberWithInteger:newChannel.lowPassSubType],
     [NSNumber numberWithInteger:newChannel.highPassfrequencyValue],
     [NSNumber numberWithBool:newChannel.isAllPassForHighPass],
     [NSNumber numberWithInteger:newChannel.lowPassfrequencyValue],
     [NSNumber numberWithBool:newChannel.isAllPassForLowhPass],
     [NSNumber numberWithInteger:newChannel.lrInputValue],
     [NSNumber numberWithInteger:newChannel.mainInputValue],
     [NSNumber numberWithInteger:newChannel.auxInputValue]];
    
    [_database close];
    [_privateChannels addObject:newChannel];
}

// 查询(返回數據)
- (void)getData {
    _database = [FMDatabase databaseWithPath:[self dbPath]];
    // 清空数组，防止手动查询数据将多个channel加入
    [_privateChannels removeAllObjects];
    
    [_database open];
    FMResultSet *results = [_database executeQuery:@"SELECT * FROM channle"];
    while ([results next]) {
        HNChannel *channel   = [HNChannel new];
        channel.channelIndex = [results intForColumn:kChannelID] - 1;// 从0开始索引
        // Home页面
        channel.volume       = [results intForColumn:kVolume];
        channel.customEQName = [results stringForColumn:kCustomEQName];
        channel.isSYNC       = [results boolForColumn:kIsSYNC];
        channel.isMUTE       = [results boolForColumn:kIsMUTE];
        // EQ页面
        channel.equalizer  = [NSKeyedUnarchiver unarchiveObjectWithData:[results dataForColumn:kEqualizer]];
        channel.effectMode = [results intForColumn:kEffectMode];
        // Monitor页面
        channel.lrInputValue   = [results intForColumn:kLrInputValue];
        channel.mainInputValue = [results intForColumn:kMainInputValue];
        channel.auxInputValue  = [results intForColumn:kAuxInputValue];
        // Fliter页面
        channel.pitchType              = [results intForColumn:kPitchType];
        channel.bassGainValue          = [results intForColumn:kBassGainValue];
        channel.trebleGainValue        = [results intForColumn:kTrebleGainValue];
        channel.passType               = [results intForColumn:kPassType];
        channel.highPassSubType        = [results intForColumn:kHighPassSubType];
        channel.lowPassSubType         = [results intForColumn:kLowPassSubType];
        channel.highPassfrequencyValue = [results intForColumn:kHighPassfrequencyValue];
        channel.isAllPassForHighPass   = [results boolForColumn:kIsAllPassForHighPass];
        channel.lowPassfrequencyValue  = [results intForColumn:kLowPassfrequencyValue];
        channel.isAllPassForLowhPass   = [results boolForColumn:kIsAllPassForLowhPass];
        [_privateChannels addObject:channel];
    }
    [_database close];
}

// HOME页面保存数据方法
- (void)updateHomeVolume:(NSInteger)volumValue sSYNC:(BOOL)isSYNC eqName:(NSString *)customEQName isMUTE:(BOOL)isMUTE forChannel:(NSInteger)channelID {
    [_database open];
    // channelID由1开始，所以加1
    [_database executeUpdate:@"UPDATE channle SET volume = ?, isSYNC = ?, customEQName = ?, isMUTE = ? WHERE channelID = ?", [NSNumber numberWithFloat:volumValue], [NSNumber numberWithBool:isSYNC], customEQName, [NSNumber numberWithBool:isMUTE], [NSNumber numberWithInteger:channelID + 1]];
    [_database close];
    NSLog(@"保存了HOME页面的数据");
}

// EQ页面保存数据方法
- (void)updateEqualizerValues:(HNEqualizer *)equalizer effectMode:(NSInteger)effectMode forChannel:(NSInteger)channelID {
    [_database open];
    // channelID由1开始，所以加1
    [_database executeUpdate:@"UPDATE channle SET equalizer = ?, effectMode = ? WHERE channelID = ?", [NSKeyedArchiver archivedDataWithRootObject:equalizer], [NSNumber numberWithInteger:effectMode], [NSNumber numberWithInteger:channelID + 1]];
    [_database close];
    NSLog(@"保存了channel%ld的equalizer", (long)channelID + 1);
}

// FLITRER页面保存数据方法
- (void)updateFilterPitchType:(NSInteger)pitchType
                bassGainValue:(NSInteger)bassGainValue
              trebleGainValue:(NSInteger)trebleGainValue
                     passType:(NSInteger)passType
              highPassSubType:(NSInteger)highPassSubType
       highPassfrequencyValue:(NSInteger)highPassfrequencyValue
         isAllpassForHighpass:(BOOL)isAllpassForHighpass
               lowPassSubType:(NSInteger)lowPassSubType
        lowPassfrequencyValue:(NSInteger)lowPassfrequencyValue
          isAllpassForLowpass:(BOOL)isAllpassForLowpass
                   forChannel:(NSInteger)channelID {
    [_database open];
    // channelID由1开始，所以加1
    [_database executeUpdate:@"UPDATE channle SET pitchType = ?, bassGainValue = ?, trebleGainValue = ?, passType = ?, highPassSubType = ?, highPassfrequencyValue = ?, isAllPassForHighPass = ?, lowPassSubType = ?, lowPassfrequencyValue = ?, isAllPassForLowhPass = ? WHERE channelID = ?",
     [NSNumber numberWithInteger:pitchType],
     [NSNumber numberWithInteger:bassGainValue],
     [NSNumber numberWithInteger:trebleGainValue],
     [NSNumber numberWithInteger:passType],
     [NSNumber numberWithInteger:highPassSubType],
     [NSNumber numberWithInteger:highPassfrequencyValue],
     [NSNumber numberWithBool:isAllpassForHighpass],
     [NSNumber numberWithInteger:lowPassSubType],
     [NSNumber numberWithInteger:lowPassfrequencyValue],
     [NSNumber numberWithBool:isAllpassForLowpass],
     [NSNumber numberWithInteger:channelID + 1]];
    [_database close];
    NSLog(@"保存了Filter页面的数据,低音值:%ld; 高音值%ld; 高通值%ld, 高通子类型%ld, 高通allPass状态:%d, 低通值%ld, 低通子类型%ld, 低通allPass状态%d", (long)bassGainValue, (long)trebleGainValue, (long)highPassfrequencyValue, (long)highPassSubType, isAllpassForHighpass, (long)lowPassfrequencyValue, (long)lowPassSubType, isAllpassForLowpass);
}

// MONITOR页面保存数据方法
- (void)updateMonitorLrInputValue:(NSInteger)lrinputValue mainInputValue:(NSInteger)mainInputValue auxInputValue:(NSInteger)auxInputValue forChannel:(NSInteger)channelID {
    [_database open];
    // channelID由1开始，所以加1
    [_database executeUpdate:@"UPDATE channle SET lrInputValue = ?, mainInputValue = ?, auxInputValue = ? WHERE channelID = ?", [NSNumber numberWithInteger:lrinputValue], [NSNumber numberWithInteger:mainInputValue], [NSNumber numberWithInteger:auxInputValue], [NSNumber numberWithInteger:channelID + 1]];
    [_database close];
    NSLog(@"保存了Monitor页面的数据,三个输入通道分别是:%ld; %ld; %ld", (long)lrinputValue, (long)mainInputValue, (long)auxInputValue);
}


#pragma mark -
- (NSArray *)allChannels {
    return [_privateChannels copy];
}

@end
