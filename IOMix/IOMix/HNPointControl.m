//
//  HNPointControl.m
//  IOMix
//
//  Created by YuHeng_Antony on 8/27/15.
//  Copyright (c) 2015 Homni Enterprises Co., Ltd. All rights reserved.
//

#import "HNPointControl.h"

// 触摸点的触摸范围
static CGFloat const RADIUS = 40;

@implementation HNPointControl

+(HNPointControl *)aInstance {
    HNPointControl * aInstance = [[self alloc]initWithFrame:(CGRect){CGPointZero, CGSizeMake(RADIUS * 2, RADIUS * 2)}];
    aInstance.layer.cornerRadius = RADIUS;
    aInstance.layer.masksToBounds = YES;
    aInstance.backgroundColor = [UIColor clearColor];// 触摸点的颜色
    [aInstance addTarget:aInstance action:@selector(touchDragInside:withEvent:) forControlEvents:UIControlEventTouchDragInside];
    
    return aInstance;

}

- (void)touchDragInside:(HNPointControl *)pointControl withEvent:(UIEvent *)event
{
    pointControl.center = [[[event allTouches] anyObject] locationInView:self.superview];
    
    if (self.dragCallBack) {
        self.dragCallBack(self);
    }
}

@end
