//
//  HNChannel.h
//  IOMix
//
//  Created by YuHeng_Antony on 15/7/10.
//  Copyright (c) 2015年 Homni Enterprises Co., Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@class HNEqualizer;

@interface HNChannel : NSObject

/**
 *  标记自身Index(0开始)
 */
@property (nonatomic) NSInteger channelIndex;

#pragma mark - HOME
@property (nonatomic) NSInteger volume;
@property (nonatomic) BOOL isSYNC;
@property (nonatomic) BOOL isMUTE;
@property (nonatomic, copy) NSString *customEQName;

#pragma mark - EQ
@property (nonatomic, strong) HNEqualizer *equalizer;

/**
 *  0-9(9表示无选择effect;0-8表示9种音效)
 */
@property (nonatomic) NSInteger effectMode;

#pragma mark - FILTRER
/**
 *  0-1(或者0-1,默认选择0)仅仅用于识别哪个按钮要高亮
    0:Bass Shelf
    1:Treble Shelf
 */
@property (nonatomic) NSInteger pitchType;

/**
 *  高音增益值-12到12(硬件对应0-24)
 */
@property (nonatomic) NSInteger bassGainValue;

/**
 *  低音增益值-12到12(硬件对应0-24)
 */
@property (nonatomic) NSInteger trebleGainValue;

/**
 *  0-1(或者0-1,默认选择0)仅仅用于识别哪个按钮要高亮
    0:High Pass
    1:Low Pass
 */
@property (nonatomic) NSInteger passType;

/**
 *  高通子模式(滤波模式),6种模式
 */
@property (nonatomic) NSInteger highPassSubType;

/**
 *  低通子模式(滤波模式),6种模式
 */
@property (nonatomic) NSInteger lowPassSubType;

/**
 *  高通频率点,0-199
 */
@property (nonatomic) NSInteger highPassfrequencyValue;

/**
 *  记录highpass的allPass按钮是否点亮
 */
@property (nonatomic) BOOL isAllPassForHighPass;

/**
 *  低通频率点,0-199
 */
@property (nonatomic) NSInteger lowPassfrequencyValue;

/**
 *  记录lowpass的allPass按钮是否点亮
 */
@property (nonatomic) BOOL isAllPassForLowhPass;


#pragma mark - MONITOR
// 保存三个输入通道值
@property (nonatomic) NSInteger lrInputValue;
@property (nonatomic) NSInteger mainInputValue;
@property (nonatomic) NSInteger auxInputValue;

@end
