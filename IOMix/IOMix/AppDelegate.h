//
//  AppDelegate.h
//  IOMix
//
//  Created by Chiwen on 2015/6/15.
//  Copyright (c) 2015年 Homni Enterprises Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

