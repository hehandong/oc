//
//  HNFilterViewControllerS.swift
//  IOMix
//
//  Created by hehandong on 15/11/14.
//  Copyright © 2015年 Homni Enterprises Co., Ltd. All rights reserved.
//

import UIKit
import Foundation


class HNFilterViewControllerS: UIViewController {
    

    @IBOutlet var channelButtons: [UIButton]!
    
    @IBOutlet weak var bassTrebleSlider: ASValueTrackingSlider!
    
    @IBOutlet weak var hightLowPassSlider: ASValueTrackingSlider!
    
    @IBOutlet var bassTrebleButtons: [UIButton]!
    
    @IBOutlet var highLowPassButtons: [UIButton]!
    
    @IBOutlet weak var curveView: UIView!
    
  
    
    @IBOutlet weak var bassTrebleValueLabel: UILabel!
    
    @IBOutlet weak var highLowPassValueLabel: UILabel!
    
    @IBOutlet weak var allPassButton: UIButton!
   
    @IBOutlet weak var middleView: UIView!
    
    var nonSeletPassTypeAlertView:UIAlertView!
    
    ///x坐标 50-20K数值的比例,这个比例有问题
    let xAxisSacleArray:[Double] = [0.12401,0.21827,0.31052,0.43453,0.52879,0.62104,0.74505,0.83951,0.93156]
    
    ///x坐标的数值
    let eqValueArray = [20,50,100,200,500,1000,2000,5000,10000,20000]
    
    ///x轴20-100中每个小格的比例
    let gVerticalSpaceLTypes:[Double] = [0.05456, 0.03969, 0.02976, 0.02381, 0.02183, 0.01786, 0.01786, 0.01290]
    
    let subMode:[String] = ["BUTTER_WORTH_1","BUTTER_WORTH_2","BESSEL_2","LINKWITZ_RILEY_2","VARIABLE_Q2","CHEBYCHEV"]
    
    let ButterWorth1Index = 1
    let ButterWorth2Index = 2
    
    // 背景图最左边的X值,最右边的X值,最顶的Y值、最底的Y值
    var HN_LEADING_VALUE:CGFloat!
    var HN_TRAILING_VALUE:CGFloat!
    var HN_TOP_VALUE:CGFloat!
    var HN_BOTTOM_VALUE:CGFloat!
    
    var HN_V_GROUP_SPACE_VC:CGFloat!
    /// 低通贝塞尔第二个点X轴要增加的数值
    var LOW_PASS_BESSEL_X_LENGTH:CGFloat!
    /// 低通贝塞尔第二个点Y轴要增加的数值
    var LOW_PASS_BESSEL_Y_ADD:CGFloat!
    
    var LOW_PASS_LINK_X_ADD:CGFloat!
    
    
    ///20-20K 10个FC值在View中屏幕坐标X值
    var valueArray = [CGFloat]()
    
    ///表格X坐标的屏幕与FC比例数组
    var scaleFrom20To20K = [CGFloat]()
    ///表格Y坐标，屏幕与DB的比例
    var scaleFrom12ToNegative12:CGFloat!
    ///水平线20的X坐标
    var horizontalLineStartXPos:CGFloat!
    ///水平线20k的X坐标
    var horizontalLineEndXPos:CGFloat!
    ///Y轴的屏幕中点
    var centreYPos:CGFloat!
    
    /**
     *  记录Bass还是Treble,用于识别发送0x14还是0x15
     */
    var currentPitchType:Int!
    /**
     *  记录High Pass还是Low Pass,用于识别发送0x16还是0x17
     */
    var currentPassType:Int!
    /**
     *  使用者选择的Filter Subtype类型(就是pickerView选择的)
     */
    var currentHighPassSubType = 0
    
    var currentLowPassSubType = 0
    
    var channelInUse:HNChannel!
    
    var allChannels:NSArray!
    
    var highPassDropDown:DropDown1!
    
    var lowPassDropDown:DropDown1!
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        NSBundle.mainBundle().loadNibNamed("HNFilterViewControllerS", owner: self, options: nil)
        //代码写下拉菜单
        dropDownView(self.middleView)
        //初始化部分需要的数据
        initFCValueAndScale()
        
        layoutSlider()
        
        if channelButtons != nil{
        toggleChannel(channelButtons[0])
        }
        
        //下拉菜单的点击事件
        dropDownAction()
        
       
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    //MARK:高低通的下拉菜单
    func dropDownView(view:UIView){
        //高通的下拉菜单
        highPassDropDown = DropDown1.init(frame: CGRectMake(249, 184, 148, 135), tag: 0)
    
        //低通的下拉菜单
        lowPassDropDown = DropDown1.init(frame: CGRectMake(249, 242, 148, 135), tag: 1)
        
        highPassDropDown.tableArray = subMode
        lowPassDropDown.tableArray = subMode
        view.addSubview(highPassDropDown)
        view.addSubview(lowPassDropDown)
        
//        //高通的下拉菜单约束
//        highPassDropDown.translatesAutoresizingMaskIntoConstraints = false
//        
//        let leadingConstraints = NSLayoutConstraint.init(item: highPassDropDown, attribute: NSLayoutAttribute.Leading, relatedBy: NSLayoutRelation.Equal, toItem: highLowPassButtons[0], attribute: NSLayoutAttribute.Trailing, multiplier: 1, constant: 85)
//        
//        let trailingConstraints = NSLayoutConstraint.init(item: allPassButton, attribute: NSLayoutAttribute.Leading, relatedBy: NSLayoutRelation.Equal, toItem: highPassDropDown, attribute: NSLayoutAttribute.Trailing, multiplier: 1, constant: 89)
//        
//        let alignConstraints = NSLayoutConstraint.init(item: highPassDropDown, attribute: NSLayoutAttribute.CenterY, relatedBy: NSLayoutRelation.Equal, toItem: highLowPassButtons[0], attribute: NSLayoutAttribute.CenterY, multiplier: 1, constant: 0.0)
//        
//        let heightConstraints = NSLayoutConstraint.init(item: highPassDropDown, attribute: NSLayoutAttribute.Height, relatedBy: NSLayoutRelation.Equal, toItem: highLowPassButtons[0], attribute: NSLayoutAttribute.Height, multiplier: 1, constant: 0.0)
//        
//        let widthConstraints = NSLayoutConstraint.init(item: highPassDropDown, attribute: NSLayoutAttribute.Width, relatedBy: NSLayoutRelation.Equal, toItem: highLowPassButtons[0], attribute: NSLayoutAttribute.Width, multiplier: 1, constant: 0)
//        
//        view.addConstraints([leadingConstraints,trailingConstraints,alignConstraints,heightConstraints,widthConstraints])
//        
//        
//        //低通的下拉菜单约束
//        lowPassDropDown.translatesAutoresizingMaskIntoConstraints = false
//        
//        let lowLeadingConstraints = NSLayoutConstraint.init(item: lowPassDropDown, attribute: NSLayoutAttribute.Leading, relatedBy: NSLayoutRelation.Equal, toItem: highLowPassButtons[1], attribute: NSLayoutAttribute.Trailing, multiplier: 1, constant: 85)
//        
//        
//        let lowAlignConstraints = NSLayoutConstraint.init(item: lowPassDropDown, attribute: NSLayoutAttribute.CenterY, relatedBy: NSLayoutRelation.Equal, toItem: highLowPassButtons[1], attribute: NSLayoutAttribute.CenterY, multiplier: 1, constant: 0.0)
//        
//        let lowHeightConstraints = NSLayoutConstraint.init(item: lowPassDropDown, attribute: NSLayoutAttribute.Height, relatedBy: NSLayoutRelation.Equal, toItem: highLowPassButtons[1], attribute: NSLayoutAttribute.Height, multiplier: 1, constant: 0.0)
//        
//        let lowWidthConstraints = NSLayoutConstraint.init(item: lowPassDropDown, attribute: NSLayoutAttribute.Width, relatedBy: NSLayoutRelation.Equal, toItem: highLowPassButtons[1], attribute: NSLayoutAttribute.Width, multiplier: 1, constant: 0)
//        
//        view.addConstraints([lowLeadingConstraints,lowAlignConstraints,lowHeightConstraints,lowWidthConstraints])
    
    
    }
    
    
    
    //MARK:-高低通下拉菜单的点击事件
    func dropDownAction(){
        
        //点击了高通下拉菜单时，低通下拉菜单TableView要隐藏起来
        NSNotificationCenter.defaultCenter().addObserverForName("show", object: nil, queue: nil) { (NSNotification2) -> Void in

           let tag = NSNotification2.userInfo!["textFildTag"]?.integerValue
           //点击高通下拉菜单时
            if tag == 0{
                self.lowPassDropDown.tv.hidden = true
                self.channelInUse.passType = PassType.PASS_TYPE_HIGH.rawValue
                self.highLowPassButtons[0].selected = true
                self.highLowPassButtons[1].selected = false
            }else {
            //点击低通下拉菜单时
                self.highPassDropDown.tv.hidden = true
                self.channelInUse.passType = PassType.PASS_TYPE_LOW.rawValue
                self.highLowPassButtons[1].selected = true
                self.highLowPassButtons[0].selected = false
            }
            
        }
        
        //点击了高低通TableView时发送指令
        NSNotificationCenter.defaultCenter().addObserverForName("test", object: nil, queue: nil) { (NSNotification) -> Void in
            
            let tableViewCellIndex = NSNotification.userInfo!["index"]?.integerValue
            let tag = NSNotification.userInfo!["textFildTag"]?.integerValue
            
            if tag == 0 {
                //点击了高通TableView时
                switch tableViewCellIndex!{
                case SubMode.BUTTER_WORTH_1.rawValue:
                    HNIOMixManager.shareManager().channelInUse.highPassSubType = SubMode.BUTTER_WORTH_1.rawValue
                case SubMode.BUTTER_WORTH_2.rawValue:
                    HNIOMixManager.shareManager().channelInUse.highPassSubType = SubMode.BUTTER_WORTH_2.rawValue
                case SubMode.BESSEL_2.rawValue:
                    HNIOMixManager.shareManager().channelInUse.highPassSubType = SubMode.BESSEL_2.rawValue
                case SubMode.LINKWITZ_RILEY_2.rawValue:
                    HNIOMixManager.shareManager().channelInUse.highPassSubType = SubMode.LINKWITZ_RILEY_2.rawValue
                case SubMode.VARIABLE_Q2.rawValue:
                    HNIOMixManager.shareManager().channelInUse.highPassSubType = SubMode.VARIABLE_Q2.rawValue
                case SubMode.CHEBYCHEV.rawValue:
                    HNIOMixManager.shareManager().channelInUse.highPassSubType = SubMode.CHEBYCHEV.rawValue
                default:
                    break
                }
                
                self.drawSaveValuePath()
                
//                self.highPassDropDown.textField.background = UIImage(named: "ButtonSelected")
//                self.lowPassDropDown.textField.background = UIImage(named: "button")

                HNIOMixManager.shareManager().device.setupHighPassForChannel(self.channelInUse.channelIndex, filterSubtypeMode: self.channelInUse.highPassSubType,frequencyValue: self.channelInUse.highPassfrequencyValue,immediately: true)
                
                
            } else {
                //点击了低通TableView时
                switch tableViewCellIndex!{
                case SubMode.BUTTER_WORTH_1.rawValue:
                    HNIOMixManager.shareManager().channelInUse.lowPassSubType = SubMode.BUTTER_WORTH_1.rawValue
                case SubMode.BUTTER_WORTH_2.rawValue:
                    HNIOMixManager.shareManager().channelInUse.lowPassSubType = SubMode.BUTTER_WORTH_2.rawValue
                case SubMode.BESSEL_2.rawValue:
                    HNIOMixManager.shareManager().channelInUse.lowPassSubType = SubMode.BESSEL_2.rawValue
                case SubMode.LINKWITZ_RILEY_2.rawValue:
                    HNIOMixManager.shareManager().channelInUse.lowPassSubType = SubMode.LINKWITZ_RILEY_2.rawValue
                case SubMode.VARIABLE_Q2.rawValue:
                    HNIOMixManager.shareManager().channelInUse.lowPassSubType = SubMode.VARIABLE_Q2.rawValue
                case SubMode.CHEBYCHEV.rawValue:
                    HNIOMixManager.shareManager().channelInUse.lowPassSubType = SubMode.CHEBYCHEV.rawValue
                default:
                    break
                }
                
                self.drawSaveValuePath()
                
//                self.lowPassDropDown.textField.background = UIImage(named: "ButtonSelected")
//                self.highPassDropDown.textField.background = UIImage(named: "button")

                HNIOMixManager.shareManager().device.setupLowPassForChannel(self.channelInUse.channelIndex, filterSubtypeMode: self.channelInUse.lowPassSubType, frequencyValue: self.channelInUse.lowPassfrequencyValue,immediately: true)
            }
        }
        
    }
    
    //MARK:初始化数据
    /**
     初始化必要的数据
     - returns: <#return value description#>
     */
    func initFCValueAndScale(){
        
        // 背景图最左边的X值,最右边的X值,最顶的Y值、最底的Y值
        if self.curveView != nil{
            
        HN_LEADING_VALUE = self.curveView.bounds.size.width * 0.02877
        HN_TRAILING_VALUE = HN_LEADING_VALUE + (self.curveView.bounds.size.width - HN_LEADING_VALUE - (self.curveView.bounds.size.width * 0.01587))
        HN_TOP_VALUE = self.curveView.bounds.size.height * 0.09016
        HN_BOTTOM_VALUE = self.curveView.bounds.size.height - HN_TOP_VALUE
        
        LOW_PASS_BESSEL_Y_ADD = self.curveView.bounds.size.height * 0.0614
        LOW_PASS_BESSEL_X_LENGTH = self.curveView.bounds.size.width * 0.08936
        LOW_PASS_LINK_X_ADD = self.curveView.bounds.size.width * 0.07952
         
        // 20-200为一组
        HN_V_GROUP_SPACE_VC = self.curveView.bounds.size.width * 0.32507
        
        //必须初始化求出 valueArray
        getXAxisRealValueArray()
        
        //必须初始化求出 scaleFrom20To20K
        setScaleFrom20To20K()
        
        allChannels = HNIOMixManager.shareManager().allChannel
        
        horizontalLineStartXPos = valueArray.first
        horizontalLineEndXPos = valueArray.last
        
        centreYPos = getYAxisValue(4)
        
        }
    
    }
    
   
    
     //MARK:-设值的两条slider配置
    func layoutSlider(){
        if bassTrebleSlider != nil && hightLowPassSlider != nil{
     bassTrebleSlider.setThumbImage(UIImage(named: "sliderButton"), forState: UIControlState.Normal)
     bassTrebleSlider.popUpViewColor = UIColor.clearColor()
     bassTrebleSlider.textColor = UIColor.clearColor()
     bassTrebleSlider.setMaxFractionDigitsDisplayed(0)
     bassTrebleSlider.setMaximumTrackImage(UIImage(named: "sliderTrackImage"), forState: UIControlState.Normal)
     bassTrebleSlider.setMinimumTrackImage(UIImage(named: "sliderTrackImage"), forState: UIControlState.Normal)
     bassTrebleSlider.minimumValue = -12.0
     bassTrebleSlider.maximumValue = 12.0
            
    
     hightLowPassSlider.setThumbImage(UIImage(named: "sliderButton"), forState: UIControlState.Normal)
     hightLowPassSlider.popUpViewColor = UIColor.clearColor()
     hightLowPassSlider.textColor = UIColor.clearColor()
     hightLowPassSlider.setMaxFractionDigitsDisplayed(0)
     hightLowPassSlider.setMaximumTrackImage(UIImage(named: "sliderTrackImage"), forState: UIControlState.Normal)
     hightLowPassSlider.setMinimumTrackImage(UIImage(named: "sliderTrackImage"), forState: UIControlState.Normal)
     hightLowPassSlider.minimumValue = 0.0
     hightLowPassSlider.maximumValue = 199.0
        
        }
        
    }
    
    
    //MARK:-channel的点击事件
    @IBAction func toggleChannel(sender: UIButton) {
        
        
        // 切换channel,设置channelInUse
        HNIOMixManager.shareManager().channelInUse = HNIOMixManager.shareManager().allChannel[sender.tag] as! HNChannel
        
        channelInUse = HNIOMixManager.shareManager().channelInUse
        
        // 配置channel按钮状态
        setupButtonsState(channelButtons, sender: sender)
        
        // 配置高低音按钮状态及slider值、label值
        // 默认显示低音按钮为选中状态
        // 显示slider的值
        bassTrebleToggle(bassTrebleButtons[channelInUse.pitchType])
        
        if channelInUse.pitchType == 0 {
          bassTrebleSlider.value = Float(channelInUse.bassGainValue)
          bassTrebleValueLabel.text = String(channelInUse.bassGainValue) + " dB"
        
        }else{
          bassTrebleSlider.value = Float(channelInUse.trebleGainValue)
          bassTrebleValueLabel.text = String(channelInUse.trebleGainValue) + " dB"
    
        }
        
        // 配置高低通按钮状态、subType类型、All Pass按钮状态、slider的值、label值
        highLowPassToggle(highLowPassButtons[channelInUse.passType])
        
        if channelInUse.passType == 0 {
        hightLowPassSlider.value = Float(fcTransformInt(channelInUse.highPassfrequencyValue))
        highLowPassValueLabel.text = String(channelInUse.highPassfrequencyValue) + " Hz"
//        highPassDropDown.textField.textColor = UIColor.redColor()
        }else{
        hightLowPassSlider.value = Float(fcTransformInt(channelInUse.lowPassfrequencyValue))
        highLowPassValueLabel.text = String(channelInUse.lowPassfrequencyValue) + " Hz"
//        lowPassDropDown.textField.textColor = UIColor.redColor()
        }
        
        highPassDropDown.textField.text = subMode[HNIOMixManager.shareManager().channelInUse.highPassSubType]
        lowPassDropDown.textField.text = subMode[HNIOMixManager.shareManager().channelInUse.lowPassSubType]
        
        drawSaveValuePath()
        
        
    }
    
    //MARK:-高低音Slider滑动事件
    @IBAction func bassTrebleSliderValueChange(sender: ASValueTrackingSlider) {
        
        bassTrebleValueLabel.text = String(Int(sender.value)) + " dB"
        
        switch channelInUse.pitchType{
        //MARK:-发送0x14低音控制指令
        case PitchType.PITCH_TYPE_BASS.rawValue:
            HNIOMixManager.shareManager().device.setupBassForChannel(HNIOMixManager.shareManager().channelInUse.channelIndex, bassGainValue: Int(sender.value))
            channelInUse.bassGainValue = Int(sender.value)
            
        //MARK:-发送0x15高音控制指令
        case PitchType.PITCH_TYPE_TREBLE.rawValue:
            HNIOMixManager.shareManager().device.setupTrebleForChannel(HNIOMixManager.shareManager().channelInUse.channelIndex, trebleGainValue: Int(sender.value))
            channelInUse.trebleGainValue = Int(sender.value)
            
        default:
            
            if (nonSeletPassTypeAlertView == nil){
                nonSeletPassTypeAlertView = UIAlertView()
                nonSeletPassTypeAlertView.title = "NOTE"
                nonSeletPassTypeAlertView.message = "Please select Bass Shelf or Treble Shelf"
                nonSeletPassTypeAlertView.delegate = self
                nonSeletPassTypeAlertView.addButtonWithTitle("OK")
                nonSeletPassTypeAlertView.show()
            }
        }
        
    }
   //MARK:-高低通Slider滑动事件
    @IBAction func highLowPassSliderValueChange(sender: ASValueTrackingSlider) {
        
        let fcValue = Float(intTransformFc(Int(sender.value)))
        
        highLowPassValueLabel.text = String(Int(fcValue)) + " Hz"
        //发通知清除旧曲线
        NSNotificationCenter.defaultCenter().postNotificationName(kNotificationFilterSliderValueChanged, object: nil, userInfo: [EQSliderValue:sender.value,EQSliderTag:sender.tag])
    
        
        let device = HNIOMixManager.shareManager().device
        let channelIndex = HNIOMixManager.shareManager().channelInUse.channelIndex
        let highPassSubType = HNIOMixManager.shareManager().channelInUse.highPassSubType
        let lowPassSubType = HNIOMixManager.shareManager().channelInUse.lowPassSubType
        let senderValue = Int(fcValue)
        
        switch(channelInUse.passType){
        
        //MARK:发送0x16高通控制指令
        case PassType.PASS_TYPE_HIGH.rawValue:
            
            device.setupHighPassForChannel(channelIndex, filterSubtypeMode: highPassSubType, frequencyValue: senderValue,immediately: false)
            
            // 保存数据
            channelInUse.highPassfrequencyValue = senderValue
            
            layoutCurve(senderValue, passType: PassType.PASS_TYPE_HIGH.rawValue, subType: highPassSubType)
            
            channelInUse.isAllPassForHighPass = false
            
            allPassButton.selected = channelInUse.isAllPassForHighPass
            
        //MARK:发送0x17低通控制指令
        case PassType.PASS_TYPE_LOW.rawValue:
            device.setupLowPassForChannel(channelIndex, filterSubtypeMode:lowPassSubType, frequencyValue: senderValue,immediately: false)
            // 保存数据
            channelInUse.lowPassfrequencyValue = senderValue
            
           layoutCurve(senderValue, passType: PassType.PASS_TYPE_LOW.rawValue, subType: lowPassSubType)
            
           channelInUse.isAllPassForLowhPass = false
            
           allPassButton.selected = channelInUse.isAllPassForLowhPass
            
            
        default:
            
            if (nonSeletPassTypeAlertView == nil){
             nonSeletPassTypeAlertView = UIAlertView()
             nonSeletPassTypeAlertView.title = "NOTE"
             nonSeletPassTypeAlertView.message = "Please select High Pass or Low Pass"
             nonSeletPassTypeAlertView.delegate = self
             nonSeletPassTypeAlertView.addButtonWithTitle("OK")
             nonSeletPassTypeAlertView.show()
            }
        }
    }
    
 
    
    //MARK:-设置高低音的按钮状态，按钮点击事件
    @IBAction func bassTrebleToggle(sender: UIButton) {
        // 配置按钮状态
        setupButtonsState(bassTrebleButtons, sender: sender)
        
        // 记录选择的是高音还是低音(保存数据)
        // 设置对应的值
        switch sender.tag {
            
        case PitchType.PITCH_TYPE_BASS.rawValue:
            // 保存数据
            channelInUse.pitchType = PitchType.PITCH_TYPE_BASS.rawValue

            // 更新slider值
            bassTrebleSlider.value = Float(channelInUse.bassGainValue)
            bassTrebleValueLabel.text = String(channelInUse.bassGainValue) + " dB"
            
        case PitchType.PITCH_TYPE_TREBLE.rawValue:
           
            channelInUse.pitchType = PitchType.PITCH_TYPE_TREBLE.rawValue

            bassTrebleSlider.value = Float(channelInUse.trebleGainValue)
            bassTrebleValueLabel.text = String(channelInUse.trebleGainValue) + " dB"
            
        default:
            break
        }
       
    }
    
    //MARK:-设置高低通按钮状态,按钮点击事件
    @IBAction func highLowPassToggle(sender: UIButton) {
        // 配置按钮状态
        setupButtonsState(highLowPassButtons, sender: sender)
        
        
        switch sender.tag {
        case PassType.PASS_TYPE_HIGH.rawValue:
             // 保存数据
            channelInUse.passType = PassType.PASS_TYPE_HIGH.rawValue
             // 更新slider值
            
            hightLowPassSlider.value = Float(fcTransformInt(channelInUse.highPassfrequencyValue))
            print("保存的高通FC数据\(channelInUse.highPassfrequencyValue)")
            print("保存的高通FC数据\(hightLowPassSlider.value)")
            
            highLowPassValueLabel.text = String(channelInUse.highPassfrequencyValue) + " Hz"
            allPassButton.selected = channelInUse.isAllPassForHighPass ? true:false
            
           
        case PassType.PASS_TYPE_LOW.rawValue:

            channelInUse.passType = PassType.PASS_TYPE_LOW.rawValue
            
            hightLowPassSlider.value = Float(fcTransformInt(channelInUse.lowPassfrequencyValue))
            print("保存的低通FC数据\(channelInUse.lowPassfrequencyValue)")
            print("保存的低通FC数据\(hightLowPassSlider.value)")
            
            highLowPassValueLabel.text = String(channelInUse.lowPassfrequencyValue) + " Hz"
            allPassButton.selected = channelInUse.isAllPassForLowhPass ? true:false
            
            
        default:
            break
        }
        
        //切换时画图
        drawSaveValuePath()
       
        
    }
    
    //MARK:-ALL PASS按钮的点击事件
    @IBAction func allPassAction(sender: UIButton) {
        
        sender.selected = !sender.selected

        if sender.selected {
        // 选择状态:发送allpass指令
            switch channelInUse.passType{
            case PassType.PASS_TYPE_HIGH.rawValue:
                // 点击的是high pass的allpass按钮
                // 199为allpass
                //MARK:发送0x16高通控制指令(all pass)
                HNIOMixManager.shareManager().device.setupHighPassForChannel(HNIOMixManager.shareManager().channelInUse.channelIndex, filterSubtypeMode: currentHighPassSubType, frequencyValue: 199,immediately: true)
                
               channelInUse.isAllPassForHighPass = true
                
               drawALine()
                
            case PassType.PASS_TYPE_LOW.rawValue:
        //MARK:发送0x17低通控制指令(all pass)
                HNIOMixManager.shareManager().device.setupLowPassForChannel(HNIOMixManager.shareManager().channelInUse.channelIndex, filterSubtypeMode: currentLowPassSubType, frequencyValue: 199,immediately: true)
                
               channelInUse.isAllPassForLowhPass = true
                
               drawALine()
                
            default:
                break
                
            }
            
            
        }else{
        // 非选择状态，发送回slider当前值
        // TODO:此种状态下发送，还会受200ms影响，导致有时候没有发送(考虑另外写接口方法给两个allPass按钮)
            switch channelInUse.passType{
            case PassType.PASS_TYPE_HIGH.rawValue:
                HNIOMixManager.shareManager().device.setupHighPassForChannel(HNIOMixManager.shareManager().channelInUse.channelIndex, filterSubtypeMode: currentHighPassSubType, frequencyValue: channelInUse.highPassfrequencyValue,immediately: true)
                  // 保存数据
                channelInUse.isAllPassForHighPass = false
                
                drawSaveValuePath()
                
            case AllPass.ALL_PASS_FOR_LOW.rawValue:
                HNIOMixManager.shareManager().device.setupLowPassForChannel(HNIOMixManager.shareManager().channelInUse.channelIndex, filterSubtypeMode: currentLowPassSubType, frequencyValue: channelInUse.lowPassfrequencyValue,immediately: true)
                  // 保存数据
                channelInUse.isAllPassForLowhPass = false
                
                drawSaveValuePath()
                
            default:
                break
            }
        }
        
        
    }
    
    
    //MARK:-画已保存数据的曲线
    /**
    画已保存数据的曲线
    */
    func drawSaveValuePath(){
        //发通知清除旧曲线
        NSNotificationCenter.defaultCenter().postNotificationName(kNotificationFilterSliderValueChanged, object: nil, userInfo: [EQSliderValue:0,EQSliderTag:0])
        
        switch(channelInUse.passType){
            //oc FILTER_TYPE_HIGH_PASS
            //MARK:发送0x16高通控制指令
        case PassType.PASS_TYPE_HIGH.rawValue:
            
            layoutCurve(channelInUse.highPassfrequencyValue, passType: PassType.PASS_TYPE_HIGH.rawValue, subType: HNIOMixManager.shareManager().channelInUse.highPassSubType)
            
            //MARK:发送0x17低通控制指令
        case PassType.PASS_TYPE_LOW.rawValue:
            
            layoutCurve(channelInUse.lowPassfrequencyValue, passType: PassType.PASS_TYPE_LOW.rawValue, subType: HNIOMixManager.shareManager().channelInUse.lowPassSubType)
            
        default:
            break
        }
    }
    
    //MARK:-单选更新按钮状态
    /**
     单选更新按钮状态Helper Method
     - parameter buttons: 按钮的数组
     - parameter sender:  当前选中的按钮
     */
    func setupButtonsState(buttons:[UIButton],sender:UIButton){
        sender.selected = true

        for i in buttons {
            if sender.selected {
                if !(i == sender) {
                i.selected = false

                }
            }
        }
    }
    
   
    //MARK:-x轴20-100中每个小格的屏幕x轴坐标
    func hnGetVSpaceWithVC(index:Int) ->CGFloat{
        
        return CGFloat(gVerticalSpaceLTypes[index]) * self.curveView.bounds.size.width
    }
    
    //MARK:-表格x轴有数字下标的实际屏幕x轴坐标
    func getXAxisRealValue(index:Int) ->CGFloat{
        
        return CGFloat(xAxisSacleArray[index]) * self.curveView.bounds.size.width
    }
    
    //MARK:-表格x轴有数字下标的实际屏幕x轴坐标数组
    func getXAxisRealValueArray() ->[CGFloat]{
        
      
        for i in 0..<3{
           
            let valueA = HN_LEADING_VALUE + HN_V_GROUP_SPACE_VC * CGFloat(i)
            valueArray.append(valueA)
            
            
            let valueB = HN_LEADING_VALUE + getXAxisRealValue(0) + HN_V_GROUP_SPACE_VC * CGFloat(i)
            valueArray.append(valueB)
            
            
            let valueC = HN_LEADING_VALUE + getXAxisRealValue(1) + HN_V_GROUP_SPACE_VC * CGFloat(i)
            valueArray.append(valueC)
        }
        
        let valueD = HN_V_GROUP_SPACE_VC * CGFloat(3)
        
        valueArray.append(valueD)

        return valueArray
    
    }
    
    //MARK:-设置X轴，50-20K的比例
    func setScaleFrom20To20K() ->[CGFloat]{
       
        for var index = 0;index < valueArray.count ;index++ {
            if index < 9 {
                //屏幕X坐标两点的差
                let valueDiffTemp:CGFloat = valueArray[index + 1] - valueArray[index]
                //FC坐标两点的差
                let eqValueDiffTemp:CGFloat = CGFloat(eqValueArray[index+1] - eqValueArray[index])
                scaleFrom20To20K.append(valueDiffTemp / eqValueDiffTemp)
            }
        
        }
      return scaleFrom20To20K
    }
    
    //MARK:-计算Y轴数字下标的屏幕Y轴坐标
    /**
    计算屏幕Y轴的数值
    - parameter num: 输入0-8,0对应12db，8对应-12
    - returns: 屏幕Y轴的数值
    */
    func getYAxisValue(num:CGFloat) ->CGFloat{
        
        var yLineValue:CGFloat = 0.0
        
        if num < 9 {
            
        let scale = (HN_BOTTOM_VALUE  - HN_TOP_VALUE)/8
        yLineValue = num * scale + HN_TOP_VALUE
            
        }
        
//        print("输入第\(num)条Y坐标，对应的Y坐标是\(yLineValue)")
        return yLineValue
    
    }
    
    //MARK:-计算fc所在区间的索引
    /**
    计算fc所在区间的索引，索引的范围[0,8]
    - parameter fc: <#fc description#>
    - returns: fc所在区间的索引
    */
    func getFcRangeIndex(fc:Int) -> Int{
        
        var index:Int = 0
        
        for var i = 0; i < valueArray.count - 1;++i {
            
            if  (fc >= eqValueArray[i]) && (fc <= eqValueArray[i+1]) {
             index = i
            }
        }
        
        return index
    }
    
    //MARK:-计算屏幕实际的X坐标值
    /**
     计算屏幕实际的X坐标值
     - parameter fc:输入的FC值
     - returns: 屏幕实际的X轴数值
     */
    func getViewXLineValueFromFC(fc:Int) ->CGFloat {
     
       let index = getFcRangeIndex(fc)
       let fcToViewPoint = valueArray[index] + scaleFrom20To20K[index] * CGFloat(fc - eqValueArray[index])
       
//       print("FC值为\(fc),在第\(index)个区间,数值为\(fcToViewPoint)")
       return fcToViewPoint
    
    }
    
    //MARK:-低通ButterWorth1坐标点
    /**
    Low Pass Butter Worth1的三个坐标点
    - parameter fc: 输入的FC值
    - returns: return value 返回3个坐标的数组
    */
    func drawLowPassButter1(fc:Int) ->[NSValue]{
   
     return drawLowPassButter(fc, xCritical: 700, yCritical: 2000)
    }
    
    //MARK:-低通ButterWorth2坐标点
    /**
    Low Pass Butter Worth2的三个坐标
    - parameter fc: 输入的FC值
    - returns: return value 返回3个坐标的数组
    */
    func drawLowPassButter2(fc:Int) ->[NSValue]{
        
        return drawLowPassButter(fc, xCritical: 900, yCritical: 3000)
        
    }
    
    
    /**
     Low Pass Butter Worth的三个坐标点
     - parameter fc:        输入的FC值
     - parameter xCritical: X轴的临界值，输入xCritical值时，终点（第三点）的X坐标为表格的X轴终点
     - parameter yCritical: Y轴的临界值，Y轴的变化会很慢，输入yCritical值时，终点（第三点）的Y坐标为表格的Y轴的第6条   线（序号为5），上往下数
     
     - returns: <#return value description#>
     */
    func drawLowPassButter(fc:Int,xCritical:Int,yCritical:Int) ->[NSValue]{
        
        //返回坐标点的数组
        var pointValueArray = [NSValue]()
        
        if  (fc >= 20) && (fc <= 20000) {
        
        //开始点
        let startPoint = CGPoint(x: horizontalLineStartXPos, y: centreYPos)
        let startPointValue = NSValue(CGPoint: startPoint)
        
        
        //fc点
        let fcXPoint = getViewXLineValueFromFC(fc)
        let fcPoint = CGPoint(x: fcXPoint, y: centreYPos)
        let fcPointValue = NSValue(CGPoint: fcPoint)
        
        //终点,分三种情况,
        let endXPoint:CGFloat!
        var endYPoint:CGFloat!
        let endPoint:CGPoint!
        let endPointValue:NSValue!
        let endTempYPoint:CGFloat!
        //Y轴-3db时实际屏幕坐标
        endTempYPoint = getYAxisValue(5)
        
        if fc < xCritical {
            //1.没到X轴的终点，(x,HN_BOTTOM_VALUE);
            if xCritical <= 700 {
            endXPoint = fcXPoint * 1.8 + 35
            }else{
            endXPoint = fcXPoint * 1.7 + 15
            }
            endPoint = CGPoint(x: endXPoint, y: HN_BOTTOM_VALUE)
            endPointValue = NSValue(CGPoint: endPoint)
            
        } else if fc >= xCritical && fc < yCritical {
            //2.到了x轴终点后，Y轴在[5,8]区间
            //getViewXLineValueFromFC(xCritical)为临界值，FC为xCritical时，终点为表格X轴的终点
            endYPoint = HN_BOTTOM_VALUE - (fcXPoint - getViewXLineValueFromFC(xCritical))
            if endYPoint < endTempYPoint {
              endYPoint = endTempYPoint
            }
            endPoint = CGPoint(x: horizontalLineEndXPos, y: endYPoint)
            endPointValue = NSValue(CGPoint: endPoint)
            
        } else {
            //3.到了x轴终点后，Y轴在[4,5]区间
            endYPoint = endTempYPoint - (fcXPoint/10 - getViewXLineValueFromFC(yCritical)/10)
            if endYPoint < centreYPos {
             endYPoint = centreYPos
            }
            endPoint = CGPoint(x: horizontalLineEndXPos, y: endYPoint)
            endPointValue = NSValue(CGPoint: endPoint)
            
        }
        
        pointValueArray.append(startPointValue)
        pointValueArray.append(fcPointValue)
        pointValueArray.append(endPointValue)
        
//        print("三个点分别是\(pointValueArray)")
        
        }else{
         //超出范围，直接画一条直线
         pointValueArray  = basicPoints()
        }
        
        return pointValueArray
        
    }
   
    
    //MARK:-低通Bessel2坐标点
    /**
     LowPassBessel2曲线
     - parameter fc: <#fc description#>
     - returns: 画LowPassBessel2曲线的四个坐标点
     */
    func drawLowPassBessel2(fc:Int)->[NSValue]{
     
      return drawLowPassCommom(fc, secondPosX: CGFloat(LOW_PASS_BESSEL_X_LENGTH),endPosX: CGFloat(23))
    
    }
    
    //MARK:-低通LinkwitzRiiley2，VariableQ2，chebychev坐标点
    /**
     LowPassLinkwitzRiiley2曲线;
     LowPassVariableQ2曲线 4个坐标点位置跟LowPassLinkwitzRiiley2一样，2个控制点不一样;
     LowPasschebychev曲线 4个坐标点位置跟LowPassLinkwitzRiiley2一样，2个控制点不一样;
     - parameter fc: <#fc description#>
     - returns: 画曲线的四个坐标点
     */
    func drawLowPassLinkwitzRiiley2(fc:Int)->[NSValue]{
      return drawLowPassCommom(fc, secondPosX: CGFloat(LOW_PASS_LINK_X_ADD),endPosX: CGFloat(12))
    }
    
    //MARK:-低通公用函数
    /**
     低通公用函数
     - parameter fc:         输入的FC值
     - parameter secondPosX: 第二个点的增益值
     - parameter endPosX: 最后一点的X轴的增益值
     - returns: 4个坐标的数组
     */
    func drawLowPassCommom(fc:Int,secondPosX:CGFloat,endPosX:CGFloat) ->[NSValue]{
        //返回坐标点的数组
        var pointValueArray = [NSValue]()
        
        if  (fc >= 20) && (fc <= 20000) {
        //开始点
        let startPoint = CGPoint(x: horizontalLineStartXPos, y: centreYPos)
        let startPointValue = NSValue(CGPoint: startPoint)
        
        //fc值
        var fcXPoint = getViewXLineValueFromFC(fc)
        if fc > 15118{
            fcXPoint = getViewXLineValueFromFC(15118)
        }
        let fcPoint = CGPoint(x: fcXPoint, y: centreYPos)
        let fcPointValue = NSValue(CGPoint: fcPoint)
        
        //第二个点
        var secondXPoint = fcXPoint + secondPosX
        let secondYPoint = centreYPos + LOW_PASS_BESSEL_Y_ADD
        if secondXPoint > horizontalLineEndXPos {
           secondXPoint = horizontalLineEndXPos
        }
        
        let secondPoint = CGPoint(x: secondXPoint, y: secondYPoint)
        let secondPointValue = NSValue(CGPoint: secondPoint)
        
        //终点
        
        let endXPoint:CGFloat!
        let endPoint:CGPoint!
        let endPointValue:NSValue!
        
        if (fc >= 20)&&(fc <= 35) {
            
            endPoint = CGPoint(x: getViewXLineValueFromFC(fc + 30), y: HN_BOTTOM_VALUE)
            
        }else if fc < 900 {
            
            endXPoint = fcXPoint * 1.7 + endPosX
            endPoint = CGPoint(x: endXPoint, y: HN_BOTTOM_VALUE)
            
            
        } else {
            
            endPoint = CGPoint(x: horizontalLineEndXPos, y: HN_BOTTOM_VALUE)
            
            
        }
        endPointValue = NSValue(CGPoint: endPoint)
            
        pointValueArray.append(startPointValue)
        pointValueArray.append(fcPointValue)
        pointValueArray.append(secondPointValue)
        pointValueArray.append(endPointValue)
        
//        print("四个点分别是\(pointValueArray)")
            
        }else{
            //超出范围，直接画一条直线
            pointValueArray  = basicPoints()
        }
        
        return pointValueArray
        
    }
    //MARK:-高通ButterWorth坐标点
    /**
     高通ButterWorth坐标点
     - parameter fc:          输入的FC值
     - parameter butterIndex: ButterWorth1为 1,ButterWorth2为 2
     - returns: <#return value description#>
     */
    func drawHighPassButter(fc:Int,butterIndex:Int) ->[NSValue]{
        //返回坐标点的数组
        var pointValueArray = [NSValue]()
        
        if  (fc >= 20) && (fc <= 20000) {
            
            //开始点，分6种情况,
            let startXFcPoint:Int!
            let startXPoint:CGFloat!
            let startPoint:CGPoint!
            let startPointValue:NSValue!
            
            if butterIndex == 1 {
                
                switch fc {
                case 20:
                    startXFcPoint = 20
                case 21...100:
                    startXFcPoint = 20 + (fc/5)
                case 101...1000:
                    startXFcPoint = 40 + (fc/10)
                case 1001...10000:
                    startXFcPoint = (Int)(130 + (CGFloat(fc)/100 * 2.8))
                case 10001...20000:
                    startXFcPoint = 400 + (fc - 10000)/1000 * 15
                default:
                    startXFcPoint = 20
                    
                }
            }else  {
                
                switch fc {
                case 20:
                    startXFcPoint = 23
                case 21...100:
                    startXFcPoint = 23 + (fc/5)
                case 101...1000:
                    startXFcPoint = 48 + (fc/10)
                case 1001...10000:
                    startXFcPoint = 148 + fc/100 * 3
                case 10001...20000:
                    startXFcPoint = 460 + (fc - 10000)/1000 * 15
                default:
                    startXFcPoint = 20
                    
                }
            }
            
            startXPoint = getViewXLineValueFromFC(startXFcPoint)
            startPoint = CGPoint(x: startXPoint, y: HN_BOTTOM_VALUE)
            startPointValue = NSValue(CGPoint: startPoint)
            
            //fc点
            var fcXPoint = getViewXLineValueFromFC(fc)
            
            if (fc >= 20) && (fc<=200){
                fcXPoint = getViewXLineValueFromFC(fc + 30)
            }
            let fcPoint = CGPoint(x: fcXPoint, y: centreYPos)
            let fcPointValue = NSValue(CGPoint: fcPoint)
            
            //终点
            
            let endPoint = CGPoint(x: horizontalLineEndXPos, y: centreYPos)
            let endPointValue = NSValue(CGPoint: endPoint)
            
            pointValueArray.append(startPointValue)
            pointValueArray.append(fcPointValue)
            pointValueArray.append(endPointValue)
            
        }else{
            //超出范围，直接画一条直线
            pointValueArray  = basicPoints()
        }
        
        return pointValueArray
        
    }
    
    //MARK:-高通BESSEL_2，LINKWITZ_RILEY_2坐标点
    /**
      高通BESSEL_2，LINKWITZ_RILEY_2坐标点
     - parameter fc:         输入的FC值
     - parameter secondPosX: 第二个点的增益值
     - parameter endPosX: 最后一点的X轴的增益值
     - returns: 4个坐标的数组
     */
    func drawHighPassBessel(fc:Int) ->[NSValue]{
        //返回坐标点的数组
        var pointValueArray = [NSValue]()
        
        if  (fc >= 20) && (fc <= 20000) {
        //开始点，分6种情况,
        let startXFcPoint:Int!
        let startXPoint:CGFloat!
        let startPoint:CGPoint!
        let startPointValue:NSValue!
        
        switch fc {
        case 20:
            startXFcPoint = 20
        case 21...100:
            startXFcPoint = 20 + (fc/5)
        case 101...1000:
            startXFcPoint = 48 + (fc/10)
        case 1001...10000:
            startXFcPoint = 148 + fc/100 * 3
        case 10001...20000:
            startXFcPoint = 460 + (fc - 10000)/1000 * 15
        default:
            startXFcPoint = 20
            
    }
        
        startXPoint = getViewXLineValueFromFC(startXFcPoint)
        startPoint = CGPoint(x: startXPoint, y: HN_BOTTOM_VALUE)
        startPointValue = NSValue(CGPoint: startPoint)
        
  
        //fc值
        var fcXPoint = getViewXLineValueFromFC(fc)
        if (fc >= 20)&&(fc<=200) {
            fcXPoint = getViewXLineValueFromFC(fc + 35)
        }
        let fcPoint = CGPoint(x: fcXPoint, y: centreYPos)
        let fcPointValue = NSValue(CGPoint: fcPoint)
        
//        //第二个点
//
//        var secondXPoint = fcXPoint - self.curveView.bounds.size.width * 0.0298
//        let secondYPoint = centreYPos + self.curveView.bounds.size.height * 0.0314
//        //越界点处理
//        if secondXPoint < HN_LEADING_VALUE {
//            secondXPoint = HN_LEADING_VALUE
//        }
//        
//        let secondPoint = CGPoint(x: secondXPoint, y: secondYPoint)
//        let secondPointValue = NSValue(CGPoint: secondPoint)
        
        
        //终点
        let endPoint = CGPoint(x: horizontalLineEndXPos, y: centreYPos)
        let endPointValue = NSValue(CGPoint: endPoint)
      
        
        pointValueArray.append(startPointValue)
//        pointValueArray.append(secondPointValue)
        pointValueArray.append(fcPointValue)
        pointValueArray.append(endPointValue)
        
//        print("四个点分别是\(pointValueArray)")
            
        }else{
            //超出范围，直接画一条直线
            pointValueArray  = basicPoints()
        }
        
        return pointValueArray
        
    }
    
    //MARK:-高通VARIABLE_Q2，CHEBYCHEV坐标点
    func drawHighPassVariable(fc:Int) ->[NSValue]{
        //返回坐标点的数组
        var pointValueArray = [NSValue]()
        
        if  (fc >= 20) && (fc <= 20000) {
            //开始点，分6种情况,
            let startXFcPoint:Int!
            let startXPoint:CGFloat!
            let startPoint:CGPoint!
            let startPointValue:NSValue!
            
            switch fc {
            case 20:
                startXFcPoint = 20
            case 21...100:
                startXFcPoint = 20 + (fc/5)
            case 101...1000:
                startXFcPoint = 48 + (fc/10)
            case 1001...10000:
                startXFcPoint = 148 + fc/100 * 3
            case 10001...20000:
                startXFcPoint = 460 + (fc - 10000)/1000 * 15
            default:
                startXFcPoint = 20
                
            }
            
            startXPoint = getViewXLineValueFromFC(startXFcPoint)
            startPoint = CGPoint(x: startXPoint, y: HN_BOTTOM_VALUE)
            startPointValue = NSValue(CGPoint: startPoint)
            
            
            //fc值
            var fcXPoint = getViewXLineValueFromFC(fc)
            if (fc >= 20)&&(fc<=200) {
                fcXPoint = getViewXLineValueFromFC(fc + 30)
            }
            let fcPoint = CGPoint(x: fcXPoint, y: centreYPos)
            let fcPointValue = NSValue(CGPoint: fcPoint)
            
            //第二个点
    
            var secondXPoint = fcXPoint - self.curveView.bounds.size.width * 0.0798
            let secondYPoint = centreYPos + self.curveView.bounds.size.height * 0.0414
            //越界点处理
            if secondXPoint < HN_LEADING_VALUE {
                secondXPoint = HN_LEADING_VALUE
            }
    
            let secondPoint = CGPoint(x: secondXPoint, y: secondYPoint)
            let secondPointValue = NSValue(CGPoint: secondPoint)
            
            
            //终点
            let endPoint = CGPoint(x: horizontalLineEndXPos, y: centreYPos)
            let endPointValue = NSValue(CGPoint: endPoint)
            
            
            pointValueArray.append(startPointValue)
            pointValueArray.append(secondPointValue)
            pointValueArray.append(fcPointValue)
            pointValueArray.append(endPointValue)
            
//            print("四个点分别是\(pointValueArray)")
            
        }else{
            //超出范围，直接画一条直线
            pointValueArray  = basicPoints()
        }
        
        return pointValueArray
        
    }
    
    //MARK:-高低通画曲线方法
    func layoutCurve(fc:Int,passType:PassType.RawValue,subType:SubMode.RawValue){
        
        var pointValueArray:[NSValue]
        
        //调用画贝塞尔曲线的方法
        let curveDrawer = HNCurveDrawer()
        curveDrawer.curveView = self.curveView
        
        switch passType {
        //高通
        case PassType.PASS_TYPE_HIGH.rawValue:
            //高通的各个子类型
            switch subType {
            case SubMode.BUTTER_WORTH_1.rawValue:
                pointValueArray = drawHighPassButter(fc, butterIndex: ButterWorth1Index)
                curveDrawer.drawCurveFor(1, filterSubType:2, usePoints: pointValueArray)
            case SubMode.BUTTER_WORTH_2.rawValue:
                pointValueArray = drawHighPassButter(fc, butterIndex: ButterWorth2Index)
                curveDrawer.drawCurveFor(1, filterSubType:3, usePoints: pointValueArray)
            case SubMode.BESSEL_2.rawValue:
                pointValueArray = drawHighPassBessel(fc)
                curveDrawer.drawCurveFor(1, filterSubType:4, usePoints: pointValueArray)
            case SubMode.LINKWITZ_RILEY_2.rawValue:
                pointValueArray = drawHighPassBessel(fc)
                curveDrawer.drawCurveFor(1, filterSubType:5, usePoints: pointValueArray)
            case SubMode.VARIABLE_Q2.rawValue:
                //三个点是一样，更改控制点的位置
                pointValueArray = drawHighPassVariable(fc)
                curveDrawer.drawCurveFor(1, filterSubType:6, usePoints: pointValueArray)
            case SubMode.CHEBYCHEV.rawValue:
                //三个点是一样，更改控制点的位置
                pointValueArray = drawHighPassVariable(fc)
                curveDrawer.drawCurveFor(1, filterSubType:7, usePoints: pointValueArray)
            default:
                break
            
            }
        //低通
        case PassType.PASS_TYPE_LOW.rawValue:
            //低通的各个子类型
            switch subType {
            case SubMode.BUTTER_WORTH_1.rawValue:
                pointValueArray = drawLowPassButter1(fc)
                curveDrawer.drawCurveFor(1, filterSubType:8, usePoints: pointValueArray)
            case SubMode.BUTTER_WORTH_2.rawValue:
                pointValueArray = drawLowPassButter2(fc)
                curveDrawer.drawCurveFor(1, filterSubType:9, usePoints: pointValueArray)
            case SubMode.BESSEL_2.rawValue:
                pointValueArray = drawLowPassBessel2(fc)
                curveDrawer.drawCurveFor(1, filterSubType:10, usePoints: pointValueArray)
            case SubMode.LINKWITZ_RILEY_2.rawValue:
                pointValueArray = drawLowPassLinkwitzRiiley2(fc)
                curveDrawer.drawCurveFor(1, filterSubType:11, usePoints: pointValueArray)
            case SubMode.VARIABLE_Q2.rawValue:
                pointValueArray = drawLowPassLinkwitzRiiley2(fc)
                curveDrawer.drawCurveFor(1, filterSubType:12, usePoints: pointValueArray)
            case SubMode.CHEBYCHEV.rawValue:
                pointValueArray = drawLowPassLinkwitzRiiley2(fc)
                curveDrawer.drawCurveFor(1, filterSubType:13, usePoints: pointValueArray)
            default:
                break
                
            }
            
        default:
            break
        
        }
        
    }
    
    //MARK:-预留的高低音画曲线方法
    func layoutCurve(fc:Int,pitchType:PitchType){
        
        var pointValueArray:[NSValue]
        
        //调用画贝塞尔曲线的方法
        let curveDrawer = HNCurveDrawer()
        curveDrawer.curveView = self.curveView
        
        switch pitchType.rawValue {
            
        case PitchType.PITCH_TYPE_BASS.rawValue:
            pointValueArray = basicPoints()
            curveDrawer.drawCurveFor(1, filterSubType:0, usePoints: pointValueArray)
            
        case PitchType.PITCH_TYPE_TREBLE.rawValue:
            pointValueArray = basicPoints()
            curveDrawer.drawCurveFor(1, filterSubType:1, usePoints: pointValueArray)
     
        default:
            break
            
        }
        
    }
    

    //MARK:-画一条直线所需的点
    /**
     基本的两个坐标点，用于输入不是20-20K时，直接画一条直线
     - returns: <#return value description#>
     */
    func basicPoints()->[NSValue]{
        
       var pointValueArray = [NSValue]()
       let bounds = curveView.bounds
        
       let pointA = CGPoint(x: HN_LEADING_VALUE + HN_V_GROUP_SPACE_VC * CGFloat(0), y: bounds.size.height / 2)
       pointValueArray.append(NSValue(CGPoint: pointA))
        
       let pointB = CGPoint(x: HN_LEADING_VALUE + getXAxisRealValue(0) + HN_V_GROUP_SPACE_VC * CGFloat(1), y: bounds.size.height / 2)
        pointValueArray.append(NSValue(CGPoint: pointB))
        
       let pointC = CGPoint(x: HN_LEADING_VALUE + getXAxisRealValue(1) + HN_V_GROUP_SPACE_VC * CGFloat(2), y: bounds.size.height / 2)
        pointValueArray.append(NSValue(CGPoint: pointC))
        
       let pointD = CGPoint(x: HN_V_GROUP_SPACE_VC * CGFloat(3), y: bounds.size.height / 2)
       pointValueArray.append(NSValue(CGPoint: pointD))
        
      return pointValueArray
    }
    
    //MARK:-画一条直线
    func drawALine(){
        
        NSNotificationCenter.defaultCenter().postNotificationName(kNotificationFilterSliderValueChanged, object: nil, userInfo: [EQSliderValue:0,EQSliderTag:0])
        
        let curveDrawer = HNCurveDrawer()
        curveDrawer.curveView = self.curveView
        curveDrawer.drawCurveFor(1, filterSubType: 99, usePoints: basicPoints())
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}