//
//  main.m
//  IOMix
//
//  Created by Chiwen on 2015/6/15.
//  Copyright (c) 2015年 Homni Enterprises Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
