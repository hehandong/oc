//
//  HNMonitorViewController.m
//  IOMix
//
//  Created by YuHeng_Antony on 15/7/8.
//  Copyright (c) 2015年 Homni Enterprises Co., Ltd. All rights reserved.
//

#import "HNMonitorViewController.h"
#import "HNIOMixManager.h"
#import "HNIOMixDevice.h"
#import "HNChannel.h"

/**
 *  输出通道号
 */
typedef NS_ENUM(NSInteger, OUTPUT_CHANNEL) {
    /**
     *  输出通道号0x00
     */
    OUTPUT_CHANNEL_L_R = 0x00,
    /**
     *  输出通道号0x01
     */
    OUTPUT_CHANNEL_OUTPUT1_2,
    /**
     *  输出通道号0x02
     */
    OUTPUT_CHANNEL_AUXSEND1_2,
};

/**
 *  输入通道选择
 */
typedef NS_ENUM(NSInteger, INPUT_CHANNEL) {
    /**
     *  没有选择任何输入通道0x00
     */
    INPUT_CHANNEL_NON_INPUT = 0x00,
    /**
     *  输入通道选择0x01
     */
    INPUT_CHANNEL_CH1_2,
    /**
     *  输入通道选择0x02
     */
    INPUT_CHANNEL_CH3_4,
    /**
     *  输入通道选择0x03
     */
    INPUT_CHANNEL_CH5_6,
    /**
     *  输入通道选择0x04
     */
    INPUT_CHANNEL_CH7_8,
};

// 输入通道按钮(对应xib中的tag值)
typedef NS_ENUM(NSInteger, INPUT_BUTTON) {
    INPUT_BUTTON_CH1 = 1,
    INPUT_BUTTON_CH2,
    INPUT_BUTTON_CH3,
    INPUT_BUTTON_CH4,
    INPUT_BUTTON_CH5,
    INPUT_BUTTON_CH6,
    INPUT_BUTTON_CH7,
    INPUT_BUTTON_CH8,
};

@interface HNMonitorViewController ()
/**
 *  L、R区域的按钮
 */
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *lrInputButtons;

/**
 *  OUTPUT1、2区域的按钮
 */
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *outputInputButtons;

/**
 *  AUX SEND1、2区域的按钮
 */
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *auxSendInputButtons;

/**
 *  MAIN OUTPUT区域的两个按钮
 */
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *mainOutputButtons;

/**
 *  MAIN OUTPUT按钮是否已经选择
 */
@property (nonatomic) BOOL isMainOutputSelected;

@property (nonatomic ,strong) UIAlertView *mainOutputNonSelectedAlertView;

@property (nonatomic, copy) NSArray *allChannels;
@end

@implementation HNMonitorViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _allChannels = [HNIOMixManager shareManager].allChannel;
    
    // 设置各个按钮状态
    [self setupButtonsState];
}

// 根据本地保存数据，设置按钮状态
- (void)setupButtonsState {
    // 输出通道，用于区分要设置哪个按钮数组(所以用一个switch来？)
    // 再判断输入通道，用于要点亮那两个按钮。四种情况(也用switch？)，
    
    
    HNChannel *channelForMonitor = _allChannels[0];
    
    [self setupInputButtonsState:_lrInputButtons inputValue:channelForMonitor.lrInputValue];
    [self setupInputButtonsState:_outputInputButtons inputValue:channelForMonitor.mainInputValue];
    [self setupInputButtonsState:_auxSendInputButtons inputValue:channelForMonitor.auxInputValue];
    
    // 配置MAIN OUTPUT两个按钮的状态
    if (channelForMonitor.mainInputValue == 0) {
        for (UIButton *button in _mainOutputButtons) {
            button.selected = NO;
        }
    } else {
        for (UIButton *button in _mainOutputButtons) {
            button.selected = YES;
            _isMainOutputSelected = YES;
        }
    }
}

- (void)setupInputButtonsState:(NSArray *)buttons inputValue:(NSInteger)inputValue {
    switch (inputValue) {
        case INPUT_CHANNEL_CH1_2:
            for (UIButton *button in buttons) {
                if (button.tag == INPUT_BUTTON_CH1 || button.tag == INPUT_BUTTON_CH2) {
                    button.selected = YES;
                } else {
                    button.selected = NO;
                }
            }
            break;
            
        case INPUT_CHANNEL_CH3_4:
            for (UIButton *button in buttons) {
                if (button.tag == INPUT_BUTTON_CH3 || button.tag == INPUT_BUTTON_CH4) {
                    button.selected = YES;
                } else {
                    button.selected = NO;
                }
            }
            break;
            
        case INPUT_CHANNEL_CH5_6:
            for (UIButton *button in buttons) {
                if (button.tag == INPUT_BUTTON_CH5 || button.tag == INPUT_BUTTON_CH6) {
                    button.selected = YES;
                } else {
                    button.selected = NO;
                }
            }
            break;
            
        case INPUT_CHANNEL_CH7_8:
            for (UIButton *button in buttons) {
                if (button.tag == INPUT_BUTTON_CH7 || button.tag == INPUT_BUTTON_CH8) {
                    button.selected = YES;
                } else {
                    button.selected = NO;
                }
            }
            break;
            
        default:
            break;
    }
}

#pragma mark - L、R区域按钮的点击事件
- (IBAction)outputChannelLRAreaAction:(UIButton *)sender {
    [self setupButtonStata:_lrInputButtons sendCommand:OUTPUT_CHANNEL_L_R forSender:sender];
}

#pragma mark OUTPUT1、2区域按钮的点击事件
- (IBAction)outputChannelOutputAreaAction:(UIButton *)sender {
    // MAIN OUTPUT按钮打开后，才能发送指令
    if (_isMainOutputSelected) {
        [self setupButtonStata:_outputInputButtons sendCommand:OUTPUT_CHANNEL_OUTPUT1_2 forSender:sender];
    } else {
        // 提醒使用者
        if (!_mainOutputNonSelectedAlertView) {
            _mainOutputNonSelectedAlertView = [[UIAlertView alloc] initWithTitle:@"NOTE" message:@"Please Select MAIN OUTPUT First." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        }
        [_mainOutputNonSelectedAlertView show];
    }
}

#pragma mark AUX SEND1、2区域按钮的点击事件
- (IBAction)outputChannelAuxSendAreaAction:(UIButton *)sender {
    [self setupButtonStata:_auxSendInputButtons sendCommand:OUTPUT_CHANNEL_AUXSEND1_2 forSender:sender];
}

#pragma mark 点击事件Helper Method
- (void)setupButtonStata:(NSArray *)buttons sendCommand:(NSInteger)outputChannelIndex forSender:(UIButton *)sender {
    sender.selected = !sender.selected;
    // 如果点击按钮的tag是奇数，点亮，并且tag加1，再点亮；遍历所有按钮，如果按钮tag值与当前点击按钮tag值以及加1的值同时不相等，切为非选择状态
    // 如果点击按钮的tag是偶数，点亮，并且tag减1，再点亮；遍历所有按钮，如果按钮tag值与当前点击按钮tag值以及减1的值同时不相等，切为非选择状态
    if (sender.tag % 2) {
        // 点击了奇数tag按钮
        [buttons enumerateObjectsUsingBlock:^(UIButton  *_Nonnull button, NSUInteger idx, BOOL * _Nonnull stop) {
            if (button.tag == sender.tag + 1) {
                button.selected = sender.selected;
            }
            if (!(button.tag == sender.tag) && !(button.tag == sender.tag + 1)) {
                button.selected = NO;
            }
        }];
        
        // 本地数据保存
        // 指令发送
        if (sender.selected) {
            switch (sender.tag) {
                case INPUT_BUTTON_CH1:
                    [self saveDataFor:outputChannelIndex inputChannel:INPUT_CHANNEL_CH1_2];
                    [[HNIOMixManager shareManager].device setupOutputChannel:outputChannelIndex inputChannel:INPUT_CHANNEL_CH1_2];
                    break;
                case INPUT_BUTTON_CH3:
                    [self saveDataFor:outputChannelIndex inputChannel:INPUT_CHANNEL_CH3_4];
                    [[HNIOMixManager shareManager].device setupOutputChannel:outputChannelIndex inputChannel:INPUT_CHANNEL_CH3_4];
                    break;
                case INPUT_BUTTON_CH5:
                    [self saveDataFor:outputChannelIndex inputChannel:INPUT_CHANNEL_CH5_6];
                    [[HNIOMixManager shareManager].device setupOutputChannel:outputChannelIndex inputChannel:INPUT_CHANNEL_CH5_6];
                    break;
                case INPUT_BUTTON_CH7:
                    [self saveDataFor:outputChannelIndex inputChannel:INPUT_CHANNEL_CH7_8];
                    [[HNIOMixManager shareManager].device setupOutputChannel:outputChannelIndex inputChannel:INPUT_CHANNEL_CH7_8];
                    break;
                default:
                    break;
            }
        }
    } else {
        // 点击了偶数tag按钮
        [buttons enumerateObjectsUsingBlock:^(UIButton  *_Nonnull button, NSUInteger idx, BOOL * _Nonnull stop) {
            if (button.tag == sender.tag - 1) {
                button.selected = sender.selected;
            }
            if (!(button.tag == sender.tag) && !(button.tag == sender.tag - 1)) {
                button.selected = NO;
            }
        }];
        
        // 本地数据保存
        // 指令发送
        if (sender.selected) {
            switch (sender.tag) {
                case INPUT_BUTTON_CH2:
                    [self saveDataFor:outputChannelIndex inputChannel:INPUT_CHANNEL_CH1_2];
                    [[HNIOMixManager shareManager].device setupOutputChannel:outputChannelIndex inputChannel:INPUT_CHANNEL_CH1_2];
                    break;
                case INPUT_BUTTON_CH4:
                    [self saveDataFor:outputChannelIndex inputChannel:INPUT_CHANNEL_CH3_4];
                    [[HNIOMixManager shareManager].device setupOutputChannel:outputChannelIndex inputChannel:INPUT_CHANNEL_CH3_4];
                    break;
                case INPUT_BUTTON_CH6:
                    [self saveDataFor:outputChannelIndex inputChannel:INPUT_CHANNEL_CH5_6];
                    [[HNIOMixManager shareManager].device setupOutputChannel:outputChannelIndex inputChannel:INPUT_CHANNEL_CH5_6];
                    break;
                case INPUT_BUTTON_CH8:
                    [self saveDataFor:outputChannelIndex inputChannel:INPUT_CHANNEL_CH7_8];
                    [[HNIOMixManager shareManager].device setupOutputChannel:outputChannelIndex inputChannel:INPUT_CHANNEL_CH7_8];
                    break;
                default:
                    break;
            }
        }
    }
    
    // 判断在没有选择任何按钮的情况下，发送对应指令
    __block BOOL isSelected = NO;
    [buttons enumerateObjectsUsingBlock:^(UIButton  *_Nonnull button, NSUInteger idx, BOOL * _Nonnull stop) {
        if (button.selected) {
            isSelected = YES;
        }
    }];
    if (!isSelected) {
        [self saveDataFor:outputChannelIndex inputChannel:INPUT_CHANNEL_NON_INPUT];
        [[HNIOMixManager shareManager].device setupOutputChannel:outputChannelIndex inputChannel:INPUT_CHANNEL_NON_INPUT];
    }
}

#pragma mark MAIN OUTPUT按钮的点击事件
- (IBAction)mainOutputAction:(UIButton *)sender {
    // 配置按钮状态
   [_mainOutputButtons enumerateObjectsUsingBlock:^(UIButton  *_Nonnull button, NSUInteger idx, BOOL * _Nonnull stop) {
       button.selected = !button.selected;
   }];
   
    // 指令发送
    if (sender.selected) {
        _isMainOutputSelected = YES;
        // 发送指令,1、输出通道号固定为OUTPUT_CHANNEL_OUTPUT1_2，输入通道号为OUTPUT1_2区域按钮状态
        __block NSInteger inputChannelIndex = INPUT_CHANNEL_NON_INPUT;
        [_outputInputButtons enumerateObjectsUsingBlock:^(UIButton  *_Nonnull button, NSUInteger idx, BOOL * _Nonnull stop) {
            if (button.selected) {
                switch (button.tag) {
                    case INPUT_BUTTON_CH1:
                        inputChannelIndex = INPUT_CHANNEL_CH1_2;
                        break;
                    case INPUT_BUTTON_CH3:
                        inputChannelIndex = INPUT_CHANNEL_CH3_4;
                        break;
                    case INPUT_BUTTON_CH5:
                        inputChannelIndex = INPUT_CHANNEL_CH5_6;
                        break;
                    case INPUT_BUTTON_CH7:
                        inputChannelIndex = INPUT_CHANNEL_CH7_8;
                        break;
                    default:
                        break;
                }
                *stop = YES;
            }
        }];
        [[HNIOMixManager shareManager].device setupOutputChannel:OUTPUT_CHANNEL_OUTPUT1_2 inputChannel:inputChannelIndex];
    } else {
        _isMainOutputSelected = NO;
        // MAIN OUTPUT按钮关闭，无论输入通道号是否有选择，都当是没有选择
        [[HNIOMixManager shareManager].device setupOutputChannel:OUTPUT_CHANNEL_OUTPUT1_2 inputChannel:INPUT_CHANNEL_NON_INPUT];
    }
}

#pragma mark -
- (void)saveDataFor:(NSInteger)outputChannel inputChannel:(NSInteger)inputChannel {
    switch (outputChannel) {
        case OUTPUT_CHANNEL_L_R:
            [_allChannels[0] setLrInputValue:inputChannel];
            break;
        case OUTPUT_CHANNEL_OUTPUT1_2:
            [_allChannels[0] setMainInputValue:inputChannel];
            break;
        case OUTPUT_CHANNEL_AUXSEND1_2:
            [_allChannels[0] setAuxInputValue:inputChannel];
            break;
        default:
            break;
    }
}
@end
