//
//  HNChannelStore.h
//  IOMix
//
//  Created by YuHeng_Antony on 15/7/10.
//  Copyright (c) 2015年 Homni Enterprises Co., Ltd. All rights reserved.
//
/// 负责沙盒数据的储存

#import <Foundation/Foundation.h>
@class HNChannel, HNEqualizer;

@interface HNChannelStore : NSObject

@property (nonatomic, readonly) NSArray *allChannels;

+ (instancetype)sharedStore;

/**
 *  HOME页面数据保存接口方法
 *
 *  @param volumValue   音量
 *  @param isSYNC       sync状态
 *  @param customEQName 自定义EQ名称
 *  @param isMUTE       静音状态
 *  @param channelID    channelID(注意表格中的channel索引从1开始)
 */
- (void)updateHomeVolume:(NSInteger)volumValue sSYNC:(BOOL)isSYNC eqName:(NSString *)customEQName isMUTE:(BOOL)isMUTE forChannel:(NSInteger)channelID;

/**
 *  EQ页面数据保存接口方法
 *
 *  @param equalizer  HNEqualizer对象
 *  @param effectMode 选择的eq音效
 *  @param channelID  channelID(注意表格中的channel索引从1开始)
 */
- (void)updateEqualizerValues:(HNEqualizer *)equalizer effectMode:(NSInteger)effectMode forChannel:(NSInteger)channelID;

/// FILTER页面数据保存接口方法
- (void)updateFilterPitchType:(NSInteger)pitchType
                bassGainValue:(NSInteger)bassGainValue
              trebleGainValue:(NSInteger)trebleGainValue
                     passType:(NSInteger)passType
              highPassSubType:(NSInteger)highPassSubType
       highPassfrequencyValue:(NSInteger)highPassfrequencyValue
         isAllpassForHighpass:(BOOL)isAllpassForHighpass
               lowPassSubType:(NSInteger)lowPassSubType
        lowPassfrequencyValue:(NSInteger)lowPassfrequencyValue
          isAllpassForLowpass:(BOOL)isAllpassForLowpass
                   forChannel:(NSInteger)channelID;

/**
 *  MONITOR页面数据保存接口方法
 *
 *  @param lrinputValue   LR部分的输入通道值
 *  @param mainInputValue MAIN OUTPUT部分的输入通道值
 *  @param auxInputValue  AUX部分的输入通道值
 *  @param channelID      其实不应该是channel的属性，这里借Channel1的地方保存这三个数值
 */
- (void)updateMonitorLrInputValue:(NSInteger)lrinputValue mainInputValue:(NSInteger)mainInputValue auxInputValue:(NSInteger)auxInputValue forChannel:(NSInteger)channelID;

- (void)getData;

@end
