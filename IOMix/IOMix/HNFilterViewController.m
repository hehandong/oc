//
//  HNFilterViewController.m
//  IOMix
//
//  Created by YuHeng_Antony on 15/7/8.
//  Copyright (c) 2015年 Homni Enterprises Co., Ltd. All rights reserved.
//

#import "HNFilterViewController.h"
#import "HNPublic.h"
#import "HNPointControl.h"
#import "HNCurveDrawer.h"
#import "ASValueTrackingSlider.h"
#import "HNIOMixManager.h"
#import "HNIOMixDevice.h"
#import "HNChannel.h"

// 高低音中的两个按钮
typedef NS_ENUM(NSInteger, PITCH_TYPE) {
    PITCH_TYPE_BASS = 0,
    PITCH_TYPE_TREBLE,
};

// 高低通中的两个按钮
typedef NS_ENUM(NSInteger, PASS_TYPE) {
    PASS_TYPE_HIGH = 0,
    PASS_TYPE_LOW,
};

// allPass两个按钮
typedef NS_ENUM(NSInteger, ALL_PASS) {
    ALL_PASS_FOR_HIGH = 0,
    ALL_PASS_FOR_LOW,
};

// 两个PickerView
typedef NS_ENUM(NSInteger, PICKER_VIEW) {
    PICKER_VIEW_HIGH_PASS = 0,
    PICKER_VIEW_LOW_PASS,
};

@interface HNFilterViewController ()<UIPickerViewDelegate, UIPickerViewDataSource>
/**
 *  绘制曲线图的背景View
 */
@property (weak, nonatomic) IBOutlet UIView *curveView;

/**
 *  10个channel按钮
 */
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *channelButtons;

/**
 *  高低音值设置slider
 */
@property (weak, nonatomic) IBOutlet ASValueTrackingSlider *bassTrebleSlider;
@property (weak, nonatomic) IBOutlet UILabel *bassTrebleValueLabel;

/**
 *  高低通值设置slider
 */
@property (weak, nonatomic) IBOutlet ASValueTrackingSlider *hightLowPassSlider;
@property (weak, nonatomic) IBOutlet UILabel *highLowPassValueLabel;
@property (weak, nonatomic) IBOutlet UIButton *allPassButton;
@property (weak, nonatomic) IBOutlet UIPickerView *highPassPickerView;
@property (weak, nonatomic) IBOutlet UIPickerView *lowPassPickerView;


@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *bassTrebleButtons;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *highLowPassButtons;

@property (strong, nonatomic) UIAlertView *nonSeletPitchTypeAlertView;

/**
 *  使用者选择的Filter Subtype类型(就是pickerView选择的)
 */
@property (nonatomic) NSInteger currentPassSubType;

@property (strong, nonatomic) UIAlertView *nonSeletPassTypeAlertView;

@property (copy, nonatomic) NSArray *allChannels;

@property (strong, nonatomic) HNChannel *channelInUse;

@property (copy, nonatomic) NSArray *filterSubTypes;

@end

@implementation HNFilterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _filterSubTypes = @[@"BUTTER_WORTH_1",@"BUTTER_WORTH_2",@"BESSEL_2",@"LINKWITZ_RILEY_2",@"VARIABLE_Q2",@"CHEBYCHEV"];

    _allChannels = [HNIOMixManager shareManager].allChannel;
    
    [self layoutSlider];
    
    // 绘制曲线
    [self layoutCurve];
    
    // 默认CH1点亮
    [self toggleChannel:_channelButtons[0]];
}

#pragma mark - 设值的两条slider配置
- (void)layoutSlider {
    [_bassTrebleSlider setThumbImage:[UIImage imageNamed:@"sliderButton"] forState:UIControlStateNormal];
    _bassTrebleSlider.popUpViewColor = [UIColor clearColor];
    _bassTrebleSlider.textColor = [UIColor clearColor];// 隐藏popView
    [_bassTrebleSlider setMaxFractionDigitsDisplayed:0];
    _bassTrebleSlider.minimumValue = -12.0;
    _bassTrebleSlider.maximumValue = 12.0;
    [_bassTrebleSlider setMaximumTrackImage:[UIImage imageNamed:@"sliderTrackImage"] forState:UIControlStateNormal];
    [_bassTrebleSlider setMaximumTrackImage:[UIImage imageNamed:@"sliderTrackImage"] forState:UIControlStateNormal];

    [_hightLowPassSlider setThumbImage:[UIImage imageNamed:@"sliderButton"] forState:UIControlStateNormal];
    _hightLowPassSlider.popUpViewColor = [UIColor clearColor];
    _hightLowPassSlider.textColor = [UIColor clearColor];// 隐藏popView
    [_hightLowPassSlider setMaxFractionDigitsDisplayed:0];
    _hightLowPassSlider.minimumValue = 20.0;
    _hightLowPassSlider.maximumValue = 20000.0;
    [_hightLowPassSlider setMaximumTrackImage:[UIImage imageNamed:@"sliderTrackImage"] forState:UIControlStateNormal];
    [_hightLowPassSlider setMaximumTrackImage:[UIImage imageNamed:@"sliderTrackImage"] forState:UIControlStateNormal];
}

#pragma mark - layoutEQCurveView(曲线图)
- (void)layoutCurve {
    // 调用绘图类，传入10个点进行绘图(10个点要自己根据curveView及相关比例计算出来)
    HNCurveDrawer *drawer = [[HNCurveDrawer alloc] init];
    drawer.curveView = self.curveView;
    
    NSMutableArray *points = [NSMutableArray new];
    CGRect bounds = _curveView.bounds;
    for (int i = 0; i < 3; i++) {
        CGPoint pointA = CGPointMake(HN_LEADING_VALUE +
                                     HN_V_GROUP_SPACE_VC * i,
                                     bounds.size.height / 2);
        NSValue *pointAValue = [NSValue valueWithCGPoint:pointA];
        [points addObject:pointAValue];
        
        CGPoint pointB = CGPointMake(HN_LEADING_VALUE +
                                     HN_GET_V_SPACE_WITH_VC(HN_V_SPACE_L1toL2) +
                                     HN_GET_V_SPACE_WITH_VC(HN_V_SPACE_L2toL3) +
                                     HN_GET_V_SPACE_WITH_VC(HN_V_SPACE_L3toL4) +
                                     HN_V_GROUP_SPACE_VC * i,
                                     bounds.size.height / 2);
        NSValue *pointBValue = [NSValue valueWithCGPoint:pointB];
        [points addObject:pointBValue];
        
        CGPoint pointC = CGPointMake(HN_LEADING_VALUE +
                                     HN_GET_V_SPACE_WITH_VC(HN_V_SPACE_L1toL2) +
                                     HN_GET_V_SPACE_WITH_VC(HN_V_SPACE_L2toL3) +
                                     HN_GET_V_SPACE_WITH_VC(HN_V_SPACE_L3toL4) +
                                     HN_GET_V_SPACE_WITH_VC(HN_V_SPACE_L4toL5) +
                                     HN_GET_V_SPACE_WITH_VC(HN_V_SPACE_L5toL6) +
                                     HN_GET_V_SPACE_WITH_VC(HN_V_SPACE_L6toL7) +
                                     HN_GET_V_SPACE_WITH_VC(HN_V_SPACE_L7toL8) +
                                     HN_GET_V_SPACE_WITH_VC(HN_V_SPACE_L8toL9) +
                                     HN_V_GROUP_SPACE_VC * i,
                                     bounds.size.height / 2);
        NSValue *pointCValue = [NSValue valueWithCGPoint:pointC];
        [points addObject:pointCValue];
    }
    
    CGPoint pointZ = CGPointMake(HN_TRAILING_VALUE, bounds.size.height / 2);
    NSValue *pointZValue = [NSValue valueWithCGPoint:pointZ];
    [points addObject:pointZValue];
    
//    [points enumerateObjectsUsingBlock:^(NSValue  *_Nonnull pointValue, NSUInteger idx, BOOL * _Nonnull stop) {
//        NSLog(@"第%d个点的值是%@", idx, pointValue);
//    }];
    
    // 调用绘图类绘制曲线(传入要绘制的点坐标即可)
    [drawer drawCurveFor:HN_CURVE_FILTER filterSubType:0 UsePoints:points];
}

#pragma mark - 按钮点击事件
#pragma mark Channel Toggle
- (IBAction)toggleChannel:(UIButton *)sender {
    // 切换channel,设置channelInUse
    [HNIOMixManager shareManager].channelInUse = [HNIOMixManager shareManager].allChannel[sender.tag];
    _channelInUse = [HNIOMixManager shareManager].channelInUse;
    
    
    // 配置channel按钮状态
    [self setupButtonsState:_channelButtons forCurrentButton:sender];
    
    // 配置高低音按钮状态及slider值、label值
    // 默认显示低音按钮为选中状态
    // 显示slider的值
    [self bassTrebleToggle:_bassTrebleButtons[_channelInUse.pitchType]];
    if (_channelInUse.pitchType == 0) {
        _bassTrebleSlider.value = _channelInUse.bassGainValue;
        _bassTrebleValueLabel.text = [@(_channelInUse.bassGainValue) stringValue];
    } else {
        _bassTrebleSlider.value = _channelInUse.trebleGainValue;
        _bassTrebleValueLabel.text = [@(_channelInUse.trebleGainValue) stringValue];
    }

    // 配置高低通按钮状态、subType类型、All Pass按钮状态、slider的值、label值
    [self highLowPassToggle:_highLowPassButtons[_channelInUse.passType]];
    if (_channelInUse.passType == 0) {
        _hightLowPassSlider.value = _channelInUse.highPassfrequencyValue;
        _highLowPassValueLabel.text = [@(_channelInUse.highPassfrequencyValue) stringValue];
    } else {
        _hightLowPassSlider.value = _channelInUse.lowPassfrequencyValue;
        _highLowPassValueLabel.text = [@(_channelInUse.lowPassfrequencyValue) stringValue];
    }
    
    // 配置pickerView状态
    [_highPassPickerView selectRow:_channelInUse.highPassSubType inComponent:0 animated:YES];
    [_lowPassPickerView selectRow:_channelInUse.lowPassSubType inComponent:0 animated:YES];
}

#pragma mark 单选更新按钮状态Helper Method
- (void)setupButtonsState:(NSArray *)buttons forCurrentButton:(UIButton *)sender {
    sender.selected = YES;
    [buttons enumerateObjectsUsingBlock:^(UIButton  *_Nonnull button, NSUInteger idx, BOOL * _Nonnull stop) {
        if (sender.selected) {
            if (!(button == sender)) {
                button.selected = NO;
            }
        }
    }];
}

#pragma mark - 高低音处理
#pragma mark 按钮点击事件
- (IBAction)bassTrebleToggle:(UIButton *)sender {
    // 配置按钮状态
    [self setupButtonsState:_bassTrebleButtons forCurrentButton:sender];
    
    // 记录选择的是高音还是低音(保存数据)
    // 设置对应的值
    switch (sender.tag) {
        case PITCH_TYPE_BASS:
            // 保存数据
            _channelInUse.pitchType = PITCH_TYPE_BASS;
            // 更新slider值
            _bassTrebleSlider.value = _channelInUse.bassGainValue;
            break;
        case PITCH_TYPE_TREBLE:
            _channelInUse.pitchType = PITCH_TYPE_TREBLE;
            _bassTrebleSlider.value = _channelInUse.trebleGainValue;
            break;
        default:
            break;
    }
}
#pragma mark Slider滑动事件
- (IBAction)bassTrebleSliderValueChanged:(ASValueTrackingSlider *)sender {
    _bassTrebleValueLabel.text = [@((NSInteger)sender.value) stringValue];
    switch (_channelInUse.pitchType) {
        case PITCH_TYPE_BASS:
#pragma mark 发送0x14低音控制指令
            [[HNIOMixManager shareManager].device setupBassForChannel:[HNIOMixManager shareManager].channelInUse.channelIndex bassGainValue:sender.value];
            // 保存数据
            _channelInUse.bassGainValue = (NSInteger)sender.value;
            break;
        case PITCH_TYPE_TREBLE:
#pragma mark 发送0x15高音控制指令
            [[HNIOMixManager shareManager].device setupTrebleForChannel:[HNIOMixManager shareManager].channelInUse.channelIndex trebleGainValue:sender.value];
            // 保存数据
            _channelInUse.trebleGainValue = (NSInteger)sender.value;
            break;
        default:
            if (!_nonSeletPitchTypeAlertView) {
                _nonSeletPitchTypeAlertView = [[UIAlertView alloc] initWithTitle:@"NOTE" message:@"Please select Bass Shelf or Treble Shelf" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            }
            [_nonSeletPitchTypeAlertView show];
            break;
    }
}

#pragma mark - 高低通处理
#pragma mark 按钮点击事件
- (IBAction)highLowPassToggle:(UIButton *)sender {
    // 配置按钮状态
    [self setupButtonsState:_highLowPassButtons forCurrentButton:sender];
    
    // 记录选择的是高通还是低通
    switch (sender.tag) {
        case PASS_TYPE_HIGH:
            // 保存数据
            _channelInUse.passType = PASS_TYPE_HIGH;
            // 更新slider值
            _hightLowPassSlider.value = _channelInUse.highPassfrequencyValue;
            // 更新allpass按钮状态
            _allPassButton.selected = _channelInUse.isAllPassForHighPass ? YES : NO;
            break;
        case PASS_TYPE_LOW:
            _channelInUse.passType = PASS_TYPE_LOW;
            _hightLowPassSlider.value = _channelInUse.lowPassfrequencyValue;
            // 更新allpass按钮状态
            _allPassButton.selected = _channelInUse.isAllPassForLowhPass ? YES : NO;
            break;
        default:
            break;
    }
}

- (IBAction)allPassAction:(UIButton *)sender {
    sender.selected = !sender.selected;
    if (sender.selected) {
        // 选择状态:发送allpass指令
        switch (_channelInUse.passType) {
            case PASS_TYPE_HIGH:
                // 点击的是high pass的allpass按钮
                // 199为allpass
#pragma mark 发送0x16高通控制指令(all pass)
                [[HNIOMixManager shareManager].device setupHighPassForChannel:[HNIOMixManager shareManager].channelInUse.channelIndex filterSubtypeMode:_currentPassSubType frequencyValue:199 immediately:YES];
                // 保存数据
                _channelInUse.isAllPassForHighPass = YES;
                break;
            case PASS_TYPE_LOW:
#pragma mark 发送0x17低通控制指令(all pass)
                [[HNIOMixManager shareManager].device setupLowPassForChannel:[HNIOMixManager shareManager].channelInUse.channelIndex filterSubtypeMode:_currentPassSubType frequencyValue:199 immediately:YES];
                // 保存数据
                _channelInUse.isAllPassForLowhPass = YES;
                break;
            default:
                break;
        }
    } else {
        // 非选择状态，发送回slider当前值
        // TODO:此种状态下发送，还会受200ms影响，导致有时候没有发送(考虑另外写接口方法给两个allPass按钮)
        switch (_channelInUse.passType) {
            case PASS_TYPE_HIGH:
                [[HNIOMixManager shareManager].device setupHighPassForChannel:[HNIOMixManager shareManager].channelInUse.channelIndex filterSubtypeMode:_currentPassSubType frequencyValue:_channelInUse.highPassfrequencyValue immediately:YES];
                // 保存数据
                _channelInUse.isAllPassForHighPass = NO;
                break;
            case PASS_TYPE_LOW:
                [[HNIOMixManager shareManager].device setupLowPassForChannel:[HNIOMixManager shareManager].channelInUse.channelIndex filterSubtypeMode:_currentPassSubType frequencyValue:_channelInUse.lowPassfrequencyValue immediately:YES];
                // 保存数据
                _channelInUse.isAllPassForLowhPass = NO;
                break;
            default:
                break;
        }
    }
}

#pragma mark UIPickerViewDataSource
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return _filterSubTypes.count;
}

#pragma mark UIPickerViewDelegate
- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view {
    UILabel* tView = (UILabel*)view;
    if (!tView)
    {
        tView = [[UILabel alloc] init];
        [tView setFont:[UIFont boldSystemFontOfSize:15]];
        tView.textColor = [UIColor whiteColor];
        tView.textAlignment = NSTextAlignmentCenter;
        tView.text=_filterSubTypes[row];
    }
    return tView;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    if (pickerView.tag == PICKER_VIEW_HIGH_PASS) {
        _channelInUse.highPassSubType = row;
    } else {
        _channelInUse.lowPassSubType = row;
    }
}

#pragma mark Slider滑动事件
- (IBAction)highLowPassSliderValueChanged:(ASValueTrackingSlider *)sender {
    _highLowPassValueLabel.text = [@((NSInteger)sender.value) stringValue];
    switch (_channelInUse.passType) {
        case PASS_TYPE_HIGH:
#pragma mark 发送0x16高通控制指令
            // TODO:_currentPassSubType还要写pickerView去供使用者选择。
            [[HNIOMixManager shareManager].device setupHighPassForChannel:[HNIOMixManager shareManager].channelInUse.channelIndex filterSubtypeMode:_channelInUse.highPassSubType frequencyValue:sender.value immediately:NO];
            // 保存数据
            _channelInUse.highPassfrequencyValue = (NSInteger)sender.value;
            break;
        case PASS_TYPE_LOW:
#pragma mark 发送0x17低通控制指令
            [[HNIOMixManager shareManager].device setupLowPassForChannel:[HNIOMixManager shareManager].channelInUse.channelIndex filterSubtypeMode:_channelInUse.lowPassSubType frequencyValue:sender.value immediately:NO];
            // 保存数据
            _channelInUse.lowPassfrequencyValue = (NSInteger)sender.value;
            break;
        default:
            if (!_nonSeletPassTypeAlertView) {
                _nonSeletPassTypeAlertView = [[UIAlertView alloc] initWithTitle:@"NOTE" message:@"Please select High Pass or Low Pass" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            }
            [_nonSeletPassTypeAlertView show];
            break;
    }
}
@end
