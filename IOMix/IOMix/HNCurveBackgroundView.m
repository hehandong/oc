//
//  HNCurveBackgroundView.m
//  IOMix
//
//  Created by YuHeng_Antony on 9/8/15.
//  Copyright (c) 2015 Homni Enterprises Co., Ltd. All rights reserved.
//

#import "HNCurveBackgroundView.h"
#import "HNPublic.h"

@implementation HNCurveBackgroundView

// 绘制背景
- (void)drawRect:(CGRect)rect {
    // 线条颜色
    [[UIColor colorWithRed:0.19 green:0.19 blue:0.19 alpha:1.0] set];
    
    // 绘制外框
    UIBezierPath *outLine = [UIBezierPath bezierPath];
    outLine.lineWidth = HN_LINE_WIDTH;
    [outLine moveToPoint:CGPointMake(HN_BG_LEADING_VALUE, HN_BG_TOP_VALUE)];// 起点
    [outLine addLineToPoint:CGPointMake(HN_BG_TRAILING_VALUE, HN_BG_TOP_VALUE)];
    [outLine addLineToPoint:CGPointMake(HN_BG_TRAILING_VALUE, HN_BG_BOTTOM_VALUE)];
    [outLine addLineToPoint:CGPointMake(HN_BG_LEADING_VALUE, HN_BG_BOTTOM_VALUE)];
    [outLine closePath];
    [outLine stroke];
    
    // 绘制水平横线
    UIBezierPath *horizontalLine = [UIBezierPath bezierPath];
    horizontalLine.lineWidth = HN_LINE_WIDTH;
    for (int i = 1; i < 8; i++) {
        [horizontalLine moveToPoint:CGPointMake(HN_BG_LEADING_VALUE, HN_BG_TOP_VALUE + HN_HORIZONTAL_SPACE * i)];
        [horizontalLine addLineToPoint:CGPointMake(HN_BG_TRAILING_VALUE, HN_BG_TOP_VALUE + HN_HORIZONTAL_SPACE * i)];
    }
    [horizontalLine stroke];

    // 绘制竖线(3部分相同的竖线，用一个for循环)
    UIBezierPath *verticalLine = [UIBezierPath bezierPath];
    verticalLine.lineWidth = HN_LINE_WIDTH;
    for (int i = 0; i < 3; i++) {
        [verticalLine moveToPoint:CGPointMake(HN_LINE1_X + HN_V_GROUP_SPACE_VIEW * i, HN_BG_TOP_VALUE)];
        [verticalLine addLineToPoint:CGPointMake(HN_BG_LEADING_VALUE + HN_V_GROUP_SPACE_VIEW * i, HN_BG_BOTTOM_VALUE)];
        
        [verticalLine moveToPoint:CGPointMake(HN_LINE2_X + HN_V_GROUP_SPACE_VIEW * i, HN_BG_TOP_VALUE)];
        [verticalLine addLineToPoint:CGPointMake(HN_LINE2_X + HN_V_GROUP_SPACE_VIEW * i, HN_BG_BOTTOM_VALUE)];
        
        [verticalLine moveToPoint:CGPointMake(HN_LINE3_X + HN_V_GROUP_SPACE_VIEW * i, HN_BG_TOP_VALUE)];
        [verticalLine addLineToPoint:CGPointMake(HN_LINE3_X + HN_V_GROUP_SPACE_VIEW * i, HN_BG_BOTTOM_VALUE)];
        
        [verticalLine moveToPoint:CGPointMake(HN_LINE4_X + HN_V_GROUP_SPACE_VIEW * i, HN_BG_TOP_VALUE)];
        [verticalLine addLineToPoint:CGPointMake(HN_LINE4_X + HN_V_GROUP_SPACE_VIEW * i, HN_BG_BOTTOM_VALUE)];
        
        [verticalLine moveToPoint:CGPointMake(HN_LINE5_X + HN_V_GROUP_SPACE_VIEW * i, HN_BG_TOP_VALUE)];
        [verticalLine addLineToPoint:CGPointMake(HN_LINE5_X + HN_V_GROUP_SPACE_VIEW * i, HN_BG_BOTTOM_VALUE)];
        
        [verticalLine moveToPoint:CGPointMake(HN_LINE6_X + HN_V_GROUP_SPACE_VIEW * i, HN_BG_TOP_VALUE)];
        [verticalLine addLineToPoint:CGPointMake(HN_LINE6_X + HN_V_GROUP_SPACE_VIEW * i, HN_BG_BOTTOM_VALUE)];
        
        [verticalLine moveToPoint:CGPointMake(HN_LINE7_X + HN_V_GROUP_SPACE_VIEW * i, HN_BG_TOP_VALUE)];
        [verticalLine addLineToPoint:CGPointMake(HN_LINE7_X + HN_V_GROUP_SPACE_VIEW * i, HN_BG_BOTTOM_VALUE)];
        
        [verticalLine moveToPoint:CGPointMake(HN_LINE8_X + HN_V_GROUP_SPACE_VIEW * i, HN_BG_TOP_VALUE)];
        [verticalLine addLineToPoint:CGPointMake(HN_LINE8_X + HN_V_GROUP_SPACE_VIEW * i, HN_BG_BOTTOM_VALUE)];
        
        [verticalLine moveToPoint:CGPointMake(HN_LINE9_X + HN_V_GROUP_SPACE_VIEW * i, HN_BG_TOP_VALUE)];
        [verticalLine addLineToPoint:CGPointMake(HN_LINE9_X + HN_V_GROUP_SPACE_VIEW * i, HN_BG_BOTTOM_VALUE)];
    }
    
    // 最后一条单独的线
    [verticalLine moveToPoint:CGPointMake(HN_RIGHT_LINE_POINT_X_SCALE * self.bounds.size.width, HN_BG_TOP_VALUE)];
    [verticalLine addLineToPoint:CGPointMake(HN_RIGHT_LINE_POINT_X_SCALE * self.bounds.size.width, HN_BG_BOTTOM_VALUE)];
    [verticalLine stroke];
    
    // 绘制刻度
    // 左边刻度
    int j = 12;
    for (int i = 0; i < 9; i++) {
        [self drawText:[NSString stringWithFormat:@"%d", j] at:CGPointMake(HN_BG_LEADING_VALUE / 2, HN_BG_TOP_VALUE + HN_HORIZONTAL_SPACE * i)];
        j -= 3;
    }
    
    // 顶部刻度
    [self drawText:HN_SCALE_20 at:CGPointMake(HN_BG_LEADING_VALUE + (HN_BG_LEADING_VALUE / 4), HN_BG_TOP_VALUE / 2)];
    [self drawText:HN_SCALE_50 at:CGPointMake(HN_LINE4_X + HN_V_GROUP_SPACE_VIEW * 0, HN_BG_TOP_VALUE / 2)];
    [self drawText:HN_SCALE_100 at:CGPointMake(HN_LINE9_X + HN_V_GROUP_SPACE_VIEW * 0, HN_BG_TOP_VALUE / 2)];
    [self drawText:HN_SCALE_200 at:CGPointMake(HN_LINE1_X + HN_V_GROUP_SPACE_VIEW * 1, HN_BG_TOP_VALUE / 2)];
    [self drawText:HN_SCALE_500 at:CGPointMake(HN_LINE4_X + HN_V_GROUP_SPACE_VIEW * 1, HN_BG_TOP_VALUE / 2)];
    [self drawText:HN_SCALE_1K at:CGPointMake(HN_LINE9_X + HN_V_GROUP_SPACE_VIEW * 1, HN_BG_TOP_VALUE / 2)];
    [self drawText:HN_SCALE_2K at:CGPointMake(HN_LINE1_X + HN_V_GROUP_SPACE_VIEW * 2, HN_BG_TOP_VALUE / 2)];
    [self drawText:HN_SCALE_5K at:CGPointMake(HN_LINE4_X + HN_V_GROUP_SPACE_VIEW * 2, HN_BG_TOP_VALUE / 2)];
    [self drawText:HN_SCALE_10K at:CGPointMake(HN_LINE9_X + HN_V_GROUP_SPACE_VIEW * 2, HN_BG_TOP_VALUE / 2)];
    [self drawText:HN_SCALE_20K at:CGPointMake(HN_RIGHT_LINE_POINT_X_SCALE * self.bounds.size.width, HN_BG_TOP_VALUE / 2)];
    
    // 底部刻度
    [self drawText:HN_SCALE_20 at:CGPointMake(HN_BG_LEADING_VALUE + (HN_BG_LEADING_VALUE / 4), (HN_BG_BOTTOM_VALUE + HN_BG_TOP_VALUE / 2))];
    [self drawText:HN_SCALE_db at:CGPointMake(HN_LINE2_X + HN_V_GROUP_SPACE_VIEW * 0, (HN_BG_BOTTOM_VALUE + HN_BG_TOP_VALUE / 2))];
    [self drawText:HN_SCALE_50 at:CGPointMake(HN_LINE4_X + HN_V_GROUP_SPACE_VIEW * 0, (HN_BG_BOTTOM_VALUE + HN_BG_TOP_VALUE / 2))];
    [self drawText:HN_SCALE_100 at:CGPointMake(HN_LINE9_X + HN_V_GROUP_SPACE_VIEW * 0, (HN_BG_BOTTOM_VALUE + HN_BG_TOP_VALUE / 2))];
    [self drawText:HN_SCALE_200 at:CGPointMake(HN_LINE1_X + HN_V_GROUP_SPACE_VIEW * 1, (HN_BG_BOTTOM_VALUE + HN_BG_TOP_VALUE / 2))];
    [self drawText:HN_SCALE_500 at:CGPointMake(HN_LINE4_X + HN_V_GROUP_SPACE_VIEW * 1, (HN_BG_BOTTOM_VALUE + HN_BG_TOP_VALUE / 2))];
    [self drawText:HN_SCALE_1K at:CGPointMake(HN_LINE9_X + HN_V_GROUP_SPACE_VIEW * 1, (HN_BG_BOTTOM_VALUE + HN_BG_TOP_VALUE / 2))];
    [self drawText:HN_SCALE_2K at:CGPointMake(HN_LINE1_X + HN_V_GROUP_SPACE_VIEW * 2, (HN_BG_BOTTOM_VALUE + HN_BG_TOP_VALUE / 2))];
    [self drawText:HN_SCALE_5K at:CGPointMake(HN_LINE4_X + HN_V_GROUP_SPACE_VIEW * 2, (HN_BG_BOTTOM_VALUE + HN_BG_TOP_VALUE / 2))];
    [self drawText:HN_SCALE_10K at:CGPointMake(HN_LINE9_X + HN_V_GROUP_SPACE_VIEW * 2, (HN_BG_BOTTOM_VALUE + HN_BG_TOP_VALUE / 2))];
    [self drawText:HN_SCALE_20K at:CGPointMake(HN_RIGHT_LINE_POINT_X_SCALE * self.bounds.size.width, (HN_BG_BOTTOM_VALUE + HN_BG_TOP_VALUE / 2))];
}

- (void)drawText:(NSString *)text at:(CGPoint)location {
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 30, 16)];
    label.center = location;
    label.textAlignment = NSTextAlignmentCenter;
    label.text = text;
    if (IS_OS_8_OR_LATER) {
        label.font = [UIFont systemFontOfSize:13 weight:3];
    } else {
        label.font = [UIFont systemFontOfSize:13];
    }
    label.textColor = [UIColor colorWithRed:0.59 green:0.59 blue:0.59 alpha:1.0];
    [self addSubview:label];
}

@end
