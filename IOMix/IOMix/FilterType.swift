//
//  FilterType.swift
//  IOMix
//
//  Created by homni_rd01 on 15/11/20.
//  Copyright © 2015年 Homni Enterprises Co., Ltd. All rights reserved.
//

import Foundation

//MARK:高低音的两个按钮
enum PitchType:Int{
    case PITCH_TYPE_BASS = 0
    case PITCH_TYPE_TREBLE

}
//MARK:高低通中的两个按钮
enum PassType:Int{
   case PASS_TYPE_HIGH = 0
   case PASS_TYPE_LOW
}

//MARK:allPass两个按钮
enum AllPass:Int{
    case ALL_PASS_FOR_HIGH = 0
    case ALL_PASS_FOR_LOW
}

//几种过滤器的模式
enum Mode:Int {
    
    case BASS_SHELF = 1  //1
    case TREBLE_SHELF //2
    case HIGH_PASS    //3
    case LOW_PASS     //4
    
}

//高低通的子模式
enum SubMode : Int{
    
    case BUTTER_WORTH_1 = 0  //0
    case BUTTER_WORTH_2    //1
    case BESSEL_2          //2
    case LINKWITZ_RILEY_2  //3
    case VARIABLE_Q2       //4
    case CHEBYCHEV         //5
    
}

/**
 滤波的类型和高低通的子类型全部整合在一起
 */
enum FilterType {
    
    case BASS_SHELF                   //0
    case TREBLE_SHELF                 //1
    
    case HIGH_PASS_BUTTER_WORTH_1     //2
    case HIGH_PASS_BUTTER_WORTH_2     //3
    case HIGH_PASS_BESSEL_2           //4
    case HIGH_PASS_LINKWITZ_RILEY_2   //5
    case HIGH_PASS_VARIABLE_Q2        //6
    case HIGH_PASS_CHEBYCHEV          //7
    
    case LOW_PASS_BUTTER_WORTH_1      //8
    case LOW_PASS_BUTTER_WORTH_2      //9
    case LOW_PASS_BESSEL_2            //10
    case LOW_PASS_LINKWITZ_RILEY_2    //11
    case LOW_PASS_VARIABLE_Q2         //12
    case LOW_PASS_CHEBYCHEV           //13
    
    case NONE                         //14
}