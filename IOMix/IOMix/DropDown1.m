//
//  DropDown1.m
//  DropDown
//
//  Created by clyd on 13-9-24.
//  Copyright (c) 2013年 clyd. All rights reserved.
//

#import "DropDown1.h"

@implementation DropDown1

//- (id)initWithFrame:(CGRect)frame
//{
//    if (frame.size.height < 200) {
//        frameHeight = 150;
//    }else
//    {
//        frameHeight = frame.size.height;
//    }
//    tabheight = frameHeight - 30;
//    frame.size.height = 30.0f;
//    self = [super initWithFrame:frame];
//    if (self) {
//        showList = NO;//默认不显示下拉框
//        
//        tv = [[UITableView alloc]initWithFrame:CGRectMake(0, 30, frame.size.width, 0)];
//        tv.delegate = self;
//        tv.dataSource = self;
//        tv.backgroundColor = [UIColor grayColor];
//        tv.separatorColor = [UIColor lightGrayColor];
//        tv.hidden = YES;
//        [self addSubview:tv];
//        
//        textField = [[UITextField alloc]initWithFrame:CGRectMake(0, 0, frame.size.width, 35)];
//        textField.backgroundColor = [UIColor colorWithRed:0.23 green:0.22 blue:0.22 alpha:1.0];
////      textField.background = [UIImage imageNamed:@"button"];
//        textField.textColor = [UIColor colorWithRed:0.68 green:0.68 blue:0.68 alpha:1.0];
//        
//        textField.borderStyle = UITextBorderStyleRoundedRect;//设置文本框的边框风格;
//        textField.textAlignment = NSTextAlignmentCenter;
//        textField.font = [UIFont systemFontOfSize:13];
//        textField.delegate = self;
//        [textField addTarget:self action:@selector(dropdown:) forControlEvents:UIControlEventAllEvents];
//        [self addSubview:textField];
//    }
//    return self;
//}

- (id)initWithFrame:(CGRect)frame tag:(NSInteger)tag {
    if (frame.size.height < 200) {
        _frameHeight = 150;
    }else
    {
        _frameHeight = frame.size.height;
    }
    _tabheight = _frameHeight - 30;
    frame.size.height = 30.0f;
    self = [super initWithFrame:frame];
    if (self) {
        _showList = NO;//默认不显示下拉框
        
        _tv = [[UITableView alloc]initWithFrame:CGRectMake(0, 30, frame.size.width, 0)];
        _tv.delegate = self;
        _tv.dataSource = self;
        _tv.backgroundColor = [UIColor grayColor];
        _tv.separatorColor = [UIColor lightGrayColor];
        _tv.hidden = YES;
        [self addSubview:_tv];
        
        _textField = [[UITextField alloc]initWithFrame:CGRectMake(0, 0, frame.size.width, 35)];
        _textField.tag = tag;
//        _textField.backgroundColor = [UIColor colorWithRed:0.23 green:0.22 blue:0.22 alpha:1.0];
        _textField.background = [UIImage imageNamed:@"button"];
        _textField.textColor = [UIColor colorWithRed:0.68 green:0.68 blue:0.68 alpha:1.0];
        
//        _textField.borderStyle = UITextBorderStyleRoundedRect;//设置文本框的边框风格;
        _textField.textAlignment = NSTextAlignmentCenter;
        _textField.font = [UIFont systemFontOfSize:13];
        _textField.delegate = self;
        [_textField addTarget:self action:@selector(dropdown:) forControlEvents:UIControlEventAllEvents];
        [self addSubview:_textField];
    }
    return self;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {

    return NO;
}

- (void)dropdown:(UITextField *)sender
{
    [_textField resignFirstResponder];
    
    if (_showList) {//如果下拉框已经显示，什么都不做
       
        _showList = NO;
        _tv.hidden = YES;
        
        CGRect sf = self.frame;
        sf.size.height = 30;
        self.frame = sf;
        CGRect frame = _tv.frame;
        frame.size.height = 0;
        _tv.frame = frame;
        
    }else//如果下拉框尚未显示，则进行显示
    {
        CGRect sf = self.frame;
        sf.size.height = _frameHeight;
        
        //把dropdownList放到前面，防止下拉框被别的空间遮住
        [self.superview bringSubviewToFront:self];
        _tv.hidden = NO;
        _showList = YES;//显示下拉框
        
        CGRect frame = _tv.frame;
        frame.size.height = 0;
        _tv.frame = frame;
        frame.size.height = _tabheight;
        [UIView beginAnimations:@"ResizeForKeyBoard" context:nil];
        [UIView setAnimationCurve:UIViewAnimationCurveLinear];
        self.frame = sf;
        _tv.frame = frame;
        [UIView commitAnimations];
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"show" object:nil userInfo:@{@"textFildTag":[NSNumber numberWithInteger:_textField.tag]}];
    
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_tableArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString* CellIdentifier = @"Cell";
    
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    cell.textLabel.text = [_tableArray objectAtIndex:[indexPath row]];
    cell.textLabel.font = [UIFont systemFontOfSize:12.0f];
    cell.textLabel.textAlignment = NSTextAlignmentCenter;
    cell.backgroundColor = [UIColor colorWithRed:0.23 green:0.22 blue:0.22 alpha:1.0];

    cell.accessoryType = UITableViewCellAccessoryNone;
    cell.selectionStyle = UITableViewCellSelectionStyleGray;
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 35;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    _textField.text = [_tableArray objectAtIndex:[indexPath row]];
    _showList = NO;
    _tv.hidden = YES;
    
    CGRect sf = self.frame;
    sf.size.height = 30;
    self.frame = sf;
    CGRect frame = _tv.frame;
    frame.size.height = 0;
    _tv.frame = frame;
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"test" object:nil userInfo:@{@"index":[NSNumber numberWithInteger:indexPath.row], @"textFildTag":[NSNumber numberWithInteger:_textField.tag]}];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}




@end
