//
//  HNEqualizer.m
//  IOMix
//
//  Created by YuHeng_Antony on 15/7/10.
//  Copyright (c) 2015年 Homni Enterprises Co., Ltd. All rights reserved.
//

#import "HNEqualizer.h"

@implementation HNEqualizer
- (instancetype)init
{
    self = [super init];
    if (self) {
        _Hz20  = 12;
        _Hz50  = 12;
        _Hz100 = 12;
        _Hz200 = 12;
        _Hz500 = 12;
        _K1    = 12;
        _K2    = 12;
        _K5    = 12;
        _K10   = 12;
        _K20   = 12;
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder{
    self = [super init];
    if (self) {
        _Hz20 =  [aDecoder decodeFloatForKey:@"Hz20"];
        _Hz50 =  [aDecoder decodeFloatForKey:@"Hz50"];
        _Hz100 = [aDecoder decodeFloatForKey:@"Hz100"];
        _Hz200 = [aDecoder decodeFloatForKey:@"Hz200"];
        _Hz500 = [aDecoder decodeFloatForKey:@"Hz500"];
        _K1 =    [aDecoder decodeFloatForKey:@"K1"];
        _K2 =    [aDecoder decodeFloatForKey:@"K2"];
        _K5 =    [aDecoder decodeFloatForKey:@"K5"];
        _K10 =   [aDecoder decodeFloatForKey:@"K10"];
        _K20 =   [aDecoder decodeFloatForKey:@"K20"];
    }
    return self;
}

-(void)encodeWithCoder:(NSCoder *)aCoder{
    [aCoder encodeFloat:_Hz20 forKey:@"Hz20"];
    [aCoder encodeFloat:_Hz50 forKey:@"Hz50"];
    [aCoder encodeFloat:_Hz100 forKey:@"Hz100"];
    [aCoder encodeFloat:_Hz200 forKey:@"Hz200"];
    [aCoder encodeFloat:_Hz500 forKey:@"Hz500"];
    [aCoder encodeFloat:_K1 forKey:@"K1"];
    [aCoder encodeFloat:_K2 forKey:@"K2"];
    [aCoder encodeFloat:_K5 forKey:@"K5"];
    [aCoder encodeFloat:_K10 forKey:@"K10"];
    [aCoder encodeFloat:_K20 forKey:@"K20"];
}
@end
