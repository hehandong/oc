//
//  ViewController.m
//  IOMix
//
//  Created by Chiwen on 2015/6/15.
//  Copyright (c) 2015年 Homni Enterprises Co., Ltd. All rights reserved.
//

#import "HNHomeViewController.h"
#import "HNChannelStore.h"
#import "HNChannel.h"
#import "ASValueTrackingSlider.h"
#import "LARSBar.h"
#import "HNPublic.h"
#import "HNIOMixManager.h"
#import "HNIOMixDevice.h"

@interface HNHomeViewController ()<UITextFieldDelegate>
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *SYNCButtons;
/***  音量控制slider*/
@property (strong, nonatomic) IBOutletCollection(ASValueTrackingSlider) NSArray *volumControlSliders;
/***  音量灯(slider)*/
@property (strong, nonatomic) IBOutletCollection(LARSBar) NSArray *volumColourfulSlider;// 彩色音量的slider
@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *dBLabels;
@property (strong, nonatomic) IBOutletCollection(UITextField) NSArray *EQNameTextFileds;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *muteButtons;
@property (copy, nonatomic) NSArray *allChannels;

@end

@implementation HNHomeViewController

-(BOOL)prefersStatusBarHidden{
    return YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    _allChannels = [HNIOMixManager shareManager].allChannel;
    
    // 根据数据，设置各个按钮状态、曲线图名称、dBLable音量值
    [self setupUIState];
    
    // 自定义slider的布局及赋初始值
    [self layoutSliders];

    // 观察音量值变化通告，dbLabel、两个slider的值都要改变
    [self observevolumeDidChangedNotification];
    
    // 观察sync被点击后的通告,同步两边的音量值
    [self observeSyncDidClickedNotification];

}

#pragma mark - setupStates
- (void)setupUIState {
    // 设置sync按钮状态
    for (int i = 0; i < CHANNEL_COUNT; i++) {
        [_SYNCButtons[i] setSelected:[_allChannels[i] isSYNC]];
    }
    
    // 设置曲线图名称
    for (int i = 0; i < CHANNEL_COUNT - 2; i++) {
        [_EQNameTextFileds[i] setText:[_allChannels[i] customEQName]];
    }
    
    // 设置静音按钮状态
    for (int i = 0; i < CHANNEL_COUNT; i++) {
        [_muteButtons[i] setSelected:[_allChannels[i] isMUTE]];
    }
    
    // 设置音量label值
    for (int i = 0; i < CHANNEL_COUNT; i++) {
        [_dBLabels[i] setText:[NSString stringWithFormat:@"%@dB", [@([_allChannels[i] volume]) stringValue]]];
    }
}

#pragma mark - 观察音量变化通告
- (void)observevolumeDidChangedNotification {
        [[NSNotificationCenter defaultCenter] addObserverForName:kNotificationVolumeDidChanged object:nil queue:nil usingBlock:^(NSNotification *note) {
            
            // dBLabel观察音量值变更通告
            UILabel *dBLabel = _dBLabels[[note.userInfo[sliderTag] integerValue]];
            dBLabel.text = [NSString stringWithFormat:@"%.0fdB", [note.userInfo[sliderValue] floatValue]];
            
            // 彩色音量slider观察音量值变更通告
            LARSBar *colourfulSlider = _volumColourfulSlider[[note.userInfo[sliderTag] integerValue]];
            colourfulSlider.value = [note.userInfo[sliderValue] floatValue];
        }];
}

#pragma mark - 观察sync按钮被点击通告
- (void)observeSyncDidClickedNotification {
    [[NSNotificationCenter defaultCenter] addObserverForName:kNotificationSyncDidClicked object:nil queue:nil usingBlock:^(NSNotification *note) {
        NSInteger channelTag = [note.userInfo[syncButtonTag] integerValue];
        
        // 點擊了奇數(tag)位置的「SYNC」
        // 1、前面的slider(tag值-1)的值要同步为为点击sync下的slider的值
        // 2、彩色音量slider的值也要同步
        // 3、dbLabel的值也要同步
        
        if ([note.userInfo[syncButtonTag] integerValue] & 1) {
            [self sync:channelTag and:channelTag - 1];
        }
        // 點擊了偶數(tag)位置的「SYNC」
        else {
            [self sync:channelTag and:channelTag + 1];
        }
    }];
    
}

// otherChannelTag是需要改变值的两个slider、一个label的tag值
- (void)sync:(NSInteger)channelTag and:(NSInteger)otherChannelTag {
    ASValueTrackingSlider *previousSlider = _volumControlSliders[otherChannelTag];
    previousSlider.value = [_allChannels[channelTag] volume];
    
    LARSBar *previousColourfulSlider = _volumColourfulSlider[otherChannelTag];
    previousColourfulSlider.value = [_allChannels[channelTag] volume];;
    
    [_dBLabels[otherChannelTag] setText:[NSString stringWithFormat:@"%@dB", [@([_allChannels[channelTag] volume]) stringValue]]];

}

#pragma mark - layoutSliders
- (void)layoutSliders {
    
    // 初始化彩色音量slider
    for (int i = 0; i < 10; i++) {
        LARSBar *colourfulSlider = _volumColourfulSlider[i];
        [colourfulSlider removeConstraints:colourfulSlider.constraints];
        colourfulSlider.userInteractionEnabled = NO;
        [colourfulSlider setThumbImage:[UIImage new] forState:UIControlStateNormal];
        colourfulSlider.transform = CGAffineTransformMakeRotation(M_PI * 1.5);
        colourfulSlider.maximumValue = 50.0;
        colourfulSlider.value = [_allChannels[i] volume];
        
        colourfulSlider.translatesAutoresizingMaskIntoConstraints = NO;
        NSArray *volumeSliderVerticalConstraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[colourfulSlider]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(colourfulSlider)];
        // 注意：要設置翻轉90度slider的「高度」，其實就是設置未翻轉前slider的「寬度」
        NSLayoutConstraint *volumeSliderHeightConstraint = [NSLayoutConstraint constraintWithItem:colourfulSlider attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:colourfulSlider.superview attribute:NSLayoutAttributeHeight multiplier:1 constant:0.0];
        NSLayoutConstraint *volumeSliderCenterXConstraint = [NSLayoutConstraint constraintWithItem:colourfulSlider attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:colourfulSlider.superview attribute:NSLayoutAttributeCenterX multiplier:1 constant:0.0];
        [colourfulSlider.superview addConstraints:volumeSliderVerticalConstraints];
        [colourfulSlider.superview addConstraint:volumeSliderHeightConstraint];
        [colourfulSlider.superview addConstraint:volumeSliderCenterXConstraint];
    }
    
    // 初始化音量控制slider
    for (int i = 0; i < 10; i++) {
        ASValueTrackingSlider *slider = _volumControlSliders[i];
        [slider removeConstraints:slider.constraints];
        slider.transform = CGAffineTransformMakeRotation(M_PI * 1.5);
        slider.popUpViewColor = [UIColor clearColor];// 首页隐藏popView
        slider.textColor = [UIColor clearColor];
        [slider setThumbImage:[UIImage imageNamed:@"sliderButton"] forState:UIControlStateNormal];
        [slider setMaximumTrackImage:[UIImage new] forState:UIControlStateNormal];
        [slider setMinimumTrackImage:[UIImage new] forState:UIControlStateNormal];
        [slider addTarget:self action:@selector(sliderValueDidChanged:) forControlEvents:UIControlEventValueChanged];
        slider.maximumValue = 50.0;
        slider.value = [_allChannels[i] volume];
        slider.isSync = [_allChannels[i] isSYNC];// 同步数据库的isSYNC数据。
        // 手开始点击slider时的工作
        [slider addTarget:self action:@selector(sliderTouchDownEvent:) forControlEvents:UIControlEventTouchDown];
        // 手指离开slider时的动作
        [slider addTarget:self action:@selector(sliderTouchUpOutsideEvent:) forControlEvents:UIControlEventTouchUpOutside];
        [slider addTarget:self action:@selector(sliderTouchUpInsideEvent:) forControlEvents:UIControlEventTouchUpInside];
        
        slider.translatesAutoresizingMaskIntoConstraints = NO;
        NSArray *sliderVerticalConstraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[slider]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(slider)];
        // 注意：要設置翻轉90度slider的「高度」，其實就是設置未翻轉前slider的「寬度」
        NSLayoutConstraint *sliderHeightConstraint = [NSLayoutConstraint constraintWithItem:slider attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:slider.superview attribute:NSLayoutAttributeHeight multiplier:1 constant:0.0];
        NSLayoutConstraint *sliderCenterXConstraint = [NSLayoutConstraint constraintWithItem:slider attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:slider.superview attribute:NSLayoutAttributeCenterX multiplier:1 constant:0.0];
        [slider.superview addConstraints:sliderVerticalConstraints];
        [slider.superview addConstraint:sliderHeightConstraint];
        [slider.superview addConstraint:sliderCenterXConstraint];
    }
}

#pragma mark - sliderValueChange
- (void)sliderValueDidChanged:(ASValueTrackingSlider *)sender {
#pragma mark 发送0x11音量控制指令
    [self sendCommandFor:sender immediately:NO];
    
    // 更新数据
    [_allChannels[sender.tag] setVolume:(NSInteger)sender.value];
    
    // 發送通告，告知彩色音量Slider及dBLabel的值要同步顯示(通过NSDictionary传值)
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationVolumeDidChanged object:nil userInfo:@{sliderValue:[NSNumber numberWithFloat:sender.value], sliderTag:[NSNumber numberWithInteger:sender.tag]}];
    
    // 判斷滑動的slider是否有「SYNC」,有的话另外一个channel的value要同步(并保存数据)
    if ([_allChannels[sender.tag] isSYNC]) {
        if (sender.tag & 1) {
            [self sync:sender.tag and:sender.tag - 1];
            [_allChannels[sender.tag - 1] setVolume:(NSInteger)sender.value];
        } else {
            [self sync:sender.tag and:sender.tag + 1];
            [_allChannels[sender.tag + 1] setVolume:(NSInteger)sender.value];
        }
    }
    
    // 检查是否有MUTE按钮高亮，有的话就取消高亮
    if ([_allChannels[sender.tag] isMUTE]) {
        [_muteButtons[sender.tag] setSelected:NO];
        if ([_allChannels[sender.tag] isSYNC]) {
            if (sender.tag & 1) {
                [_muteButtons[sender.tag - 1] setSelected:NO];
                [_allChannels[sender.tag - 1] setIsMUTE:NO];
            } else {
                [_muteButtons[sender.tag + 1] setSelected:NO];
                [_allChannels[sender.tag + 1] setIsMUTE:NO];
            }
        }
        [_allChannels[sender.tag] setIsMUTE:NO];
    }
}

#pragma mark sliderValueChange Helper Method
- (void)sliderTouchDownEvent:(ASValueTrackingSlider *)sender {
#pragma mark 发送0x11音量控制指令
    // 即刻发送(用于手指开始点击slider发送)
    [self sendCommandFor:sender immediately:YES];
}

- (void)sliderTouchUpOutsideEvent:(ASValueTrackingSlider *)sender {
#pragma mark 发送0x11音量控制指令
    // 即刻发送(用于手指离开slider后最后一次发送)
    [self sendCommandFor:sender immediately:YES];

}

- (void)sliderTouchUpInsideEvent:(ASValueTrackingSlider *)sender {
    // 即刻发送(用于手指离开slider后最后一次发送)
#pragma mark 发送0x11音量控制指令
    [self sendCommandFor:sender immediately:YES];
}

- (void)sendCommandFor:(ASValueTrackingSlider *)sender immediately:(BOOL)immediately {
    UIButton *syncButton =  _SYNCButtons[sender.tag];
    if (syncButton.selected) {
        // 如果点击了sync按钮，需要发送"sync指令"
        if (sender.tag & 1) {
            // 滑动奇数tag值的slider
            [[HNIOMixManager shareManager].device setupSyncVolumeForChannelA:sender.tag channelB:sender.tag - 1 volumeValue:sender.value immediately:immediately];
        } else {
            [[HNIOMixManager shareManager].device setupSyncVolumeForChannelA:sender.tag channelB:sender.tag + 1 volumeValue:sender.value immediately:immediately];
        }
    } else {
        [[HNIOMixManager shareManager].device setupVolumeForChannel:sender.tag volumeValue:sender.value immediately:immediately];
    }
}

#pragma mark - syncAction
- (IBAction)syncAction:(UIButton *)sender {
    sender.selected = !sender.selected;
    
    // 更新数据
    [_allChannels[sender.tag] setIsSYNC:sender.selected];
    
    // 更新另一个syncButton的状态(并保存数据)
    if (sender.tag & 1) {
        // 点击了奇数(tag)
        [_SYNCButtons[sender.tag - 1] setSelected:sender.selected];
        [_allChannels[sender.tag - 1] setIsSYNC:sender.selected];
    } else {
        // 点击了偶数(tag)
        [_SYNCButtons[sender.tag + 1] setSelected:sender.selected];
        [_allChannels[sender.tag + 1] setIsSYNC:sender.selected];
    }
    
    if (sender.selected) {
        // 綁定另外一个channel
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationSyncDidClicked object:nil userInfo:@{syncButtonTag:[NSNumber numberWithInteger:sender.tag]}];
    }
}

#pragma mark - muteAction
- (IBAction)muteToggle:(UIButton *)sender {
    sender.selected = !sender.selected;
    
    // 更新数据
    [_allChannels[sender.tag] setIsMUTE:sender.selected];
    
    // 点击奇数按钮，判断同tag值的sync按钮是否为选择状态
    // 选择状态：与tag值减1的按钮都高亮；同时发送两个channel的0x11指令
    // 非选择状态：单独发送该channel的0x11指令
    // MUTE关闭时，还需要发复原音量0x11指令
    
    // 点击偶数那妞，判断同tag值的sync按钮是否为选择状态
    // 选择状态：与tag值加1的按钮都高亮；同时发送两个channel的0x11指令
    // 非选择状态：单独发送该channel的0x11指令
    // MUTE关闭时，还需要发复原音量0x11指令
    
    if (sender.tag & 1) {
    // 点击奇数按钮
        UIButton *syncButton = _SYNCButtons[sender.tag];
        if (syncButton.selected) {
            
            // 保存数据
            [_allChannels[sender.tag - 1] setIsMUTE:sender.selected];
            
            // 同步另一个mute按钮状态
            [_muteButtons[sender.tag - 1] setSelected:sender.selected];
            
            // 同步状态(发2条指令)
            if (sender.selected) {
                // MUTE按钮打开
#pragma mark SYNC、MUTE打开,发送2个channel的0x11静音指令
                [[HNIOMixManager shareManager].device setupNonDelayVolumeForChannelA:sender.tag channelB:sender.tag - 1 volumeValue:0];
            } else {
                // MUTE按钮关闭
#pragma mark MUTE关闭,发送2个channel的0x11原音量指令
                ASValueTrackingSlider *slider = _volumControlSliders[sender.tag];
                [[HNIOMixManager shareManager].device setupNonDelayVolumeForChannelA:sender.tag channelB:sender.tag - 1 volumeValue:slider.value];
            }
        } else {
            // 非同步状态(发1条指令)
            if (sender.selected) {
                // MUTE按钮打开
#pragma mark MUTE打开,发送1个channel的0x11静音指令
                [[HNIOMixManager shareManager].device setupNonDelayVolumeForChannel:sender.tag volumeValue:0];
            } else {
                // MUTE按钮关闭
#pragma mark MUTE关闭,发送1个channel的0x11原音量指令
                ASValueTrackingSlider *slider = _volumControlSliders[sender.tag];
                [[HNIOMixManager shareManager].device setupNonDelayVolumeForChannel:sender.tag volumeValue:slider.value];
            }
        }
    } else {
    // 点击偶数按钮
        UIButton *syncButton = _SYNCButtons[sender.tag];
        if (syncButton.selected) {
            
            // 保存数据
            [_allChannels[sender.tag + 1] setIsMUTE:sender.selected];
            
            // 同步另一个mute按钮状态
            [_muteButtons[sender.tag + 1] setSelected:sender.selected];
            
            // 同步状态(发2条指令)
            if (sender.selected) {
                // MUTE按钮打开
#pragma mark SYNC、MUTE打开,发送2个channel的0x11静音指令
                [[HNIOMixManager shareManager].device setupNonDelayVolumeForChannelA:sender.tag channelB:sender.tag + 1 volumeValue:0];
            } else {
#pragma mark MUTE关闭,发送2个channel的0x11原音量指令
                ASValueTrackingSlider *slider = _volumControlSliders[sender.tag];
                [[HNIOMixManager shareManager].device setupNonDelayVolumeForChannelA:sender.tag channelB:sender.tag + 1 volumeValue:slider.value];
            }
        } else {
            // 非同步状态(发1条指令)
            if (sender.selected) {
#pragma mark MUTE打开,发送1个channel的0x11静音指令
                [[HNIOMixManager shareManager].device setupNonDelayVolumeForChannel:sender.tag volumeValue:0];
            } else {
                // MUTE按钮关闭
#pragma mark MUTE关闭,发送1个channel的0x11原音量指令
                ASValueTrackingSlider *slider = _volumControlSliders[sender.tag];
                [[HNIOMixManager shareManager].device setupNonDelayVolumeForChannel:sender.tag volumeValue:slider.value];
            }
        }
    }
    // TODO:数据本地保存
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    // 保存数据
    [_allChannels[textField.tag] setCustomEQName:textField.text];
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    // 保存数据
    [_allChannels[textField.tag] setCustomEQName:textField.text];
    return YES;
}

@end
