//
//  HNIOMixManager.m
//  IOMix
//
//  Created by YuHeng_Antony on 11/9/15.
//  Copyright © 2015 Homni Enterprises Co., Ltd. All rights reserved.
//

#import "HNIOMixManager.h"
#import "HNIOMixDevice.h"
#import "HNBaseCommand.h"
#import "HNPublic.h"
#import "HNChannelStore.h"
#import "HNChannel.h"
#import "HNEqualizer.h"

/**
 *  发送同步指令的间隔时间
 */
static int sleepTime = 200000;

@interface HNIOMixManager ()<HNIOMixDeviceDelegate>
/**
 *  链接成功，提示使用者
 */
@property (strong, nonatomic) UIAlertView *connectSuccessAlertView;

/**
 *  链接断开，提示使用者
 */
@property (strong, nonatomic) UIAlertView *disconnectAlertView;
@end

@implementation HNIOMixManager
+ (instancetype)shareManager {
    static id shareManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shareManager = [[[self class] alloc] init];
    });
    return shareManager;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        _store = [[HNChannelStore alloc] init];
        
        // 创建device对象的时候，同时就建立了链接
        _device = [HNIOMixDevice deviceWithIP:HNServerDefaultIPAddress];
        _device.delegate = self;
    }
    return self;
}

#pragma mark - 
- (NSArray *)allChannel {
    return _store.allChannels;
}

- (void)setChannelInUse:(HNChannel *)channelInUse {
    if (!(channelInUse == _channelInUse)) {
        _channelInUse = channelInUse;
    }
}

#pragma mark - HNIOMixDeviceDelegate
- (void)deviceDidConnect {
    NSLog(@"链接成功");
    _isConnected = YES;
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationSocketDidConnect object:nil];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        // 链接成功alerView
        if (!_connectSuccessAlertView) {
            _connectSuccessAlertView = [[UIAlertView alloc] initWithTitle:@"NOTE" message:@"Connect Success. Please continue." delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        }
        [[HNIOMixManager shareManager].connectSuccessAlertView show];
    });
    
    // 链接成功，同步数据到硬件
    [self syncDevice];
}

- (void)deviceDidDisconnect {
    // 链接断开后，发出通告，弹出alerView提示使用者
    _isConnected = NO;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        // 链接断开alerView
        if (!_disconnectAlertView) {
            _disconnectAlertView = [[UIAlertView alloc] initWithTitle:@"NOTE" message:@"Connect Disconnect. please check and reopen app try again." delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        }
        [[HNIOMixManager shareManager].disconnectAlertView show];
    });
}

#pragma mark HNIOMixDeviceDelegate Helper method
- (void)syncDevice {
    // 发送数据给硬件,同步硬件
    // 准备数据
    NSMutableArray *volumes          = [NSMutableArray new];
    NSMutableArray *equalizers       = [NSMutableArray new];
    NSMutableArray *bassValues       = [NSMutableArray new];
    NSMutableArray *trebleValues     = [NSMutableArray new];
    NSMutableArray *highpassSubTypes = [NSMutableArray new];
    NSMutableArray *highpassValues   = [NSMutableArray new];
    NSMutableArray *lowpassSubTypes  = [NSMutableArray new];
    NSMutableArray *lowpassValues    = [NSMutableArray new];
    if (_store.allChannels.count > 0) {
        for (HNChannel *channel in _store.allChannels) {
            [volumes addObject:[NSNumber numberWithInteger:channel.volume]];
            [equalizers addObject:channel.equalizer];
            [bassValues addObject:[NSNumber numberWithInteger:channel.bassGainValue]];
            [trebleValues addObject:[NSNumber numberWithInteger:channel.trebleGainValue]];
            [highpassSubTypes addObject:[NSNumber numberWithInteger:channel.highPassSubType]];
            [highpassValues addObject:[NSNumber numberWithInteger:channel.highPassfrequencyValue]];
            [lowpassSubTypes addObject:[NSNumber numberWithInteger:channel.lowPassSubType]];
            [lowpassValues addObject:[NSNumber numberWithInteger:channel.lowPassfrequencyValue]];
        }
    }
    // 同步输入通道值(Monitor)
    [[HNIOMixManager shareManager].device syncLrInputValue:[_store.allChannels[0] lrInputValue] mainInputValue:[_store.allChannels[0] mainInputValue] auxInputValue:[_store.allChannels[0] auxInputValue]];
    usleep(sleepTime);
    // 同步音量
    [[HNIOMixManager shareManager].device syncVolume:volumes];
    usleep(sleepTime);
    // 同步EQ
    [equalizers enumerateObjectsUsingBlock:^(HNEqualizer  *_Nonnull equalizer, NSUInteger idx, BOOL * _Nonnull stop) {
        [[HNIOMixManager shareManager].device syncEQ:equalizer.Hz20
                                                 eq2:equalizer.Hz50
                                                 eq3:equalizer.Hz100
                                                 eq4:equalizer.Hz200
                                                 eq5:equalizer.Hz500
                                                 eq6:equalizer.K1
                                                 eq7:equalizer.K2
                                                 eq8:equalizer.K5
                                                 eq9:equalizer.K10
                                                eq10:equalizer.K20
                                          forChannel:idx];
        usleep(sleepTime);
    }];
    // 同步低音
    [[HNIOMixManager shareManager].device syncBass:bassValues];
    usleep(sleepTime);
    // 同步高音
    [[HNIOMixManager shareManager].device syncTreble:trebleValues];
    usleep(sleepTime);
    // 同步高通
    [[HNIOMixManager shareManager].device syncHighPass:highpassValues highPassSubTypes:highpassSubTypes];
    usleep(sleepTime);
    // 同步低通
    [[HNIOMixManager shareManager].device syncLowPass:lowpassValues lowPassSubType:lowpassSubTypes];
}
@end
