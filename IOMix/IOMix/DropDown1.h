//
//  DropDown1.h
//  DropDown
//
//  Created by clyd on 13-9-24.
//  Copyright (c) 2013年 clyd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DropDown1 : UIView<UITableViewDataSource,UITableViewDelegate, UITextFieldDelegate>

@property(nonatomic,strong) UITableView* tv;//下拉列表
@property(nonatomic,strong) NSArray* tableArray;//下拉列表数据
@property(nonatomic,strong) UITextField* textField;//文本输入框
@property(nonatomic) BOOL showList;//是否弹出下拉列表
@property(nonatomic) CGFloat tabheight;//table下拉列表的高度
@property(nonatomic) CGFloat frameHeight;//frame的高度

- (id)initWithFrame:(CGRect)frame tag:(NSInteger)tag;

@end
