//
//  HNTabBarViewController.m
//  IOMix
//
//  Created by YuHeng_Antony on 8/21/15.
//  Copyright (c) 2015 Homni Enterprises Co., Ltd. All rights reserved.
//

#import "HNTabBarViewController.h"
#import "HNHomeViewController.h"
#import "HNEuqalizerViewController.h"
#import "HNFilterViewController.h"
#import "HNMonitorViewController.h"
#import "HNSetupViewController.h"
#import "HNIOMixManager.h"
#import "HNPublic.h"
#import "IOMix-Swift.h"

#define _INTERNAL_SWIFT_USE_ 1

@interface HNTabBarViewController ()
@property (weak, nonatomic) IBOutlet UIView *contentView;

@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *tabBarButtons;

@property (strong, nonatomic) NSMutableDictionary       *controllerViews;
@property (strong, nonatomic) HNHomeViewController      *homeVC;
@property (strong, nonatomic) HNEuqalizerViewController *eqVC;
#if _INTERNAL_SWIFT_USE_
@property (strong, nonatomic) HNFilterViewControllerS   *filterVC;
#else
@property (strong, nonatomic) HNFilterViewController    *filterVC;
#endif
@property (strong, nonatomic) HNMonitorViewController   *monitorVC;
@property (strong, nonatomic) HNSetupViewController     *setupVC;

@end

@implementation HNTabBarViewController

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // 链接硬件
    [HNIOMixManager shareManager];
    
    _controllerViews = [NSMutableDictionary new];
    // 创建Home页并设置Home tabbar按钮为选择状态
    _homeVC = [HNHomeViewController new];
    [_contentView addSubview:[HNHomeViewController new].view];
    [_controllerViews setObject:_homeVC.view forKey:[@(HN_TabBarIndex_HOME) stringValue]];
    [_tabBarButtons[HN_TabBarIndex_HOME] setSelected:YES];
    
    // 提前创建EQ页(防止切换时卡顿)
    _eqVC = [HNEuqalizerViewController new];
    [_contentView addSubview:_eqVC.view];
    [_controllerViews setObject:_eqVC.view forKey:[@(HN_TabBarIndex_EQ) stringValue]];
    _eqVC.view.hidden = YES;
}

- (IBAction)tabBarToggle:(UIButton *)sender {
    switch (sender.tag) {
        case HN_TabBarIndex_HOME:
            [self changeStateFor:HN_TabBarIndex_HOME];
            break;
            
        case HN_TabBarIndex_EQ:
            if (!_eqVC) {
                _eqVC = [HNEuqalizerViewController new];
                [_contentView addSubview:_eqVC.view];
                [_controllerViews setObject:_eqVC.view forKey:[@(HN_TabBarIndex_EQ) stringValue]];
            }
            [self changeStateFor:HN_TabBarIndex_EQ];
            break;
            
        case HN_TabBarIndex_FILTER:
            if (!_filterVC) {
                #if _INTERNAL_SWIFT_USE_
                    _filterVC    = [HNFilterViewControllerS new];
                #else
                    _filterVC    = [HNFilterViewController new];
                #endif
                [_contentView addSubview:_filterVC.view];
                [_controllerViews setObject:_filterVC.view forKey:[@(HN_TabBarIndex_FILTER) stringValue]];
            }
            [self changeStateFor:HN_TabBarIndex_FILTER];
            break;
            
        case HN_TabBarIndex_MONITOR:
            if (!_monitorVC) {
                _monitorVC = [HNMonitorViewController new];
                [_contentView addSubview:_monitorVC.view];
                [_controllerViews setObject:_monitorVC.view forKey:[@(HN_TabBarIndex_MONITOR) stringValue]];
            }
            [self changeStateFor:HN_TabBarIndex_MONITOR];
            break;
            
        case HN_TabBarIndex_SETUP:
            if (!_setupVC) {
                _setupVC = [HNSetupViewController new];
                [_contentView addSubview:_setupVC.view];
                [_controllerViews setObject:_setupVC.view forKey:[@(HN_TabBarIndex_SETUP) stringValue]];
            }
            [self changeStateFor:HN_TabBarIndex_SETUP];
            break;
            
        default:
            break;
    }
}

- (void)changeStateFor:(NSUInteger)tabBarIndex {
    // 图标状态的切换
    [_tabBarButtons enumerateObjectsUsingBlock:^(UIButton *tabBarButton, NSUInteger idx, BOOL *stop) {
        tabBarButton.selected = tabBarIndex == idx ? YES : NO;
    }];
    
    // 视图的切换
    [_controllerViews enumerateKeysAndObjectsUsingBlock:^(NSString  *_Nonnull key, UIView  *_Nonnull controllerView, BOOL * _Nonnull stop) {
        // 如果key和传进来的索引一样，就设置controllerView的hidden为NO
        // 否则都设置为YES
        if ([[@(tabBarIndex) stringValue] isEqualToString:key]) {
            controllerView.hidden = NO;
        } else {
            controllerView.hidden = YES;
        }
    }];
}

@end
