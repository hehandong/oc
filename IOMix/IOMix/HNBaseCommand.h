//
//  HNBaseCommand.h
//  IOMix
//
//  Created by YuHeng_Antony on 11/6/15.
//  Copyright © 2015 Homni Enterprises Co., Ltd. All rights reserved.
//

#ifndef HNBaseCommand_h
#define HNBaseCommand_h

#endif /* HNBaseCommand_h */

#import <Foundation/Foundation.h>

#pragma mark - DEVICE COMMAND CODE
typedef NS_ENUM(NSUInteger, HNM2DCommands) {
    // MOBILE2DEVICE
    /// 通道控制(Monitor页面)
    HN_CONTROL_INPUT_CHANNEL = 0X10,
    /// 音量控制
    HN_CONTROL_VOLUME        = 0x11,
    /// EQ控制
    HN_CONTROL_EQ            = 0x12,
    /// 音效控制(8种预设音效)
    HN_CONTROL_EFFECT        = 0x13,
    /// 低音控制
    HN_CONTROL_BASS          = 0x14,
    /// 高音控制
    HN_CONTROL_TREBLE        = 0x15,
    /// 高通控制
    HN_CONTROL_HIGH_PASS     = 0x16,
    /// 低通控制
    HN_CONTROL_LOW_PASS      = 0x17,
    /// 修改SSID
    HN_MODIFY_SSID           = 0x18,
    /// 同步输入通道(Monitor页面)
    HN_SYNC_INPUT_CHANNLE    = 0x50,
    /// 同步音量
    HN_SYNC_VOLUME           = 0x51,
    /// 同步EQ
    HN_SYNC_EQ               = 0x52,
    /// 同步低音
    HN_SYNC_BASS             = 0x54,
    /// 同步高音
    HN_SYNC_TREBLE           = 0x55,
    /// 同步高通
    HN_SYNC_HIGH_PASS        = 0x56,
    /// 同步低通
    HN_SYNC_LOW_PASS         = 0x57,
};

#pragma mark - Mobile 2 Device
#pragma mark Command: 0x10 通道控制(Monitor页)
// Start of alignment pack, use 1 byte as unit type
#pragma pack(1)
typedef struct {
    char cmd;
    char outputChannelIndex;// 0x0-2,3个输出通道号
    char inputChannelIndex;// 0x00-0x04,5个输入通道选择,每一位对应一个通道选择,0代表不选择输入
    UInt16 reserved;
    char checksum;// 0x55
} M2DControlOutputChannelCommand;

#pragma mark Command: 0x11 音量控制
typedef struct {
    char cmd;
    char channelIndex;// 0x0-9,控制通道号
    char volumeValue;// 0-100,1-100为音量，0为静音
    UInt16 reserved;
    char checksum;
} M2DControlVolumeCommand;

#pragma mark Command: 0x12 EQ控制
typedef struct {
    char cmd;
    char channelIndex;// 0-9
    char frequencyIndex;// 0-9,控制频率点
    char gainValue;// 0-24,增益值
    UInt16 reserved;
    char checksum;
} M2DControlEQCommand;

#pragma mark Command: 0x13 音效控制
typedef struct {
    char cmd;
    char channelIndex;// 0-9
    char effectIndex;// 0x0-7,音效号
    UInt16 reserved;
    char checksum;
} M2DControlEffectCommand;

#pragma mark Command: 0x14 低音控制
typedef struct {
    char cmd;
    char channelIndex;// 0-9
    unsigned char bassGainValue;// 0x0-24,低音增益值
    UInt16 reserved;
    char checksum;
} M2DControlBassCommand;

#pragma mark Command: 0x15 高音控制
typedef struct {
    char cmd;
    char channelIndex;// 0-9
    unsigned char trebleGainValue;// 0x0-24,高音增益值
    UInt16 reserved;
    char checksum;
} M2DControlTrebleCommand;

#pragma mark Command: 0x16 高通控制
typedef struct {
    char cmd;
    char channelIndex;// 0-9
    char filterSubtypeMode;//0x0-5,滤波模式
    unsigned char highPassFrequencyValue;// 0x0-199,高通频率点(199为all pass)
    UInt16 reserved;
    char checksum;
} M2DControlHighPassCommand;

#pragma mark Command: 0x17 低通控制
typedef struct {
    char cmd;
    char channelIndex;// 0-9
    char filterSubtypeMode;//0x0-5,滤波模式
    unsigned char lowPassFrequencyValue;// 0x0-199,高通频率点(199为all pass)
    UInt16 reserved;
    char checksum;
} M2DControlLowPassCommand;

#pragma mark Command: 0x18 SSID更改
typedef struct {
    char cmd;
    char wifiMode;// 现在只有一种模式，固定发0
    char wifiNameLength;// 0x0-0x14
} M2DModifySSIDCommandHeader;

typedef struct {
    char segmentationCharacter;// 0xff 分割字符
    char wifiPasswordLength;// 0x0-A
} M2DModifySSIDCommandMiddle;

typedef struct {
    UInt16 reserved;
    char checksum;
} M2DModifySSIDCommandTail;

#pragma mark Command: 0x50 同步输入通道
typedef struct {
    char cmd;
    char lrInputValue;
    char mainInputValue;
    char auxInputValue;
    UInt32 reserved;
    char checksum;
} M2DSyncInputChannelCommand;

#pragma mark Command: 0x51 同步音量
typedef struct {
    char cmd;
    char channel_1_Volume;
    char channel_2_Volume;
    char channel_3_Volume;
    char channel_4_Volume;
    char channel_5_Volume;
    char channel_6_Volume;
    char channel_7_Volume;
    char channel_8_Volume;
    char channel_9_Volume;
    char channel_10_Volume;
    char checksum;
} M2DSyncVolumeCommand;

#pragma mark Command: 0x52 同步EQ
typedef struct {
    char cmd;
    char channelIndex;
    char eq1;
    char eq2;
    char eq3;
    char eq4;
    char eq5;
    char eq6;
    char eq7;
    char eq8;
    char eq9;
    char eq10;
    char checksum;
} M2DSyncEQCommand;

#pragma mark Command: 0x54 同步低音
typedef struct {
    char cmd;
    char channel_1_Bass;
    char channel_2_Bass;
    char channel_3_Bass;
    char channel_4_Bass;
    char channel_5_Bass;
    char channel_6_Bass;
    char channel_7_Bass;
    char channel_8_Bass;
    char channel_9_Bass;
    char channel_10_Bass;
    char checksum;
} M2DSyncBassCommand;

#pragma mark Command: 0x55 同步高音
typedef struct {
    char cmd;
    char channel_1_Treble;
    char channel_2_Treble;
    char channel_3_Treble;
    char channel_4_Treble;
    char channel_5_Treble;
    char channel_6_Treble;
    char channel_7_Treble;
    char channel_8_Treble;
    char channel_9_Treble;
    char channel_10_Treble;
    char checksum;
} M2DSyncTrebleCommand;

#pragma mark Command: 0x56 同步高通
typedef struct {
    char cmd;
    char channel_1_HighpassSubType;
    char channel_1_Highpass;
    char channel_2_HighpassSubType;
    char channel_2_Highpass;
    char channel_3_HighpassSubType;
    char channel_3_Highpass;
    char channel_4_HighpassSubType;
    char channel_4_Highpass;
    char channel_5_HighpassSubType;
    char channel_5_Highpass;
    char channel_6_HighpassSubType;
    char channel_6_Highpass;
    char channel_7_HighpassSubType;
    char channel_7_Highpass;
    char channel_8_HighpassSubType;
    char channel_8_Highpass;
    char channel_9_HighpassSubType;
    char channel_9_Highpass;
    char channel_10_HighpassSubType;
    char channel_10_Highpass;
    char checksum;
} M2DSyncHighpassCommand;

#pragma mark Command: 0x57 同步低通
typedef struct {
    char cmd;
    char channel_1_LowpassSubType;
    char channel_1_Lowpass;
    char channel_2_LowpassSubType;
    char channel_2_Lowpass;
    char channel_3_LowpassSubType;
    char channel_3_Lowpass;
    char channel_4_LowpassSubType;
    char channel_4_Lowpass;
    char channel_5_LowpassSubType;
    char channel_5_Lowpass;
    char channel_6_LowpassSubType;
    char channel_6_Lowpass;
    char channel_7_LowpassSubType;
    char channel_7_Lowpass;
    char channel_8_LowpassSubType;
    char channel_8_Lowpass;
    char channel_9_LowpassSubType;
    char channel_9_Lowpass;
    char channel_10_LowpassSubType;
    char channel_10_Lowpass;
    char checksum;
} M2DSyncLowpassCommand;

// End of alignment pack
#pragma pack()

#pragma mark - Networking
static NSString *HNServerDefaultIPAddress      = @"192.168.4.1";
static NSUInteger HN_TCP_COMMAND_PORT          = 12500;
static char HNChecksumChar55                   = 0x55;
static NSUInteger HN_TCP_COMMAND_COMMNONLY_TAG = 1;