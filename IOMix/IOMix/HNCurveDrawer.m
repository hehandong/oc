//
//  HNCurveDrawer.m
//  IOMix
//
//  Created by YuHeng_Antony on 11/12/15.
//  Copyright © 2015 Homni Enterprises Co., Ltd. All rights reserved.
//

#import "HNCurveDrawer.h"
#import "HNPointControl.h"
#import "HNPublic.h"
#import "UIBezierPath+LxThroughPointsBezier.h"

@interface HNCurveDrawer ()
{
    // for EQ曲线
    UIBezierPath * _curveEqBezierPath;
    CAShapeLayer * _shapeEqLayer;
    NSMutableArray * _pointViewArray;
    
    // for Filter曲线
    UIBezierPath * _curveFilterBezierPath;
    CAShapeLayer * _shapeFilterLayer;
}

@property (copy, nonatomic) NSMutableArray *initializePointValueArray;
@property (copy, nonatomic) NSMutableArray *fixedPointX;// 每个触摸点固定的X值
@property (copy, nonatomic) NSMutableArray *oldPointYs;// 纪录旧触摸点Y值

@end

@implementation HNCurveDrawer

- (void)drawCurveFor:(NSInteger)curveType filterSubType:(NSInteger)filterSubType UsePoints:(NSArray *)points {
    _pointViewArray = [[NSMutableArray alloc]init];// 触摸点(UIControl)数组
    _initializePointValueArray = [NSMutableArray array];
    
    // 根据传入的10个点，创建初始触控点(水平直线)
    for (int i = 0; i < points.count; i++) {
        NSValue * pointIValue = points[i];
        CGPoint point = [pointIValue CGPointValue];
        [self creatPointControlAt:point forCurve:curveType fliterSubType:filterSubType];
    }
    
    
    // 给每个触摸点标记tag值
    // 每個觸摸點的Y值，要對應上slider上的值
    [_pointViewArray enumerateObjectsUsingBlock:^(HNPointControl *  _Nonnull pointControl, NSUInteger idx, BOOL * _Nonnull stop) {
        pointControl.tag    = idx + 211;
        
        
        // 进入页面时(初始化时)，保持触摸点的值与slider上的值一致
        UISlider *slider    = _eqSliders[idx];
        float scale         = (float)slider.value / 24;
#pragma mark slider的值转换为触摸点的Y值
        // 计算出触摸点的Y值:(HN_BOTTOM_VALUE - HN_TOP_VALUE)表示底部点到顶部点之间的长度，乘上比列，加回顶点值
        float pointControlY = (HN_BOTTOM_VALUE - HN_TOP_VALUE) * scale + HN_TOP_VALUE;
        CGPoint center      = pointControl.center;
        center.y            = pointControlY;
        pointControl.center = center;
    }];
    

    // 根据触摸点绘制初始直线
    NSValue * firstPointValue = _initializePointValueArray.firstObject;
#pragma mark 根据触摸点(中心点坐标)绘制线条
    _curveEqBezierPath          = [UIBezierPath bezierPath];// CAShapeLayer要和UIBezierPath配合使用才有意义
    [_curveEqBezierPath moveToPoint:firstPointValue.CGPointValue];// 起点
    
    // 根据传入的curveType参数，调用不同的绘图方法(区别:控制点的算法不同)
    if (curveType == HN_CURVE_EQ) {
        [_curveEqBezierPath addBezierThroughPoints:_initializePointValueArray];// 终点？
        _shapeEqLayer               = [CAShapeLayer layer];
        _shapeEqLayer.strokeColor   = [UIColor colorWithRed:0.96 green:0.6 blue:0.08 alpha:1.0].CGColor;// 线条颜色
        _shapeEqLayer.fillColor     = nil;
        _shapeEqLayer.lineWidth     = 2;// 线条宽度
        _shapeEqLayer.path          = _curveEqBezierPath.CGPath;
        _shapeEqLayer.lineCap       = kCALineCapRound;
        [self.curveView.layer addSublayer:_shapeEqLayer];
    } else if (curveType == HN_CURVE_FILTER) {
        // Filter曲线，有自己的UIBezierPath对象
        _curveFilterBezierPath = [UIBezierPath bezierPath];
        [_curveFilterBezierPath moveToPoint:firstPointValue.CGPointValue];
        [_curveFilterBezierPath addBezierThroughFilterPoints:_initializePointValueArray filterSubType:filterSubType];
        _shapeFilterLayer               = [CAShapeLayer layer];
        _shapeFilterLayer.strokeColor   = [UIColor colorWithRed:0.96 green:0.6 blue:0.08 alpha:1.0].CGColor;// 线条颜色
        _shapeFilterLayer.fillColor     = nil;
        _shapeFilterLayer.lineWidth     = 2;// 线条宽度
        _shapeFilterLayer.path          = _curveFilterBezierPath.CGPath;
        _shapeFilterLayer.lineCap       = kCALineCapRound;
        [self.curveView.layer addSublayer:_shapeFilterLayer];
    }
}

-(void)creatPointControlAt:(CGPoint)location forCurve:(NSInteger)curveType fliterSubType:(NSInteger)filterSubType {
    HNPointControl *pointControl = [HNPointControl aInstance];
    pointControl.center = location;
    
    // 根据传入的curveType参数，禁用触摸
    if (curveType == HN_CURVE_EQ) {
        pointControl.userInteractionEnabled = YES;
    } else if (curveType == HN_CURVE_FILTER) {
        pointControl.userInteractionEnabled = NO;
    }
    
    
    if (!_fixedPointX) {
        _fixedPointX = [NSMutableArray array];
    }
    // 获取10个点的X值,用于固定在X轴上
    [_fixedPointX addObject:[NSNumber numberWithFloat:location.x]];
    
    if (!_oldPointYs) {
        _oldPointYs = [NSMutableArray array];
    }
    // 纪录触摸点旧的Y值
    [_oldPointYs addObject:[NSNumber numberWithFloat:location.y]];
    
#pragma mark - 触摸后重新绘制线条
    pointControl.dragCallBack = ^(HNPointControl *pv){
        [_curveEqBezierPath removeAllPoints];
        HNPointControl *firstPointControl = _pointViewArray.firstObject;
        firstPointControl.center = CGPointMake([_fixedPointX.firstObject floatValue], firstPointControl.center.y);
        if (firstPointControl.center.y < HN_TOP_VALUE) {
            firstPointControl.center = CGPointMake(HN_LEADING_VALUE, HN_TOP_VALUE);
        }
        if (firstPointControl.center.y > HN_BOTTOM_VALUE) {
            firstPointControl.center = CGPointMake(HN_LEADING_VALUE, HN_BOTTOM_VALUE);
        }
        [_curveEqBezierPath moveToPoint:firstPointControl.center];// 重绘起点
        
        // 重绘其他点
        NSMutableArray * pointValueArray = [NSMutableArray array];
        for (int i = 0; i < EQSliderCount; i++) {
            HNPointControl * pointControl = _pointViewArray[i];// 获取触摸点
            // 固定x点(只能移动y值/上下移动)
            pointControl.center = CGPointMake([_fixedPointX[i] floatValue], pointControl.center.y);
            
            // 防止触摸点超出上下视图边界
            if (pointControl.center.y < HN_TOP_VALUE) {
                pointControl.center = CGPointMake([_fixedPointX[i] floatValue], HN_TOP_VALUE);
            }
            if (pointControl.center.y > HN_BOTTOM_VALUE) {
                pointControl.center = CGPointMake([_fixedPointX[i] floatValue], HN_BOTTOM_VALUE);
            }
            
            [pointValueArray addObject:[NSValue valueWithCGPoint:pointControl.center]];
        }
        
        // 重新绘制
        if (curveType == HN_CURVE_EQ) {
            [_curveEqBezierPath addBezierThroughPoints:pointValueArray];// 终点？
        } else if (curveType == HN_CURVE_FILTER) {
            [_curveEqBezierPath addBezierThroughFilterPoints:pointValueArray filterSubType:filterSubType];
        }

        _shapeEqLayer.path = _curveEqBezierPath.CGPath;
        
        // 通知slider需要改变值了
        [_pointViewArray enumerateObjectsUsingBlock:^(HNPointControl *  _Nonnull pointControl, NSUInteger idx, BOOL * _Nonnull stop) {
            // 要转为int，否则float会
            if (!((int)pointControl.center.y == (int)[_oldPointYs[idx] floatValue])) {
                // 重新绘制后，通知slider，做调整slider的位置
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationEQPointControlYValueChanged object:nil userInfo:@{EQPointControlTag:[NSNumber numberWithInteger:pointControl.tag], EQPointControlValue:[NSNumber numberWithFloat:pointControl.center.y]}];
                // 要将新的y值放入_oldPointYs作为旧的，否则下次会判断错触摸点
                [_oldPointYs removeObjectAtIndex:idx];
                [_oldPointYs insertObject:[NSNumber numberWithFloat:pointControl.center.y] atIndex:idx];
            }
        }];
    };
#pragma mark - EQ slider改变后重新绘制线条
    // 观察slider的值是否改变，有改变的话要重绘触摸点
    [[NSNotificationCenter defaultCenter] addObserverForName:kNotificationEQSliderValueChanged object:nil queue:nil usingBlock:^(NSNotification *note) {
        
        // 0到24，计算出当前点所占的比例
        float scale = (float)[note.userInfo[EQSliderValue] integerValue] / 24;
#pragma mark slider的值转换为触摸点的Y值
        // 计算出触摸点的Y值:(HN_BOTTOM_VALUE - HN_TOP_VALUE)表示底部点到顶部点之间的长度，乘上比列，加回顶点值
        float pointControlY = (HN_BOTTOM_VALUE - HN_TOP_VALUE) * (1.0 - scale) + HN_TOP_VALUE;
        
        [_curveEqBezierPath removeAllPoints];
        
        // 重绘起点
        // 如果移动的是第一个slider,要根据移动比列改变起点的Y值;反之移动的不是第一个slider,则起点的Y值不变
        if ([note.userInfo[EQSliderTag] integerValue] == EQSlider20Hz) {
            HNPointControl *firstPointControl = _pointViewArray.firstObject;
            firstPointControl.center = CGPointMake([_fixedPointX.firstObject floatValue], pointControlY);
            if (firstPointControl.center.y < HN_TOP_VALUE) {
                firstPointControl.center = CGPointMake(HN_LEADING_VALUE, HN_TOP_VALUE);
            }
            if (firstPointControl.center.y > HN_BOTTOM_VALUE) {
                firstPointControl.center = CGPointMake(HN_LEADING_VALUE, HN_BOTTOM_VALUE);
            }
            [_curveEqBezierPath moveToPoint:firstPointControl.center];// 重绘起点
        }else {
            HNPointControl *firstPointControl = _pointViewArray.firstObject;
            [_curveEqBezierPath moveToPoint:firstPointControl.center];
        }
        
        // 重绘其他点
        NSMutableArray * pointValueArray = [NSMutableArray array];
        [_pointViewArray enumerateObjectsUsingBlock:^(HNPointControl * _Nonnull pointControl, NSUInteger idx, BOOL * _Nonnull stop) {
            // 判断移动的是哪个slider
            // 因为slider的tag值定义成211到220，所以要加上211
            if ([note.userInfo[EQSliderTag] integerValue] == idx + 211) {
                // 固定x点(只能移动y值/上下移动)
                pointControl.center = CGPointMake([_fixedPointX[idx] floatValue], pointControlY);
                
                // 防止触摸点超出上下视图边界
                if (pointControl.center.y < HN_TOP_VALUE) {
                    pointControl.center = CGPointMake([_fixedPointX[idx] floatValue], HN_TOP_VALUE);
                }
                if (pointControl.center.y > HN_BOTTOM_VALUE) {
                    pointControl.center = CGPointMake([_fixedPointX[idx] floatValue], HN_BOTTOM_VALUE);
                }
                
                [pointValueArray addObject:[NSValue valueWithCGPoint:pointControl.center]];
            } else {
                // 没有移动到的slider，对应的点的Y值，也不需要改变
                pointControl.center = CGPointMake([_fixedPointX[idx] floatValue], pointControl.center.y);
                
                // 防止触摸点超出上下视图边界
                if (pointControl.center.y < HN_TOP_VALUE) {
                    pointControl.center = CGPointMake([_fixedPointX[idx] floatValue], HN_TOP_VALUE);
                }
                if (pointControl.center.y > HN_BOTTOM_VALUE) {
                    pointControl.center = CGPointMake([_fixedPointX[idx] floatValue], HN_BOTTOM_VALUE);
                }
                
                [pointValueArray addObject:[NSValue valueWithCGPoint:pointControl.center]];
            }
        }];
        
        // 重新绘制
        if (curveType == HN_CURVE_EQ) {
            [_curveEqBezierPath addBezierThroughPoints:pointValueArray];// 终点？
        } else if (curveType == HN_CURVE_FILTER) {
//            [_curveEqBezierPath addBezierThroughFilterPoints:pointValueArray filterSubType:filterSubType];
        }
        
        _shapeEqLayer.path = _curveEqBezierPath.CGPath;
    }];
    
#pragma mark - Filter slider改变后重新绘制线条
    // 观察Filter的slider的值是否改变，有改变的话要重绘触摸点
    [[NSNotificationCenter defaultCenter] addObserverForName:kNotificationFilterSliderValueChanged object:nil queue:nil usingBlock:^(NSNotification *note) {
        [_curveFilterBezierPath removeAllPoints];
        _shapeFilterLayer.path = _curveFilterBezierPath.CGPath;
    }];

    
    [self.curveView addSubview:pointControl];// 触摸点(UIControl)加入父视图
    [_pointViewArray addObject:pointControl];// 触摸点(UIControl)加入数组
    [_initializePointValueArray addObject:[NSValue valueWithCGPoint:pointControl.center]];// 触摸点(UIControl)的中心点的值加入数组
}

@end
