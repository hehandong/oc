//
//  BNRDetailViewController.h
//  MyHomePwner
//
//  Created by homni_rd01 on 16/2/17.
//  Copyright © 2016年 Homni Electron Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
@class BNRItem;

@interface BNRDetailViewController : UIViewController <UIViewControllerRestoration>

@property (nonatomic,strong) BNRItem *item;
@property (nonatomic,copy) void (^dismissBlock)(void);

- (instancetype)initForNewItem:(BOOL)isNew;

@end
