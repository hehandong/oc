//
//  BNRImageStore.m
//  MyHomePwner
//
//  Created by homni_rd01 on 16/2/17.
//  Copyright © 2016年 Homni Electron Inc. All rights reserved.
//

#import "BNRImageStore.h"

@interface BNRImageStore ()
@property (nonatomic,strong) NSMutableDictionary *dictionary;
- (NSString *)imagePathForKey:(NSString *)key;
@end

@implementation BNRImageStore


//单例模式
+ (instancetype)shareStore
{
    static BNRImageStore *sharedStore = nil;
    
    static dispatch_once_t onceToken ;
    
        dispatch_once(&onceToken, ^{
            sharedStore = [[super allocWithZone:NULL] initPrivate] ;
        }) ;
    
    return sharedStore;
}


+(id) allocWithZone:(struct _NSZone *)zone
{
    return [BNRImageStore shareStore] ;
}


-(id) copyWithZone:(struct _NSZone *)zone
{
    return [BNRImageStore shareStore] ;
}

- (instancetype)initPrivate
{
    self = [super init];
    if (self) {
        _dictionary = [[NSMutableDictionary alloc] init];
        
        NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
        //将BNRImageStore对象注册为通知中心的观察者
        [nc addObserver:self selector:@selector(clearCache:) name:UIApplicationDidReceiveMemoryWarningNotification object:nil];
        
    }
    return self;
}



#pragma mark - 将图片保存至本地
- (void)setImage:(UIImage *)image forKey:(NSString *)key
{
//    [self.dictionary setObject:image forKey:key];
    self.dictionary[key] = image;
    
    //获取保存图片的路径
    NSString *imagePath = [self imagePathForKey:key];
    
    //从图片提取JPEG格式的数据
    NSData *data = UIImageJPEGRepresentation(image, 0.5);
    //将PEG格式的数据写入文件
    [data writeToFile:imagePath atomically:YES];
}

#pragma mark - 清除缓存
- (void)clearCache:(NSNotification *)note
{
    NSLog(@"flushing %d images out of the cache",[self.dictionary count]);
    [self.dictionary removeAllObjects];
}

- (UIImage *)imageForKey:(NSString *)key
{
    //先尝试通过字典对象获取图片
    UIImage *result = self.dictionary[key];
    
    if (!result) {
        NSString *imagePath = [self imagePathForKey:key];
        //通过文件创建UIImage对象
        result = [UIImage imageWithContentsOfFile:imagePath];
        //如果能够通过文件创建图片，就将其放入缓存
        if (result) {
            self.dictionary[key] = result;
        }else{
            NSLog(@"Error: unable to find %@",[self imagePathForKey:key]);
        }
    }
    
    return result;
}

#pragma mark -获取文件路径
- (NSString *)imagePathForKey:(NSString *)key{
    
    NSArray *documentDirectories = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDirectory = [documentDirectories firstObject];
    return [documentDirectory stringByAppendingPathComponent:key];

}

#pragma mark -删除图片
- (void)deleteImageForKey:(NSString *)key
{
    if (!key) {
        return;
    }
    
    [self.dictionary removeObjectForKey:key];
    
    NSString *imagePath = [self imagePathForKey:key];
    //删除保存在本地的图片
    [[NSFileManager defaultManager] removeItemAtPath:imagePath error:nil];
}

@end
