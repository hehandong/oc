//
//  main.m
//  MyHomePwner
//
//  Created by homni_rd01 on 16/2/16.
//  Copyright © 2016年 Homni Electron Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
