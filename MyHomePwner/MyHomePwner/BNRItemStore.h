//
//  BNRItemStore.h
//  MyHomePwner
//
//  Created by homni_rd01 on 16/2/16.
//  Copyright © 2016年 Homni Electron Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
@class BNRItem;

@interface BNRItemStore : NSObject
@property (nonatomic,readonly) NSArray *allItems;

//这是一个类方法
+ (instancetype)sharedStore;
- (BNRItem *)createItem;
- (void)removeItem:(BNRItem *)item;
- (void)moveItemAtIndex:(NSUInteger)fromIndex
                toIndex:(NSUInteger)toIndex;
- (BOOL)saveChanges;

@end
