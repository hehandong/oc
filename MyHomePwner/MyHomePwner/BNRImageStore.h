//
//  BNRImageStore.h
//  MyHomePwner
//
//  Created by homni_rd01 on 16/2/17.
//  Copyright © 2016年 Homni Electron Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "BNRImageStore.h"


@interface BNRImageStore : NSObject

+ (instancetype)shareStore;

- (void)setImage:(UIImage *)image forKey:(NSString *) key;
- (UIImage *)imageForKey:(NSString *)key;
- (void)deleteImageForKey:(NSString *)key;


@end
