//
//  BNRItemsViewController.h
//  MyHomePwner
//
//  Created by homni_rd01 on 16/2/16.
//  Copyright © 2016年 Homni Electron Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BNRDetailViewController.h"
#import "BNRItemCell.h"

@interface BNRItemsViewController : UITableViewController <UIViewControllerRestoration>

@end
