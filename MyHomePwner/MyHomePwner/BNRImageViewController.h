//
//  BNRImageViewController.h
//  MyHomePwner
//
//  Created by homni_rd01 on 16/2/18.
//  Copyright © 2016年 Homni Electron Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BNRImageViewController : UIViewController

@property (nonatomic,strong) UIImage *image;
@end
