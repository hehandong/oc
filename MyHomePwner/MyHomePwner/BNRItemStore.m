//
//  BNRItemStore.m
//  MyHomePwner
//
//  Created by homni_rd01 on 16/2/16.
//  Copyright © 2016年 Homni Electron Inc. All rights reserved.
//

#import "BNRItemStore.h"
#import "BNRItem.h"
#import "BNRImageStore.h"

@interface BNRItemStore ()
@property (nonatomic) NSMutableArray *privateItems;
@end

@implementation BNRItemStore

//单例
+ (instancetype)sharedStore
{
    static BNRItemStore *shareStore = nil;
    //判断是否创建一个shareStore对象
    static dispatch_once_t onceToken ;
    
    dispatch_once(&onceToken, ^{
        shareStore = [[super allocWithZone:NULL] initPrivate] ;
    }) ;
    
    return shareStore;
}


+ (id) allocWithZone:(struct _NSZone *)zone
{
    return [BNRItemStore sharedStore] ;
}


- (id) copyWithZone:(struct _NSZone *)zone
{
    return [BNRItemStore sharedStore] ;
}

#pragma mark - 私有的初始化方法
- (instancetype)initPrivate
{
    self = [super init];
    if (self) {

        //获取以前保存好的数据
        NSString *path = [self itemArchivePath];
        _privateItems = [NSKeyedUnarchiver unarchiveObjectWithFile:path];
        
        //如果之前没有保存过privateItems，就创建一个新的
        if (!_privateItems) {
            _privateItems = [[NSMutableArray alloc] init];
        }
    }
    return self;
}

- (NSArray *)allItems
{
    return self.privateItems;
}

- (BNRItem *)createItem
{
//    BNRItem *item = [BNRItem randomItem];
    BNRItem *item = [[BNRItem alloc] init];
    
    [self.privateItems addObject:item];
    return item;
}

- (void)removeItem:(BNRItem *)item
{
    NSString *key = item.itemKey;
    [[BNRImageStore shareStore] deleteImageForKey:key];
    
    [self.privateItems removeObjectIdenticalTo:item];
}

- (void)moveItemAtIndex:(NSUInteger)fromIndex toIndex:(NSUInteger)toIndex
{
    if (fromIndex == toIndex) {
        return;
    }
    //得到要移动的对象的指针
    BNRItem *item = self.privateItems[fromIndex];
    //将item从allItems数组中移除
    [self.privateItems removeObjectAtIndex:fromIndex];
    //根据新的索引，将item插回allItems数组
    [self.privateItems insertObject:item atIndex:toIndex];
}

#pragma mark - 获取文件路径
- (NSString *)itemArchivePath
{
    //获取沙盒中的目录全路径
    NSArray *documentDirectories = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDirectory = [documentDirectories firstObject];
    
    return [documentDirectory stringByAppendingPathComponent:@"items.archive"];
    
}

#pragma mark -NSKeyedArchiver固化数据
- (BOOL)saveChanges
{
    NSString *path = [self itemArchivePath];
    //如果固化成功就返回yes
    return [NSKeyedArchiver archiveRootObject:self.privateItems toFile:path];
}

@end
