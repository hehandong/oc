//
//  BNRMWebViewController.m
//  MyNerdfeed
//
//  Created by homni_rd01 on 16/2/22.
//  Copyright © 2016年 Homni Electron Inc. All rights reserved.
//

#import "BNRMWebViewController.h"

@implementation BNRMWebViewController


- (void)loadView
{
    UIWebView *webView = [[UIWebView alloc] init];
    //设置可以缩放
    webView.scalesPageToFit = YES;
    self.view = webView;
}

- (void)setURL:(NSURL *)URL
{
    _URL = URL;
    if (_URL) {
        NSURLRequest *req = [NSURLRequest requestWithURL:_URL];
        [(UIWebView *)self.view loadRequest:req];
    }
}

#pragma mark -splitViewController隐藏时调用
- (void)splitViewController:(UISplitViewController *)svc
     willHideViewController:(UIViewController *)aViewController
          withBarButtonItem:(UIBarButtonItem *)barButtonItem
       forPopoverController:(UIPopoverController *)pc
{
    barButtonItem.title = @"Courses";
    self.navigationItem.leftBarButtonItem = barButtonItem;

}

#pragma mark -splitViewController显示时调用
- (void)splitViewController:(UISplitViewController *)svc willShowViewController:(UIViewController *)aViewController invalidatingBarButtonItem:(UIBarButtonItem *)barButtonItem
{
    if (barButtonItem == self.navigationItem.leftBarButtonItem) {
        self.navigationItem.leftBarButtonItem = nil;
    }
}

@end
