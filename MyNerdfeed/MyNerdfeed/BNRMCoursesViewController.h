//
//  BNRMCoursesViewController.h
//  MyNerdfeed
//
//  Created by homni_rd01 on 16/2/19.
//  Copyright © 2016年 Homni Electron Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BNRMWebViewController;

@interface BNRMCoursesViewController : UITableViewController

@property (nonatomic) BNRMWebViewController *webViewController;

@end
