//
//  BNRMWebViewController.h
//  MyNerdfeed
//
//  Created by homni_rd01 on 16/2/22.
//  Copyright © 2016年 Homni Electron Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface BNRMWebViewController : UIViewController <UISplitViewControllerDelegate>

@property (nonatomic) NSURL *URL;

@end
