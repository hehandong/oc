//
//  FirstViewController.m
//  BlockDemo
//
//  Created by hehandong on 16/3/4.
//  Copyright © hehandong All rights reserved.
//

#import "FirstViewController.h"
#import "SecondViewController.h"

@interface FirstViewController ()
@property (weak, nonatomic) IBOutlet UILabel *showDataLabel;

@property (weak, nonatomic) IBOutlet UIButton *clickButton;
@end

@implementation FirstViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.showDataLabel.text = @"等待接收第二页的数据";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)clickEvent:(id)sender {
    
    SecondViewController *secondVC = [[SecondViewController alloc] init];
    
//6.在block的使用者中，接收过来的参数，并做其它操作
    [secondVC SetSecondDataBlock:^(NSString *showText) {
        self.showDataLabel.text  = [NSString stringWithFormat:@"我收到：%@", showText];
    }];
    
    
    [self presentViewController:secondVC animated:YES completion:nil];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
