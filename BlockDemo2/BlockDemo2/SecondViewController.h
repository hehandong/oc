//
//  SecondViewController.h
//  BlockDemo
//
//  Created by hehandong on 16/3/4.
//  Copyright © hehandong All rights reserved.
//

#import <UIKit/UIKit.h>

//block回传值得用法：
//1.声明一个无返回值的block(为block定义一个别名，方便用)
typedef void (^DataBlock)(NSString *showText);

@interface SecondViewController : UIViewController
//2.定义成属性
@property (nonatomic, copy) DataBlock SecondDataBlock;
//3.定义一个block的setter方法
-(void)SetSecondDataBlock:(DataBlock)block;

//4.(在SecondViewController.m文件)在控制器的实现文件中来实现block的setter方法：
//5.(在SecondViewController.m文件)使用block实现回调. 当点击按钮时，将需要的数据传递
//   给block的使用者(使用block回传值的时机)
//6.(FirstViewController.m文件)在block的使用者中，接收过来的参数，并做其它操作

@end
