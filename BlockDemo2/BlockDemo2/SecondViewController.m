//
//  SecondViewController.m
//  BlockDemo
//
//  Created by hehandong on 16/3/4.
//  Copyright © hehandong All rights reserved.
//

#import "SecondViewController.h"

@interface SecondViewController ()

@property (weak, nonatomic) IBOutlet UILabel *secondDataLabel;

@property (weak, nonatomic) IBOutlet UIButton *backButton;
@end

@implementation SecondViewController

- (void)viewDidLoad {
    [super viewDidLoad];
   
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

//4.实现block的setter方法
- (void)SetSecondDataBlock:(DataBlock)block{
    
    self.SecondDataBlock = block;

}

//5.使用block实现回调. 当点击按钮时，将需要的数据传递给block的使用者
- (IBAction)clickEvent:(id)sender {
    
    if(self.SecondDataBlock){
        self.SecondDataBlock(self.secondDataLabel.text);
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}


@end
