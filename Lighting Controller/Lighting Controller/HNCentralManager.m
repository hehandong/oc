//
//  HNCentralManager.m
//  Lighting Controller
//
//  Created by YuHeng_Antony on 12/3/15.
//  Copyright © 2015 Homni Electron Inc. All rights reserved.
//

#import "HNCentralManager.h"
#import "BabyBluetooth.h"
#import "HNBaseCommand.h"
#import "HNPublic.h"
#import "HNLCManager.h"
#import "HNLightingControllerStore.h"
#import "HNLight.h"
#import "SVProgressHUD.h"

@interface HNCentralManager ()
{
    BabyBluetooth *_baby;
}
// 保存扫描出来的设备
@property (copy, nonatomic) NSMutableArray *discoveredPeripherals;

// TODO:收到设备返回数据的提示框(后续可删除)
@property (strong, nonatomic) UIAlertView *receiveSyncDataAlertView;

// 用于判断手动断开其他设备，不需要重新链接
@property (nonatomic) BOOL needReconnect;

// 扫描定时器
@property (strong, nonatomic) NSTimer *scanTimeer;

// 没有扫描到设备的提示框
@property (strong, nonatomic) UIAlertView *scanNonPeripheralAlertView;

// 發送指令的定時器
@property (nonatomic, strong) NSTimer *sendTimer;

// 是否為發送狀態(處於發送狀態才發送指令)
@property (nonatomic) BOOL isSendState;

// 掃描的次數
@property (nonatomic) NSUInteger scanDeviceTimesTotal;

// 没有扫描到足夠的设备提示框
@property (strong, nonatomic) UIAlertView *scanNotEnoughPeripheralAlertView;

// 接收到硬件返回數據的次數計數(用於何時消失同步數據提示框)
@property (nonatomic) NSUInteger receiveDataCount;

@end

@implementation HNCentralManager

#pragma mark - 开始扫描设备
- (instancetype)init
{
    self = [super init];
    if (self) {
        
        _sendTimer = [NSTimer scheduledTimerWithTimeInterval:intervalTimeForSlider target:self selector:@selector(changeSendState) userInfo:nil repeats:YES];
        
        _scanDeviceTimesTotal = 0;
        
        // 接收到硬件返回數據的計數(用於拿到取消同步提示的時間點)
        _receiveDataCount = 0;
        
        _discoveredPeripherals = [[NSMutableArray alloc]init];
        _needReconnect = YES;
        
        // 实例化第三方库单例
        _baby = [BabyBluetooth shareBabyBluetooth];
        // 写各种回调
        [self babyDiscoverDelegate];
        [self babyConnectStateDelegate];
        [self babyServiceCharacteristicDelegate];
        [self babyOtherDelegate];
        // 开始扫描
        [_baby cancelAllPeripheralsConnection];
        // 扫描、链接、发现「服务」、发现「特征」、读取「特征」同时完成
        // 扫描:scanForPeripherals()
        // 连接:connectToPeripherals()
        // 发现「服務」:discoverServices()
        // 发现「特徵」:discoverCharacteristics()
        // 读取「特徵」值:readValueForCharacteristic()// 注意，一定要先执行scanForPeripherals()后才能执行后面的方法
        // 发现「Descriptor」:discoverDescriptorsForCharacteristic()
        // 读取「Descriptor」值:readValueForDescriptors()// 「Descriptor」感觉没有用到，所以没有调用
        //    _baby.scanForPeripherals().connectToPeripherals().discoverServices().discoverCharacteristics().readValueForCharacteristic().begin();
        
        // 因为要手动实现链接，所以一开始只做扫描动作
#pragma mark __步骤1__:开始扫描设备
        _baby.scanForPeripherals().begin();
    }
    return self;
}

- (void)changeSendState {
    if (_isSendState) {
        _isSendState = NO;
    }
    else {
        _isSendState = YES;
    }
}

#pragma mark - 各种回调
#pragma mark 设置扫描、链接的过滤条件;发现设备的回调
// 手机、外围设备(硬件)各种状态改变后的回调
- (void)babyDiscoverDelegate {
    __weak typeof(self) weakSelf = self;
    
#pragma mark __步骤2__:扫描计时开始(1.5秒)
    // 返回CBCentralManager实例的状态(手机蓝牙的状态，开、关等)
    [_baby setBlockOnCentralManagerDidUpdateState:^(CBCentralManager *central) {
        switch (central.state) {
            case CBCentralManagerStatePoweredOn:
//                NSLog(@"手机蓝牙状态: 打开");
                // 在确认手机蓝牙处于打开状态，才开始扫描计时
                _scanTimeer = [NSTimer scheduledTimerWithTimeInterval:scanTime target:self selector:@selector(shouldConnect) userInfo:nil repeats:NO];
                break;
            case CBCentralManagerStatePoweredOff:
                NSLog(@"手机蓝牙状态: 关闭");
                break;
            default:
                break;
        }
    }];
    
#pragma mark __步骤3__:将扫描到的peripheral(IUL11)加入一个array待用
    // 搜索到设备后的回调
    [_baby setBlockOnDiscoverToPeripherals:^(CBCentralManager *central, CBPeripheral *peripheral, NSDictionary *advertisementData, NSNumber *RSSI) {
//        NSLog(@"搜索到了设备:%@",peripheral.name);
        
        // 根据服务UUID来判断是否为IUL11
        /*
        NSArray *services = advertisementData[@"kCBAdvDataServiceUUIDs"];
        for (NSUUID *serviceUUID in services) {
            // 根据“服务”的UUID来识别是否为我们的设备(满足扫描预设时间后开始链接)
            if ([serviceUUID.UUIDString isEqualToString:kOurServiceUUIDString]) {
                NSLog(@"扫描到%@(该设备有名為%@的服务,是我们需要链接的设备)", peripheral.name, kOurServiceUUIDString);
                [weakSelf insertDiscoveredPeripheral:peripheral];
            }
        }
        */
        
        // 根據設備名稱來識別是否為我們的設備
        BOOL isIUL11 = [peripheral.name containsString:kIUL11NamePrefix];
        if (isIUL11) {
            NSLog(@"扫描到%@(该设备有我們設備名稱的前綴,是我们需要链接的设备)", peripheral.name);
            [weakSelf insertDiscoveredPeripheral:peripheral];
        }
    }];
}

#pragma mark 硬件链接状态变化的回调
- (void)babyConnectStateDelegate {
    __weak typeof(self) weakSelf = self;
    __weak typeof(_baby) weakBaby = _baby;
    
#pragma mark __步骤5__:链接成功,将设备的identifier赋值给light
#pragma mark __步骤6__:检查是否所有(发现的)设备都链接成功
    // 硬件链接成功的回调
    [_baby setBlockOnConnected:^(CBCentralManager *central, CBPeripheral *peripheral) {
        NSLog(@"设备: %@ -- 连接成功",peripheral.name);
        
        // 设置HNLight的identifier及isConnect属性(UI会根据HNLight来更新UI)
        [weakSelf setupLight:peripheral];
        
        // 检查是否所有被发现的设备都链接成功了,都链接了，就发送通告(IUL11收到硬件返回信息再停止加载)
        [weakSelf checkForPostAllConnectedNotification];
    }];
    
    // 硬件断开链接的回调(要进行动作:1、手动断开所有连接；2、判断是否已经断开所有连接，如果已经断开所有，则进行重连)
    [_baby setBlockOnDisconnect:^(CBCentralManager *central, CBPeripheral *peripheral, NSError *error) {
        NSLog(@"设备：%@--连接断开",peripheral.name);
        
        // 断开后重新链接
        if (weakSelf.needReconnect) {
            NSLog(@"发现友设备被断开了，主动斷開所有連接");
            [weakBaby cancelAllPeripheralsConnection];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD showWithStatus:@"Disconnect,Reconnecting" maskType:SVProgressHUDMaskTypeGradient];
            });
            
            // 判断是否所有链接都断开了
            BOOL isAllDisConnect = [weakSelf checkAllDevicesConnectState];
            if (isAllDisConnect) {
                NSLog(@"已经断开了所有连接，可以重新连接了");
                
                // 收到返回指令的计数，重新设置为0
                _receiveDataCount = 0;
                
                // 设置数据，并通知更新UI
                [weakSelf setupDisConnectedLights];
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationDevicesAllDisConnected object:nil];
                
                // 重新连接
                [weakSelf reConnect];
            }
        }
    }];
}

- (BOOL)checkAllDevicesConnectState {
    BOOL isAllDisConnect;
    if ([_baby findConnectedPeripherals].count == 0) {
        isAllDisConnect = YES;
    }
    return isAllDisConnect;
}

- (void)reConnect {
    if (_discoveredPeripherals.count == defaultConnectDeviceTotal) {
        // 链接设备(同时发现「服務」、读取「特徵」值)
        for (CBPeripheral *peripheral in _discoveredPeripherals) {
            _baby.having(peripheral).connectToPeripherals().discoverServices().discoverCharacteristics().begin();
        }
        
        // 通知UI更改加载提示为:链接中
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationScanTimeout object:nil];
    }
}

- (void)setupDisConnectedLights {
    for (HNLight *light in [HNLCManager shareManager].store.allLights) {
        light.name = kDefultLightName;
        light.isConnect = NO;
        light.isOn = NO;
        light.identifier = kDefaultIdentifier;
    }
}

#pragma mark 读取硬件数据(「服务」「特征」)的回调
- (void)babyServiceCharacteristicDelegate {
    __weak typeof(self) weakSelf = self;
    // 发现硬件「服务」后的回调
    [_baby setBlockOnDiscoverServices:^(CBPeripheral *peripheral, NSError *error) {
    }];
    
#pragma mark __步骤7__:發現「FF02特徵」，进行监听
    // 发现硬件「特征」后的回调
    [_baby setBlockOnDiscoverCharacteristics:^(CBPeripheral *peripheral, CBService *service, NSError *error) {
#if _INTERNAL_IUL11_USE_
        if ([service.UUID.UUIDString isEqualToString:kOurServiceUUIDString]) {
            for (CBCharacteristic *characteristic in service.characteristics) {
                if ([characteristic.UUID.UUIDString isEqualToString:kCharacteristicDataOutUUIDString]) {
                    // 發現“数据输出“「FF02特徵」,監聽;发同步指令
                    [weakSelf addObserveforPeripheral:peripheral];
                }
            }
        }
#else
#endif
    }];
    
    // 读取「特征」值后的回调
    [_baby setBlockOnReadValueForCharacteristic:^(CBPeripheral *peripheral, CBCharacteristic *characteristic, NSError *error) {
        
        // 发现”数据写入““特征”
        if ([characteristic.UUID.UUIDString isEqualToString:kCharacteristicDataInUUIDString]) {
        }
        
        // 打印出可以进行值写入的characteristic
        if (characteristic.properties & CBCharacteristicPropertyWrite || characteristic.properties & CBCharacteristicPropertyWriteWithoutResponse) {
//            NSLog(@"有可写入值的characteristic: %@ 现在的值是%@,", characteristic.UUID, characteristic.value);
        }
    }];
}

#pragma mark 其他回调(数据写入、通知……)
- (void)babyOtherDelegate {
    // 模块被修改名称后的回调
    [_baby setBlockOnDidUpdateName:^(CBPeripheral *peripheral) {
        for (HNLight *light in [HNLCManager shareManager].store.allLights) {
            if ([light.identifier isEqualToString:peripheral.identifier.UUIDString]) {
                NSString *realName = [peripheral.name substringFromIndex:5];
                light.name = realName;
            }
        }
        // 发送通知,蓝牙模块名字被更改了
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationPeripheralDidUpdateName object:nil];
    }];
    
    [_baby setBlockOnDidWriteValueForCharacteristic:^(CBCharacteristic *characteristic, NSError *error) {
        NSLog(@"已经向「特征」UUID:%@ 写入数据,特徵值:%@", characteristic.UUID,characteristic.value);
    }];
}

#pragma mark Helper Method
// 監聽硬件通告過來的新值
- (void)addObserveforPeripheral:(CBPeripheral *)peripheral {
    CBCharacteristic *dataOutCharacteristic = [self getCharacteristic:peripheral useCharacteristicUUIDString:kCharacteristicDataOutUUIDString fromService:kOurServiceUUIDString];

#pragma mark __步骤9__:监听到有数据返回,接收数据,更新模型数据
    // 監聽「FF02特徵」
    [_baby notify:peripheral characteristic:dataOutCharacteristic block:^(CBPeripheral *peripheral, CBCharacteristic *characteristics, NSError *error) {
        NSLog(@"______收到硬件从「FF02特徵」发出的新值: %@", characteristics.value);
        
//        if (peripheral == _discoveredPeripherals.lastObject) {
//            NSLog(@"接收到最后一个设备返回的数据，通知UI停止同步数据的提提示框");
//            // 如果接收到最后一个设备返回的数据，就通知UI停止同步数据的提提示框
//            [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationSyncDataDidCompleted object:nil];
//        }
        
        _receiveDataCount++;
        // 如果同步數據完成，通知UI停止提示框
        if (_receiveDataCount == [[_baby findConnectedPeripherals] count]) {
            [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationSyncDataDidCompleted object:nil];
        }

        // 利用硬件数据更新本地数据模型(再通知更新UI)
        const void *raw = [characteristics.value bytes];
        D2MDeviceParamResponse *responseData = (D2MDeviceParamResponse *)raw;
        if (responseData) {
            [[HNLCManager shareManager].store.allLights enumerateObjectsUsingBlock:^(HNLight  *_Nonnull light, NSUInteger idx, BOOL * _Nonnull stop) {
                if ([peripheral.identifier.UUIDString isEqualToString:light.identifier]) {
//                    NSLog(@"硬件返回的数据和Light的identifier一致,要更新Light值了");
                    light.brightness = responseData->brightnessValue;
                    light.colorR     = responseData->colourR;
                    light.colorG     = responseData->colourG;
                    light.colorB     = responseData->colourB;
                    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationReceiveDeviceData object:nil];
                }
            }];
        }
    }];
#pragma mark __步骤8__:监听动作结束后,发送同步指令
    // 监听完之后，再发同步/控制指令
    [self sendSyncCommandTo:peripheral];
    usleep(200000);// 200000μs(微秒) = 200ms(毫秒) = 0.2s(秒)
}

#pragma mark __步骤4__:计时结束,开始链接;并停止扫描
- (void)shouldConnect {
    // 停止扫描
    [_baby cancelScan];
    
    // 改為掃描夠固定模塊數量，再開始連接
    NSLog(@"已經掃描到設備的數量_discoveredPeripherals為:%lu", (unsigned long)_discoveredPeripherals.count);
    if (_discoveredPeripherals.count == defaultConnectDeviceTotal) {
        // 链接设备(同时发现「服務」、读取「特徵」值)
        for (CBPeripheral *peripheral in _discoveredPeripherals) {
            _baby.having(peripheral).connectToPeripherals().discoverServices().discoverCharacteristics().begin();
        }
        
        // 通知UI更改加载提示为:链接中
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationScanTimeout object:nil];
    }
    else {
        _scanDeviceTimesTotal++;
        if ((_scanDeviceTimesTotal == defaultScanDeviceTimesTotal) || (_scanDeviceTimesTotal > defaultScanDeviceTimesTotal)) {
            NSLog(@"重新搜索3次後，還是搜不夠足夠的設備,提示使用者");
            // 提示使用者
            if (!_scanNotEnoughPeripheralAlertView) {
                _scanNotEnoughPeripheralAlertView = [[UIAlertView alloc] initWithTitle:@"Note!" message:[NSString stringWithFormat:@"Just found %@ light(s),please check again.", @(_discoveredPeripherals.count)] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationScanNotEnoughDevices object:nil];
            });
            [_scanNotEnoughPeripheralAlertView show];
        }
        else {
            // 發送通知, 重新掃描ing
            [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationReScaning object:nil];
            
            // 停止3秒後再重新掃描
            sleep(2);
            NSLog(@"搜不夠足夠的設備，停2秒後重新搜索");
            _baby.scanForPeripherals().begin();
            _scanTimeer = [NSTimer scheduledTimerWithTimeInterval:scanTime target:self selector:@selector(shouldConnect) userInfo:nil repeats:NO];
        }
    }
    
    //TODO:后期应该改为：扫描发现的设备，如果不等于8，继续扫描3次，如果还是不等于8，也进行连接，不过要提示使用者
    /*
    NSLog(@"扫描1.5秒过去了，要开始链接硬件了,同时停止扫描");
    if (_discoveredPeripherals.count > 0) {
        // 链接设备(同时发现「服務」、读取「特徵」值)
        for (CBPeripheral *peripheral in _discoveredPeripherals) {
            _baby.having(peripheral).connectToPeripherals().discoverServices().discoverCharacteristics().begin();
            usleep(150000);// 150000μs(微秒) = 150ms(毫秒) = 0.15s(秒)
        }
        
        // 通知UI更改加载提示为:链接中
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationScanTimeout object:nil];
    }
    else {
        // 没有扫描到设备,弹出提示
        if (!_scanNonPeripheralAlertView) {
            _scanNonPeripheralAlertView = [[UIAlertView alloc] initWithTitle:@"NOTE" message:@"Scan 0 devices, please check again. " delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        }
        [_scanNonPeripheralAlertView show];
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationScanNonDevice object:nil];
    }
    */
}

// 检查是否所有设备都链接上
- (void)checkForPostAllConnectedNotification {
    // 遍历_discoveredPeripherals，如果所有对象都是已经链接状态，就发送通知
    BOOL allConnected = YES;
    for (CBPeripheral *peripheral in _discoveredPeripherals) {
        if (peripheral.state == CBPeripheralStateConnecting) {
            allConnected = NO;
        }
    }
    
    if (allConnected) {
        NSLog(@"所有设备链接成功了,要提示使用者链接成功了(共成功連接______%@________個設備)", @([[_baby findConnectedPeripherals] count]));
        // TODO:其实这个通告也没有必要传值,因为可以通过store类的connectedLightTotal属性拿到值
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationDeviceAllConnected
                                                            object:nil
                                                          userInfo:@{kConnectedPeripheralsTotal:[NSNumber numberWithInteger:[[_baby findConnectedPeripherals] count]]}];
    }
}

// 保存扫描到的设备到数组
- (void)insertDiscoveredPeripheral:(CBPeripheral *)peripheral {
    BOOL isContained = NO;
    
    for (CBPeripheral *discoveredPeripheral in _discoveredPeripherals) {
        if (discoveredPeripheral.identifier == peripheral.identifier) {
            isContained = YES;
        }
    }
    
    if (!isContained) {
        [_discoveredPeripherals addObject:peripheral];
    }
}

// 设置本地数据HNLight的isConnect属性及identifier
- (void)setupLight:(CBPeripheral *)peripheral {
    if ([_baby findConnectedPeripherals].count > 0) {
        NSArray *allLights = [HNLCManager shareManager].store.allLights;
        
        // 每次连接，按照连接顺序来将每个peripheral的identifier重新赋值给allLight中的light
        [[_baby findConnectedPeripherals] enumerateObjectsUsingBlock:^(CBPeripheral  *_Nonnull peripheral, NSUInteger idx, BOOL * _Nonnull stop) {
            HNLight *light = allLights[idx];
            NSString *realName = [peripheral.name substringFromIndex:5];
            light.name = realName;
            light.isConnect = YES;
            light.isOn = YES;
            light.identifier = peripheral.identifier.UUIDString;
            
            // 没有链接的设备，要设置回NO(防止中途断开模块，没有正确将light设置回断开状态)
            if (idx + 1 == [[_baby findConnectedPeripherals] count]) {
                for (int i = (int)[[_baby findConnectedPeripherals] count]; i < 8; i++) {
                    [allLights[i] setIsConnect:NO];
                    [allLights[i] setIsOn:NO];
                }
            }
        }];
    }
}

#pragma mark - 指令/数据的发送
#pragma mark 單個模塊下的指令發送
- (void)sendSyncCommandTo:(CBPeripheral *)peripheral {
    M2DControlSyncRequestCommand cmd;
    cmd.startBit = HNStartBitDA;
    cmd.cmd      = HN_COMMAND_SYNC_REQUEST;
    cmd.content  = 0x00;
    cmd.reserved = 0;
    cmd.checksum = HNChecksumChar88;
    NSData *syncData = [NSData dataWithBytes:&cmd length:sizeof(M2DControlSyncRequestCommand)];
    CBCharacteristic *dataInCharacteristic = [self getCharacteristic:peripheral useCharacteristicUUIDString:kCharacteristicDataInUUIDString fromService:kOurServiceUUIDString];
    if (dataInCharacteristic) {
        [peripheral writeValue:syncData forCharacteristic:dataInCharacteristic type:CBCharacteristicWriteWithoutResponse];
        NSLog(@"發送了同步指令:0x%x, 起始位:0x%x, 內容:%d, 寫入了「特徵」:%@", cmd.cmd, cmd.startBit, cmd.content, dataInCharacteristic.UUID);
    } else {
        NSLog(@"找不到writCharacteristic,无法发送指令");
    }
}

- (void)sendSyncCommand:(NSArray *)lightIdentifiers {
    for (CBPeripheral *peripheral in [_baby findConnectedPeripherals]) {
        if (lightIdentifiers.count > 0) {
            for (NSString *lightIdentifier in lightIdentifiers) {
                if ([lightIdentifier isEqualToString:peripheral.identifier.UUIDString]) {
                    M2DControlSyncRequestCommand cmd;
                    cmd.startBit = HNStartBitDA;
                    cmd.cmd      = HN_COMMAND_SYNC_REQUEST;
                    cmd.content  = 0x00;
                    cmd.reserved = 0;
                    cmd.checksum = HNChecksumChar88;
                    NSData *syncData = [NSData dataWithBytes:&cmd length:sizeof(M2DControlSyncRequestCommand)];
                    CBCharacteristic *dataInCharacteristic = [self getCharacteristic:peripheral useCharacteristicUUIDString:kCharacteristicDataInUUIDString fromService:kOurServiceUUIDString];
                    if (dataInCharacteristic) {
                        // 防止找不到“特征”导致崩溃
                        [peripheral writeValue:syncData forCharacteristic:dataInCharacteristic type:CBCharacteristicWriteWithoutResponse];
                    } else {
                        NSLog(@"找不到亮度的characteristic,无法发送指令");
                    }
                }
            }
        } else {
            NSLog(@"没有传入lightIdentifiers参数确认不了要发送指令给哪台硬件");
        }
    }
}

#pragma mark 指令——開燈、關燈指令
- (void)turnOnLights:(NSArray *)lightIdentifiers withBrihtnessValue:(uint8_t)brightnessValue {
    // 用_baby的findConnectedPeripherals得到当前链接的所有设备,这样就不需要手动保存已链接设备了
    for (CBPeripheral *peripheral in [_baby findConnectedPeripherals]) {
        if (lightIdentifiers.count > 0) {
            for (NSString *lightIdentifier in lightIdentifiers) {
                if ([lightIdentifier isEqualToString:peripheral.identifier.UUIDString]) {
                    M2DControlBrightnessCommand cmd;
                    cmd.startBit = HNStartBitDA;
                    cmd.cmd = HN_COMMAND_BRIGHTNESS;
                    cmd.brightnessValue = brightnessValue;
                    cmd.reserved = 0;
                    cmd.checksum = HNChecksumChar55;
                    NSData *brightnessData = [NSData dataWithBytes:&cmd length:sizeof(M2DControlBrightnessCommand)];
                    CBCharacteristic *dataInCharacteristic = [self getCharacteristic:peripheral useCharacteristicUUIDString:kCharacteristicDataInUUIDString fromService:kOurServiceUUIDString];
                    if (dataInCharacteristic) {
                        // 防止找不到“特征”导致崩溃
                            [peripheral writeValue:brightnessData forCharacteristic:dataInCharacteristic type:CBCharacteristicWriteWithoutResponse];
                            NSLog(@"發送了亮度指令給硬件%@，亮度值為%d", peripheral.identifier.UUIDString, brightnessValue);
                    } else {
                        NSLog(@"找不到亮度的characteristic,无法发送指令");
                    }
                }
            }
        } else {
            NSLog(@"没有传入lightIdentifiers参数确认不了要发送指令给哪台硬件");
        }
    }
}

- (void)turnOffLights:(NSArray *)lightIdentifiers {
    // 用_baby的findConnectedPeripherals得到当前链接的所有设备,这样就不需要手动保存已链接设备了
    for (CBPeripheral *peripheral in [_baby findConnectedPeripherals]) {
        if (lightIdentifiers.count > 0) {
            for (NSString *lightIdentifier in lightIdentifiers) {
                if ([lightIdentifier isEqualToString:peripheral.identifier.UUIDString]) {
                    M2DControlBrightnessCommand cmd;
                    cmd.startBit = HNStartBitDA;
                    cmd.cmd = HN_COMMAND_BRIGHTNESS;
                    cmd.brightnessValue = 0;
                    cmd.reserved = 0;
                    cmd.checksum = HNChecksumChar55;
                    NSData *brightnessData = [NSData dataWithBytes:&cmd length:sizeof(M2DControlBrightnessCommand)];
                    CBCharacteristic *dataInCharacteristic = [self getCharacteristic:peripheral useCharacteristicUUIDString:kCharacteristicDataInUUIDString fromService:kOurServiceUUIDString];
                    if (dataInCharacteristic) {
                        // 防止找不到“特征”导致崩溃
                        [peripheral writeValue:brightnessData forCharacteristic:dataInCharacteristic type:CBCharacteristicWriteWithoutResponse];
                        NSLog(@"發送了亮度指令給硬件%@，亮度值為%d", peripheral.identifier.UUIDString, 0);
                    } else {
                        NSLog(@"找不到亮度的characteristic,无法发送指令");
                    }
                }
            }
        } else {
            NSLog(@"没有传入lightIdentifiers参数确认不了要发送指令给哪台硬件");
        }
    }
}


#pragma mark 指令——亮度控制指令
- (void)setupLights:(NSArray *)lightIdentifiers brightnessValue:(uint8_t)brightnessValue {
    // 用_baby的findConnectedPeripherals得到当前链接的所有设备,这样就不需要手动保存已链接设备了
    for (CBPeripheral *peripheral in [_baby findConnectedPeripherals]) {
        if (lightIdentifiers.count > 0) {
            for (NSString *lightIdentifier in lightIdentifiers) {
                if ([lightIdentifier isEqualToString:peripheral.identifier.UUIDString]) {
                    M2DControlBrightnessCommand cmd;
                    cmd.startBit = HNStartBitDA;
                    cmd.cmd = HN_COMMAND_BRIGHTNESS;
                    cmd.brightnessValue = brightnessValue;
                    cmd.reserved = 0;
                    cmd.checksum = HNChecksumChar55;
                    NSData *brightnessData = [NSData dataWithBytes:&cmd length:sizeof(M2DControlBrightnessCommand)];
                    CBCharacteristic *dataInCharacteristic = [self getCharacteristic:peripheral useCharacteristicUUIDString:kCharacteristicDataInUUIDString fromService:kOurServiceUUIDString];
                    if (dataInCharacteristic) {
                        // 防止找不到“特征”导致崩溃
                        if (_isSendState) {
                            [peripheral writeValue:brightnessData forCharacteristic:dataInCharacteristic type:CBCharacteristicWriteWithoutResponse];
                            NSLog(@"發送了亮度指令給硬件%@，亮度值為%d", peripheral.identifier.UUIDString, brightnessValue);
                        }
                    } else {
                        NSLog(@"找不到亮度的characteristic,无法发送指令");
                    }
                }
            }
        } else {
            NSLog(@"没有传入lightIdentifiers参数确认不了要发送指令给哪台硬件");
        }
    }
}

#pragma mark 指令——颜色控制指令
- (void)setupLights:(NSArray *)lightIdentifiers colourR:(uint8_t)colourR colourG:(uint8_t)colourG colourB:(uint8_t)colourB {
    for (CBPeripheral *peripheral in [_baby findConnectedPeripherals]) {
        if (lightIdentifiers.count > 0) {
            for (NSString *lightIdentifier in lightIdentifiers) {
                if ([lightIdentifier isEqualToString:peripheral.identifier.UUIDString]) {
                    M2DControlColourCommand cmd;
                    cmd.startBit = HNStartBitDA;
                    cmd.cmd = HN_COMMAND_COLOUR;
                    cmd.colourR = colourR;
                    cmd.colourG = colourG;
                    cmd.colourB = colourB;
                    cmd.reserved = 0;
                    cmd.checksum = HNChecksumChar66;
                    NSData *colorData = [NSData dataWithBytes:&cmd length:sizeof(M2DControlColourCommand)];
                    CBCharacteristic *dataInCharacteristic = [self getCharacteristic:peripheral useCharacteristicUUIDString:kCharacteristicDataInUUIDString fromService:kOurServiceUUIDString];
                    if (dataInCharacteristic) {
                        if (_isSendState) {
                            // 防止找不到“特征”导致崩溃
                            [peripheral writeValue:colorData forCharacteristic:dataInCharacteristic type:CBCharacteristicWriteWithoutResponse];
                            NSLog(@"發送了修改顏色的指令");
                        }
                    } else {
                        NSLog(@"找不到颜色的characteristic,无法发送指令");
                    }
                }
            }
        } else {
            NSLog(@"没有传入lightIdentifiers参数确认不了要发送指令给哪台硬件");
        }
    }
}

- (void)setupImmediatelyLights:(NSArray *)lightIdentifiers colourR:(uint8_t)colourR colourG:(uint8_t)colourG colourB:(uint8_t)colourB {
    for (CBPeripheral *peripheral in [_baby findConnectedPeripherals]) {
        if (lightIdentifiers.count > 0) {
            for (NSString *lightIdentifier in lightIdentifiers) {
                if ([lightIdentifier isEqualToString:peripheral.identifier.UUIDString]) {
                    M2DControlColourCommand cmd;
                    cmd.startBit = HNStartBitDA;
                    cmd.cmd = HN_COMMAND_COLOUR;
                    cmd.colourR = colourR;
                    cmd.colourG = colourG;
                    cmd.colourB = colourB;
                    cmd.reserved = 0;
                    cmd.checksum = HNChecksumChar66;
                    NSData *colorData = [NSData dataWithBytes:&cmd length:sizeof(M2DControlColourCommand)];
                    CBCharacteristic *dataInCharacteristic = [self getCharacteristic:peripheral useCharacteristicUUIDString:kCharacteristicDataInUUIDString fromService:kOurServiceUUIDString];
                    if (dataInCharacteristic) {
                            // 防止找不到“特征”导致崩溃
                            [peripheral writeValue:colorData forCharacteristic:dataInCharacteristic type:CBCharacteristicWriteWithoutResponse];
                            NSLog(@"發送了修改顏色的指令");
                    } else {
                        NSLog(@"找不到颜色的characteristic,无法发送指令");
                    }
                }
            }
        } else {
            NSLog(@"没有传入lightIdentifiers参数确认不了要发送指令给哪台硬件");
        }
    }
}

#pragma mark 指令——修改硬件名称指令
- (void)setupNewnameForLights:(NSArray *)lightIdentifiers {
    for (CBPeripheral *peripheral in [_baby findConnectedPeripherals]) {
        if (lightIdentifiers.count > 0) {
            for (int i = 0; i < lightIdentifiers.count; i++) {
                NSString *lightIdentifier = lightIdentifiers[i];
                if ([lightIdentifier isEqualToString:peripheral.identifier.UUIDString]) {

                    NSMutableData *nameData = [NSMutableData data];
                    M2DControlSetNameCommandHeader cmdHeader;
                    cmdHeader.startBit = HNStartBitAA;
                    
                    // 拿到light的name值
                    HNLight *light = [HNLCManager shareManager].store.allLights[i];
                    // 动态长度的名称
                    [nameData appendBytes:&cmdHeader length:sizeof(M2DControlSetNameCommandHeader)];
                    if (light) {
                        [nameData appendBytes:[light.name UTF8String] length:light.name.length];
                    }
                    M2DControlSetNameCommandTail cmdTail;
                    cmdTail.checksum = HNChecksumChar77;
                    [nameData appendBytes:&cmdTail length:sizeof(M2DControlSetNameCommandTail)];

                    CBCharacteristic *dataInCharacteristic = [self getCharacteristic:peripheral useCharacteristicUUIDString:kCharacteristicDataInUUIDString fromService:kOurServiceUUIDString];
                    if (dataInCharacteristic) {
                        // 防止找不到“特征”导致崩溃
                        [peripheral writeValue:nameData forCharacteristic:dataInCharacteristic type:CBCharacteristicWriteWithoutResponse];
                        NSLog(@"发送了修改名称的指令:%@", nameData);
                    } else {
                        NSLog(@"找不到颜色的characteristic,无法发送指令");
                    }
                }
            }
        } else {
            NSLog(@"没有传入lightIdentifiers参数确认不了要发送指令给哪台硬件");
        }
    }
}

#pragma mark 指令——帮助硬件保存数据指令(传数据到硬件)
- (void)setupLights:(NSArray *)lightIdentifiers colourR:(uint8_t)colourR colourG:(uint8_t)colourG colourB:(uint8_t)colourB brightnessValue:(uint8_t)brightnessValue {
    for (CBPeripheral *peripheral in [_baby findConnectedPeripherals]) {
        if (lightIdentifiers.count > 0) {
            for (int i = 0; i < lightIdentifiers.count; i++) {
                NSString *lightIdentifier = lightIdentifiers[i];
                if ([lightIdentifier isEqualToString:peripheral.identifier.UUIDString]) {
                    
                    M2DControlHelpSaveCommand cmd;
                    cmd.startBit = HNStartBitDA;
                    cmd.cmd = HN_COMMAND_HLPE_SAVE;
                    cmd.colourR = colourR;
                    cmd.colourG = colourG;
                    cmd.colourB = colourB;
                    cmd.brightnessValue = brightnessValue;
                    cmd.reserved = 0;
                    cmd.checksum =HNChecksumChar9A;
                    
                    CBCharacteristic *dataInCharacteristic = [self getCharacteristic:peripheral useCharacteristicUUIDString:kCharacteristicDataInUUIDString fromService:kOurServiceUUIDString];
                    if (dataInCharacteristic) {
                        // 防止找不到“特征”导致崩溃
                        [peripheral writeValue:[NSData dataWithBytes:&cmd length:sizeof(M2DControlHelpSaveCommand)] forCharacteristic:dataInCharacteristic type:CBCharacteristicWriteWithoutResponse];
                        NSLog(@"发送了0x14指令,数据内容:%@", [NSData dataWithBytes:&cmd length:sizeof(M2DControlHelpSaveCommand)]);
                    } else {
                        NSLog(@"找不到颜色的characteristic,无法发送指令");
                    }
                }
            }
        } else {
            NSLog(@"没有传入lightIdentifiers参数确认不了要发送指令给哪台硬件");
        }
    }
}

#pragma mark 指令——upload:帮助硬件保存数据指令(传数据到硬件)
- (void)upload {
    if ([_baby findConnectedPeripherals].count > 0) {
        for (int i = 0; i < [_baby findConnectedPeripherals].count; i++) {
            CBPeripheral *peripheral = [_baby findConnectedPeripherals][i];
            HNLight *light = [HNLCManager shareManager].store.allLights[i];
            
            M2DControlHelpSaveCommand cmd;
            cmd.startBit = HNStartBitDA;
            cmd.cmd = HN_COMMAND_HLPE_SAVE;
            cmd.colourR = light.colorR;
            cmd.colourG = light.colorG;
            cmd.colourB = light.colorB;
            cmd.brightnessValue = light.brightness;
            cmd.reserved = 0;
            cmd.checksum =HNChecksumChar9A;
            
            CBCharacteristic *dataInCharacteristic = [self getCharacteristic:peripheral useCharacteristicUUIDString:kCharacteristicDataInUUIDString fromService:kOurServiceUUIDString];
            if (dataInCharacteristic) {
                // 防止找不到“特征”导致崩溃
                [peripheral writeValue:[NSData dataWithBytes:&cmd length:sizeof(M2DControlHelpSaveCommand)] forCharacteristic:dataInCharacteristic type:CBCharacteristicWriteWithoutResponse];
                NSLog(@"发送了0x14指令,数据内容:%@", [NSData dataWithBytes:&cmd length:sizeof(M2DControlHelpSaveCommand)]);
            } else {
                NSLog(@"找不到颜色的characteristic,无法发送指令");
            }
        }
    }
}

#pragma mark 指令——erase:帮助硬件恢复初始值
- (void)erase {
    if ([_baby findConnectedPeripherals].count > 0) {
        for (int i = 0; i < [_baby findConnectedPeripherals].count; i++) {
            CBPeripheral *peripheral = [_baby findConnectedPeripherals][i];
            
            M2DControlHelpSaveCommand cmd;
            cmd.startBit = HNStartBitDA;
            cmd.cmd = HN_COMMAND_HLPE_SAVE;
            cmd.colourR = 255;
            cmd.colourG = 255;
            cmd.colourB = 255;
            cmd.brightnessValue = 255;
            cmd.reserved = 0;
            cmd.checksum =HNChecksumChar9A;
            
            CBCharacteristic *dataInCharacteristic = [self getCharacteristic:peripheral useCharacteristicUUIDString:kCharacteristicDataInUUIDString fromService:kOurServiceUUIDString];
            if (dataInCharacteristic) {
                // 防止找不到“特征”导致崩溃
                [peripheral writeValue:[NSData dataWithBytes:&cmd length:sizeof(M2DControlHelpSaveCommand)] forCharacteristic:dataInCharacteristic type:CBCharacteristicWriteWithoutResponse];
                NSLog(@"发送了0x14指令,数据内容:%@", [NSData dataWithBytes:&cmd length:sizeof(M2DControlHelpSaveCommand)]);
            } else {
                NSLog(@"找不到颜色的characteristic,无法发送指令");
            }
        }
    }
}

#pragma mark 获取"特征"的Help Method
// Help Method 根据“设备”、“服务UUID”、“特征UUID”获取可以写入值的“特征”
- (CBCharacteristic *)getCharacteristic:(CBPeripheral *)peripheral useCharacteristicUUIDString:(NSString *)characteristicUUIDString fromService:(NSString *)serviceUUIDString {
    CBCharacteristic *myCharacteristic;
    for (CBService *service in peripheral.services) {
        // 根据service的UUID拿对应的「服务」
        if ([service.UUID.UUIDString isEqualToString:serviceUUIDString]) {
            for (CBCharacteristic *characteristic in service.characteristics) {
                // 根据characteristic的UUID拿对应的「特征」
                if ([characteristic.UUID.UUIDString isEqualToString:characteristicUUIDString]) {
                    myCharacteristic = characteristic;
                }
            }
        }
    }
    return myCharacteristic;
}

#pragma mark - 斷開藍牙連接
- (void)disConnectDevice {
    [_baby cancelAllPeripheralsConnection];
    NSLog(@"手動斷開了所有設備");
}

#pragma mark - 测试板的开关指令
- (void)setupLights:(NSArray *)lightIdentifiers state:(BOOL)open {
    [self setupPeripheral:lightIdentifiers state:open];
}

- (void)setupPeripheral:(NSArray *)lightIdentifiers state:(BOOL)open {
    NSLog(@"所有的链接设备：%@", [_baby findConnectedPeripherals]);
    
    for (CBPeripheral *peripheral in [_baby findConnectedPeripherals]) {
        if (lightIdentifiers.count > 0) {
            for (NSString *lightIdentifier in lightIdentifiers) {
                if ([lightIdentifier isEqualToString:peripheral.identifier.UUIDString]) {
                    CBCharacteristic *dataInCharacteristic = [self getCharacteristic:peripheral useCharacteristicUUIDString:kCharacteristicDataInUUIDString fromService:kOurServiceUUIDString];
                    if (dataInCharacteristic) {
                        if (open) {
                            // 开灯
                            M2DControlOnoffCommand cmd;
                            cmd.onOff = 0x01;
                            NSData *testData = [NSData dataWithBytes:&cmd length:sizeof(M2DControlOnoffCommand)];
                            [peripheral writeValue:testData forCharacteristic:dataInCharacteristic type:CBCharacteristicWriteWithResponse];
                        } else {
                            // 关灯
                            M2DControlOnoffCommand cmd;
                            cmd.onOff = 0x00;
                            NSData *testData = [NSData dataWithBytes:&cmd length:sizeof(M2DControlOnoffCommand)];
                            [peripheral writeValue:testData forCharacteristic:dataInCharacteristic type:CBCharacteristicWriteWithResponse];
                        }
                    } else {
                        NSLog(@"找不到颜色的characteristic,无法发送指令");
                    }
                }
            }
        } else {
            NSLog(@"没有传入lightIdentifiers参数确认不了要发送指令给哪台硬件");
        }
    }
}
@end
