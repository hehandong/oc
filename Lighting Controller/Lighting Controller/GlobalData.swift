//
//  GlobalData.swift
//  Lighting Controller
//
//  Created by homni_rd01 on 15/12/7.
//  Copyright © 2015年 Homni Electron Inc. All rights reserved.
//

import Foundation

var identifierStrings  = [Int:String]()

let lightsDataKey = "lightsData"

var lightsData = [Int:HNLight]()

var lightsInitNames = [
    "TOP_LEFT",
    "TOP_CENTRE",
    "TOP_RIGHT",
    "MIDDLE_LEFT",
    "MIDDLE_CENTRE",
    "MIDDLE_RIGHT",
    "BOTTOM_LEFT",
    "BOTTOM_CENTRE",]



