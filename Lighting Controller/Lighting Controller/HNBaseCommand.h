//
//  HNBaseCommand.h
//  Lighting Controller
//
//  Created by YuHeng_Antony on 11/30/15.
//  Copyright © 2015 Homni Electron Inc. All rights reserved.
//

#ifndef HNBaseCommand_h
#define HNBaseCommand_h

#endif /* HNBaseCommand_h */

#import <Foundation/Foundation.h>

static UInt8 HNStartBitDA    = 0xDA;
static UInt8 HNStartBitAA    = 0xAA;
static char HNChecksumChar55 = 0x55;
static char HNChecksumChar66 = 0x66;
static char HNChecksumChar77 = 0x77;
static char HNChecksumChar88 = 0x88;
static char HNChecksumChar99 = 0x99;
static char HNChecksumChar9A = 0x9A;

// TODO:确定指令「设备名」是什麼指令
#pragma mark - DEVICE COMMAND CODE
typedef NS_ENUM(NSUInteger, HNM2DCommands) {
    // MOBILE2DEVICE
    /// 亮度控制指令
    HN_COMMAND_BRIGHTNESS   = 0x10,
    /// 颜色控制指令
    HN_COMMAND_COLOUR       = 0x11,
    /// 请求(同步)数据指令
    HN_COMMAND_SYNC_REQUEST = 0x12,
    /// 发送亮度&颜色给硬件,协助硬件保存数据
    HN_COMMAND_HLPE_SAVE    = 0x14,
    /// 设置模块名称指令(这条指令没有指定指令号,只有起始位)
    HN_COMMAND_SET_NAME     = 0x00,
};

typedef NS_ENUM(NSUInteger, HND2MResponses) {
    HN_FEEDBACK_DATA = 0x13,
};

#pragma mark - Mobile 2 Device
#pragma mark Command: 0x10 亮度控制指令
#pragma pack(1)
typedef struct {
    UInt8 startBit;
    UInt8 cmd;
    UInt8 brightnessValue;// 取值范围:0-255, 0为灭，255为最亮
    UInt8 reserved;
    UInt8 checksum;// 0x55
} M2DControlBrightnessCommand;

#pragma mark Command: 0x11 颜色控制指令
typedef struct {
    UInt8 startBit;
    UInt8 cmd;
    UInt8 colourR;// 取值范围:0-255
    UInt8 colourG;
    UInt8 colourB;
    UInt8 reserved;
    UInt8 checksum;// 0x66
} M2DControlColourCommand;

#pragma mark Command: 0x12 请求(同步)数据指令
typedef struct {
    UInt8 startBit;
    UInt8 cmd;
    UInt8 content;// 内容定位为0x00
    UInt8 reserved;
    UInt8 checksum;// 0x88
} M2DControlSyncRequestCommand;

#pragma mark Command: 0x14 发送亮度&颜色给硬件,协助硬件保存数据
typedef struct {
    UInt8 startBit;
    UInt8 cmd;
    UInt8 colourR;// 取值范围:0-255
    UInt8 colourG;
    UInt8 colourB;
    UInt8 brightnessValue;// 取值范围:0-255, 0为灭，255为最亮
    UInt8 reserved;
    UInt8 checksum;// 0x9A
} M2DControlHelpSaveCommand;

#pragma mark Command: 设置蓝牙模块名称指令
typedef struct {
    UInt8 startBit;
} M2DControlSetNameCommandHeader;

typedef struct {
    UInt8 checksum;// 0x77
} M2DControlSetNameCommandTail;

#pragma mark Command: 测试指令
typedef struct {
    UInt8 onOff;// 0x00灭; 0x01亮
}M2DControlOnoffCommand;

#pragma mark - Device 2 Mobile
#pragma mark Response: 0x13 模块返回数据
typedef struct {
    UInt8 startBit;
    UInt8 cmd;
    UInt8 colourR;// 取值范围:0-255
    UInt8 colourG;
    UInt8 colourB;
    UInt8 brightnessValue;// 取值范围:0-255, 0为灭，255为最亮
    UInt8 reserved;
    UInt8 checksum;
} D2MDeviceParamResponse;


