//
//  HNHomeViewController.swift
//  Lighting Controller
//
//  Created by YuHeng_Antony on 12/2/15.
//  Copyright © 2015 Homni Electron Inc. All rights reserved.
//

import UIKit

class HNHomeViewController: UIViewController,HNCustomLightButtonDelegate,SwiftHSVColorPickerDelegate {
    
    var selectedColor: UIColor = UIColor.yellowColor()
    
    @IBOutlet weak var colorPicker: SwiftHSVColorPicker!
    
    @IBOutlet var lightButtonViews: [UIView]!
    
    @IBOutlet weak var lightSwitchButton: UIButton!
    
    @IBOutlet weak var groupEditButton: UIButton!
    
    @IBOutlet var groupButtons: [UIButton]!
    
    @IBOutlet weak var brightnessSlider: UISlider!
    
    var noLightAlertView:UIAlertView!
    
    var lightButtons = [HNCustomLightButton]()
    var selectedLightButtons = [Int:HNCustomLightButton]()
    
    var lightButton:HNCustomLightButton!
    var selectedLightButton:HNCustomLightButton!
    var editViewController:HNEditViewController!
    
    var value:CGFloat!
    var connectedLightTotal:Int32 = 0
    
    var identifierArray = [String]()
    
    
    
    //MARK:-UI自动布局时需要调用
    override func viewDidLayoutSubviews() {
        //自动布局加载完后，画一个圆形取色器
        if colorPicker.isInitColorWheel{
           colorPicker.setup()
           colorPicker.isInitColorWheel = false
        }
      
    }
    

    //MARK:-每次view出现时都调用
    override func viewWillAppear(animated: Bool) {
        reloadLightButtons()
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        colorPicker.addTarget(self, action: "colorPickerTouchUpInside", forControlEvents: UIControlEvents.TouchUpInside)
        
        brightnessSlider.addTarget(self, action: "brightSliderTouchUpInside", forControlEvents: UIControlEvents.TouchUpInside)
        
        
        colorPicker.setViewColor(selectedColor)
        colorPicker.delegate = self
        initLightButtons()
        
        initBrightnessSlider()
        
        
        NSNotificationCenter.defaultCenter().addObserverForName(kNotificationDeviceAllConnected, object: nil, queue: nil) { (lightNSNotification) -> Void in
            self.connectedLightTotal = HNLCManager.shareManager().store.connectedLightTotal
    
//            print("-------------------------连接到\(self.connectedLightTotal)个设备")
            // 根据搜索到的设备，设置灯按钮和数据
            var i: Int
            for i = 0; i < 8; ++i {
                self.lightButtons[i].isConnect = light(i).isConnect
                self.lightButtons[i].isOn = light(i).isOn
                self.lightButtons[i].setLightButtonBrightness()
//                print("---第\(i)个灯是否连接---\(light(i).isConnect)----是否开---\(light(i).isOn)")
               
            }
            
            //默认选中第一个灯按钮
//            if self.selectedLightButtons.count == 0 {
//                if self.connectedLightTotal != 0 {
//                    self.lightButtons[0].selcetedSameGroupBackground()
//                    identifierStrings[0] = light(0).identifier
//                    self.selectedLightButtons[0] = self.lightButtons[0]
//                    saveLightSelected(0, isSelected: true)
//                }
//            }
            
            self.reloadLightButtons()
            
        }
        
        // 所有設備斷開連接，更新UI
        NSNotificationCenter.defaultCenter().addObserverForName(kNotificationDevicesAllDisConnected, object: nil, queue: nil) { (lightNSNotification) -> Void in
            self.connectedLightTotal = HNLCManager.shareManager().store.connectedLightTotal

            // 根据搜索到的设备，设置灯按钮和数据
            var i: Int
            for i = 0; i < 8; ++i {
                self.lightButtons[i].isConnect = light(i).isConnect
                self.lightButtons[i].isOn = light(i).isOn
                self.lightButtons[i].setLightButtonBrightness()  
            }
            self.reloadLightButtons()
        }
        
        
        //接收硬件的资料，更新UI
        NSNotificationCenter.defaultCenter().addObserverForName(kNotificationReceiveDeviceData, object: nil, queue: nil) { (UINSNotification) -> Void in
//            print("-------------------------------UI通知")
//            print("----------------------------\(light(0).name)---R:\(light(0).colorR)G:\(light(0).colorG)B:\(light(0).colorB)--\(light(0).brightness)")
//            print("-----------------------------\(light(1).name)--R:\(light(1).colorR)G:\(light(1).colorG)B:\(light(1).colorB)--\(light(1).brightness)")
            self.reloadLightButtons()
            
            var flag:Bool = true
            for light in HNLCManager.shareManager().store.allLights{
                if (light as! HNLight).isSelected{
                    if flag {
                    self.brightnessSlider.setValue(Float((light as! HNLight).brightness), animated: true)
                    flag = false
                    }
                }
            }
        }
        
        NSNotificationCenter.defaultCenter().addObserverForName(kNotificationPeripheralDidUpdateName, object: nil, queue: nil) { (UINSNotification) -> Void in
            print("------------------------收到改名称通知")
            self.reloadLightButtonsName()
        }
        
    }
    
    func brightSliderTouchUpInside() {
        print("------------------------手指離開了slider")
        // 再發送一次指令
        HNLCManager.shareManager().manager.turnOnLights(identifierArray, withBrihtnessValue: UInt8(brightnessSlider.value))
    }
    
    func colorPickerTouchUpInside() {
        print("------------------------手指離開了colorPicker")
        // 再發送一次指令
        
    }
    
    //MARK:初始化亮度拖动条
    func initBrightnessSlider(){
        
        if brightnessSlider != nil{
            brightnessSlider.setMaximumTrackImage(UIImage(named: "cross_small"), forState: UIControlState.Normal)
            brightnessSlider.setMinimumTrackImage(UIImage(named: "cross_selected_small"), forState: UIControlState.Normal)
            brightnessSlider.setThumbImage(UIImage(named: "round_small"), forState: UIControlState.Normal)
            brightnessSlider.minimumValue = 0
            brightnessSlider.maximumValue = 255
            brightnessSlider.value = 255
        }
        
    }
    
    //MARK:-初始化灯按钮
    func  initLightButtons(){
        
        var i: Int
        for i = 0; i < 8; ++i {
            
            lightButton =  NSBundle.mainBundle().loadNibNamed("HNCustomLightButton", owner: self, options: nil).first as! HNCustomLightButton
            lightButton.setViewNoBorderColor()
            lightButton.delegate = self
            lightButton.clickListener()
        
            lightButtons.append(lightButton)
            self.lightButtonViews[i].addSubview(lightButtons[i])
            //约束
            lightButtons[i].setConstraints()
            
        }
        
    }
    
    
    //MARK:-重载灯按钮的数据
    func reloadLightButtons(){
        
        var i: Int
        for i = 0; i < 8; ++i {
        
            lightButtons[i].setLightColor(UIColor(red: CGFloat(Double(light(i).colorR)/255.0), green: CGFloat(Double(light(i).colorG)/255.0), blue: CGFloat(Double(light(i).colorB)/255.0), alpha: 1.0))
            
            lightButtons[i].groupIndex = light(i).groupIndex
            
            lightButtons[i].setLightName(light(i).name)
            lightButtons[i].setLightButtonBrightness()
            
            switch light(i).groupIndex {
            case 0:
                lightButtons[i].setLightGroup(UIImage(named: "box_group1")!)
            case 1:
                lightButtons[i].setLightGroup(UIImage(named: "box_group2")!)
            case 2:
                lightButtons[i].setLightGroup(UIImage(named: "box_group3")!)
            case 3:
                lightButtons[i].setLightGroup(UIImage(named: "box_group4")!)
            default:
                lightButtons[i].setLightGroup(UIImage(named: "box_empty")!)
            }
            
        }
        
        //之前是选中，有新灯加入同一个组时，
        var index: Int
        for index = 0; index < 8; ++index {
            if lightButtons[index].selected {
                sameGroupEvent(lightButtons[index])
            }
        }
        
        //之前有组按钮选中时
        for groupButton in groupButtons{
            if groupButton.selected{
               for button in lightButtons{
                if button.groupIndex == groupButton.tag{
                    
                    selectedLightButtons[(button.superview?.tag)!] = button
                    identifierStrings[(button.superview?.tag)!] = light((button.superview?.tag)!).identifier
                    //数据的保存
                    saveLightSelected((button.superview?.tag)!, isSelected: true)
                    button.selcetedSameGroupBackground()
                
                 }
               }
            }
        }
    
    }
    
    
    //MARK:-重载灯按钮的名字
    func reloadLightButtonsName(){
        
        var i: Int
        for i = 0; i < 8; ++i {
            lightButtons[i].setLightName(light(i).name)
        }
        
    }
    
    //MARK:-灯开或关的切换
    @IBAction func lightSwitchButtonAction(sender: UIButton) {
    
        print("点击了灯开关")
        
        var switchFlag:Bool = false
        var flag:Bool = true
        var brightness:UInt8 = 0
        
        for (tag,buttonView) in selectedLightButtons{
            
            buttonView.isOn = !buttonView.isOn
            buttonView.setLightButtonBrightness()
            saveLightIsOn(tag, isOn: buttonView.isOn)
            
            if flag {
                switchFlag = light(tag).isOn
                brightness = light(tag).brightness
                flag = false
            }
            
        }
        
        
        DicToArray()
        if switchFlag {
           //恢复亮度条
           brightnessSlider.setValue(Float(brightness), animated: true)
           //发送指令
//           HNLCManager.shareManager().manager.setupLights(identifierArray, brightnessValue: brightness)
            HNLCManager.shareManager().manager.turnOnLights(identifierArray, withBrihtnessValue: brightness)

        }
        else {
//           HNLCManager.shareManager().manager.setupLights(identifierArray, brightnessValue: UInt8(0))
            HNLCManager.shareManager().manager.turnOffLights(identifierArray)
        }
        
        
//        //肖工的设备发送指令
//        DicToArray()
//        if switchFlag{
//            HNLCManager.shareManager().manager.setupLights(identifierArray, state: true)
//        }else{
//            HNLCManager.shareManager().manager.setupLights(identifierArray, state: false)
//        }
       
        
    }
    
    
    //MARK:-群组编辑的点击事件，切换到编辑页面
    @IBAction func groupEdit(sender: UIButton) {
        
        print("-------------------------------groupEdit--\(light(0).name)")
        
        if editViewController == nil {
           editViewController = HNEditViewController(nibName:"HNEditViewController", bundle:nil)
        }
        //信息界面出现的动画方式
        editViewController.modalTransitionStyle = UIModalTransitionStyle.CoverVertical
        //界面跳转
        self.presentViewController(editViewController, animated:true, completion:nil)
        
//        print("点击了群编辑")
        
    }
    
    
    
    //MARK:-群组按钮的点击事件
    @IBAction func groupButtonAction(sender: UIButton) {
        
        groupsButtonsToggle(groupButtons, sender: sender)
        
    }
    
   
    //MARK:-亮度拖动条的事件
    @IBAction func brightnessSliderValueChange(sender: UISlider) {
    
        var flag:Bool = true
        //没有选择灯时
        if selectedLightButtons.count == 0 {
            if (noLightAlertView == nil){
                noLightAlertView = UIAlertView()
                noLightAlertView.title = "NOTE"
                noLightAlertView.message = "Please select a light"
                noLightAlertView.addButtonWithTitle("OK")
            }
            noLightAlertView.show()
            
        }
        
        for tag in selectedLightButtons.keys{
            if light(tag).isOn {
                saveLightBrightness(tag, brightness: UInt8(sender.value))
                if flag {
                    //MARK:-如果灯时开得发送亮度指令。。。
                    DicToArray()
                    HNLCManager.shareManager().manager.setupLights(identifierArray, brightnessValue: UInt8(sender.value))
                    flag = false
                }
            }
        }
    }
    
   
    //获取亮度的值
    func getBrightnessFromValue() -> CGFloat {
        print("---亮度\(value/255)")
        return value/255
    }
    
   
    //MARK:取色器的回调
    func touchColorWheel(view: SwiftHSVColorPicker, color: UIColor, brightness: CGFloat, colorR: CGFloat, colorG: CGFloat, colorB: CGFloat) {
        
        var flag:Bool = true
        //没有选择灯时
        if selectedLightButtons.count == 0 {
            if (noLightAlertView == nil){
                noLightAlertView = UIAlertView()
                noLightAlertView.title = "NOTE"
                noLightAlertView.message = "Please select a light"
                noLightAlertView.addButtonWithTitle("OK")
            }
            noLightAlertView.show()
        
        }
        
        for (tag,buttonView) in selectedLightButtons{
           //灯开时
          if light(tag).isOn {
                
              buttonView.setLightColor(UIColor(red: CGFloat(Double(colorR)/255.0), green: CGFloat(Double(colorG)/255.0), blue: CGFloat(Double(colorB)/255.0), alpha: 1.0))
              //保存数据
              saveLightColorR(tag, colorR: UInt8(colorR))
              saveLightColorG(tag, colorG: UInt8(colorG))
              saveLightColorB(tag, colorB: UInt8(colorB))
            
            if flag {
            //如果灯时开得就发指令
                DicToArray()
                HNLCManager.shareManager().manager.setupLights(identifierArray, colourR: UInt8(colorR), colourG: UInt8(colorG), colourB: UInt8(colorB))
                flag = false
            }
         }
        }
        
//        //肖工的设备发送指令
//        DicToArray()
//        if sendCMDFlag {
//            HNLCManager.shareManager().manager.setupLights(identifierArray, state: false)
//            
//        }else{
//            HNLCManager.shareManager().manager.setupLights(identifierArray, state: true)
//        }
        
    }
    
    //MARK:手指抬起来的取色器回调事件
    func touchUpInsideColorWheel(view: SwiftHSVColorPicker, color: UIColor, brightness: CGFloat, colorR: CGFloat, colorG: CGFloat, colorB: CGFloat) {
        
        print("TouchUpInside--再次发送指令")
        DicToArray()
        HNLCManager.shareManager().manager.setupImmediatelyLights(identifierArray, colourR: UInt8(colorR), colourG: UInt8(colorG), colourB: UInt8(colorB))
    }
    
    //MARK:灯按钮的点击事件回调
    func click(view: HNCustomLightButton, tag: Int) {
        
        colorPicker.colorPickerTag = tag
        //每点一次灯，设置亮度拖动条
        brightnessSlider.setValue(Float(light(tag).brightness), animated: true)
        
        lightsButtonsToggle(lightButtons, sender: view)
        
   }
    
    //MARK:同一组灯选中时的事件处理
    func sameGroupEvent(selcetedLight:HNCustomLightButton){
        //临时的选择状态标志，
        let flag = selcetedLight.selected
        //先清空以前选中的灯按钮
        selectedLightButtons.removeAll()
        identifierStrings.removeAll()
        
        for button in lightButtons{
        
            saveLightSelected((button.superview?.tag)!, isSelected: false)
            button.deSelcetedSameGroupBackground()
            
            }
        
        selcetedLight.selected = flag
        
        if selcetedLight.selected {
        //处理群按钮
        for groupButton in groupButtons {
            if groupButton.tag == selcetedLight.groupIndex {
                groupButton.selected = true
                
            }else{
                groupButton.selected = false
            }
            
        }
        
        var colorR: CGFloat = 0.0, colorG: CGFloat = 0.0, colorB: CGFloat = 0.0, alpha: CGFloat = 0.0
        selcetedLight.selectColorView.backgroundColor!.getRed(&colorR, green: &colorG, blue: &colorB, alpha: &alpha)
            
            
        //处理同一个群的灯按钮
        for button in lightButtons{
            if button.groupIndex == selcetedLight.groupIndex && selcetedLight.groupIndex != 4 {
                //按钮加入字典
                selectedLightButtons[(button.superview?.tag)!] = button
                identifierStrings[(button.superview?.tag)!] = light((button.superview?.tag)!).identifier
                //数据的保存
                saveLightSelected((button.superview?.tag)!, isSelected: true)
            
                saveLightColorR((button.superview?.tag)!, colorR: UInt8(colorR*255))
                saveLightColorG((button.superview?.tag)!, colorG: UInt8(colorG*255))
                saveLightColorB((button.superview?.tag)!, colorB: UInt8(colorB*255))
                
//                print("---\(UInt8(colorR*255))---\(UInt8(colorG*255))---\(UInt8(colorB*255))")
                
                saveLightIsOn((button.superview?.tag)!, isOn: selcetedLight.isOn)
                
                //视图的更新
                button.isOn = selcetedLight.isOn
                button.setLightButtonBrightness()
                button.selectColorView.backgroundColor = selcetedLight.selectColorView.backgroundColor
                button.selcetedSameGroupBackground()
            }
            
        }
         
        //处理没有群组的灯按钮
        if selcetedLight.groupIndex == 4{
            selectedLightButtons[(selcetedLight.superview?.tag)!] = selcetedLight
            let lightIdentifier = light((selcetedLight.superview?.tag)!).identifier
            identifierStrings[(selcetedLight.superview?.tag)!] = lightIdentifier
            saveLightSelected((selcetedLight.superview?.tag)!, isSelected: true)
            selcetedLight.selcetedSameGroupBackground()
        
        }
        
            
        }else{
        
            for groupButton in groupButtons {
                if groupButton.tag == selcetedLight.groupIndex {
                    groupButton.selected = false
                }
            }
            
            //处理同一个群的灯按钮
            for button in lightButtons{
                if button.groupIndex == selcetedLight.groupIndex && selcetedLight.groupIndex != 4 {
                    selectedLightButtons[(button.superview?.tag)!] = nil
                    identifierStrings[(button.superview?.tag)!] = nil
                    saveLightSelected((button.superview?.tag)!, isSelected: false)
                    button.deSelcetedSameGroupBackground()
                }
            }
           
            if selcetedLight.groupIndex == 4{
                selectedLightButtons[(selcetedLight.superview?.tag)!] = nil
                identifierStrings[(selcetedLight.superview?.tag)!] = nil
                saveLightSelected((selcetedLight.superview?.tag)!, isSelected: false)
                selcetedLight.deSelcetedSameGroupBackground()
                
            }
        }
    }
    
    //MARK:-灯按钮互斥，没有群组时只能选择一个，有群组时，只能选择一个群组
    func lightsButtonsToggle(lightsButtons:[HNCustomLightButton],sender:HNCustomLightButton){
        
        if sender.groupIndex == 4{
        
        singleLightEvent(sender)
            
        for button in lightsButtons {
            if sender.selected {
                if !(button == sender) {
                    button.selected = false
                    singleLightEvent(button)
                }
            }
          }
        }else{
        
        sameGroupEvent(sender)
        
        }
    }
    
    
    func singleLightEvent(sender:HNCustomLightButton){
    
        if sender.selected {
            sender.setSelectedBackground()
            selectedLightButtons[(sender.superview?.tag)!] = sender

            let lightIdentifier = light((sender.superview?.tag)!).identifier
            identifierStrings[(sender.superview?.tag)!] = lightIdentifier

            saveLightSelected((sender.superview?.tag)!, isSelected: true)
            //处理组按钮
            for groupButton in groupButtons {
                groupButton.selected = false
            }
            
            brightnessSlider.setValue(Float(light((sender.superview?.tag)!).brightness), animated: false)
            
        }else{
            
            sender.setSelectedBackground()
            selectedLightButtons[(sender.superview?.tag)!] = nil
            identifierStrings[(sender.superview?.tag)!] = nil
            saveLightSelected((sender.superview?.tag)!, isSelected: false)
        }
    }
    
    
    //MARK:-群组按钮互斥，只能选择一个群组
    func groupsButtonsToggle(groupsButtons:[UIButton],sender:UIButton){
        
        sender.selected = !sender.selected
//        //先清空以前选中的灯按钮
//        selectedLightButtons.removeAll()
//        identifierStrings.removeAll()
//        
//        for button in lightButtons{
//            saveLightSelected((button.superview?.tag)!, isSelected: false)
//            button.deSelcetedSameGroupBackground()
//            
//        }
        
        var flag:Bool = true
        
        if sender.selected {

        for lightButton in lightButtons {
            if lightButton.groupIndex == sender.tag {
                //处理同一个组的灯按钮
                selectedLightButtons[(lightButton.superview?.tag)!] = lightButton
                identifierStrings[(lightButton.superview?.tag)!] = light((lightButton.superview?.tag)!).identifier
                saveLightSelected((lightButton.superview?.tag)!, isSelected: true)
                lightButton.selcetedSameGroupBackground()
                //更新亮度拖动条
                if flag {
                   brightnessSlider.setValue(Float(light((lightButton.superview?.tag)!).brightness), animated: true)
                   flag = false
                  }
                
            }else{
                //处理不是一个组的按钮
                selectedLightButtons[(lightButton.superview?.tag)!] = nil
                identifierStrings[(lightButton.superview?.tag)!] = nil
                saveLightSelected((lightButton.superview?.tag)!, isSelected: false)
                lightButton.deSelcetedSameGroupBackground()
            }
          }
        }
            
        else {
          //组按钮没有选中时
          for lightButton in lightButtons {
            if lightButton.groupIndex == sender.tag {
            selectedLightButtons[(lightButton.superview?.tag)!] = nil
            identifierStrings[(lightButton.superview?.tag)!] = nil
            saveLightSelected((lightButton.superview?.tag)!, isSelected: false)
            lightButton.deSelcetedSameGroupBackground()
            }
          }
        }
        //处理其他组按钮
        for groupsButton in groupsButtons {
            if sender.selected {
                if !(groupsButton == sender) {
                    groupsButton.selected = false
                  
                }
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK：-字典转数组
    func DicToArray(){
        identifierArray.removeAll()
        for (_,identifier) in identifierStrings{
            identifierArray.append(identifier)
        }
    }
   
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
