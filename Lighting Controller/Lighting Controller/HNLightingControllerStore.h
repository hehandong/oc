//
//  HNLightingControllerStore.h
//  Lighting Controller
//
//  Created by YuHeng_Antony on 12/7/15.
//  Copyright © 2015 Homni Electron Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HNLightingControllerStore : NSObject

@property (strong, nonatomic) NSArray *allLights;

/**
 *  返回已经链接light的数量
 */
@property (nonatomic) int connectedLightTotal;

@property (readonly, nonatomic) NSArray *allGroups;

+ (instancetype)shareStore;

- (BOOL)saveData;
@end
