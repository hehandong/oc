//
//  AppDelegate.m
//  Lighting Controller
//
//  Created by YuHeng_Antony on 11/30/15.
//  Copyright © 2015 Homni Electron Inc. All rights reserved.
//

#import "AppDelegate.h"
#import "HNHomeViewControllerOC.h"
#import "HNLCManager.h"
#import "HNCentralManager.h"
#import "HNLightingControllerStore.h"
#import "SVProgressHUD.h"
#import "HNPublic.h"
#import "HNLight.h"

#define _INTERNAL_OBJECTIVE_C_USE_ 0

@interface AppDelegate ()

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    // 链接前将所有light的isConnect属性都要改为NO
    [[HNLCManager shareManager].store.allLights enumerateObjectsUsingBlock:^(HNLight  *_Nonnull light, NSUInteger idx, BOOL * _Nonnull stop) {
        light.isConnect = NO;
        light.isOn      = NO;
    }];
    
    // 启动蓝牙链接
    __unused HNCentralManager *manger = [HNLCManager shareManager].manager;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [SVProgressHUD showWithStatus:@"Scanning" maskType:SVProgressHUDMaskTypeGradient];
    });
    
    // 重新掃描的提示
    [[NSNotificationCenter defaultCenter] addObserverForName:kNotificationReScaning object:nil queue:nil usingBlock:^(NSNotification * _Nonnull note) {
        [SVProgressHUD showWithStatus:@"Rescanning" maskType:SVProgressHUDMaskTypeGradient];
    }];
    
    // 扫描结束，更改提示
    [[NSNotificationCenter defaultCenter] addObserverForName:kNotificationScanTimeout object:nil queue:nil usingBlock:^(NSNotification * _Nonnull note) {
        [SVProgressHUD showWithStatus:@"Connecting" maskType:SVProgressHUDMaskTypeGradient];
    }];
    
    // 链接成功，更改提示
    [[NSNotificationCenter defaultCenter] addObserverForName:kNotificationDeviceAllConnected object:nil queue:nil usingBlock:^(NSNotification * _Nonnull note) {
        // 提示連接成功後,開始同步數據
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD showWithStatus:[NSString stringWithFormat:@" \"%@\" Devices connected success. Syncing Data", note.userInfo[kConnectedPeripheralsTotal]] maskType:SVProgressHUDMaskTypeGradient];
        });
    }];
    
    // 同步數據結束,停止提示介面
    [[NSNotificationCenter defaultCenter] addObserverForName:kNotificationSyncDataDidCompleted object:nil queue:nil usingBlock:^(NSNotification * _Nonnull note) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD dismiss];
        });
    }];
    
    // 没有扫描到设备,停止加载
    [[NSNotificationCenter defaultCenter] addObserverForName:kNotificationScanNonDevice object:nil queue:nil usingBlock:^(NSNotification * _Nonnull note) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD dismiss];
        });
    }];
    
    // 沒有掃描到足夠的設備,停止加載
    [[NSNotificationCenter defaultCenter] addObserverForName:kNotificationScanNotEnoughDevices object:nil queue:nil usingBlock:^(NSNotification * _Nonnull note) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD dismiss];
        });
    }];
    
#if _INTERNAL_OBJECTIVE_C_USE_
    self.window.rootViewController = [[HNHomeViewControllerOC alloc] init];
#else
#endif
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    [[HNLCManager shareManager].store saveData];
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    NSLog(@"退出APP了, 要手動斷開藍牙連接了");
    [[HNLCManager shareManager].manager disConnectDevice];
}

@end
