//
//  HNHomeViewControllerOC.m
//  Lighting Controller
//
//  Created by YuHeng_Antony on 12/3/15.
//  Copyright © 2015 Homni Electron Inc. All rights reserved.
//

#import "HNHomeViewControllerOC.h"
#import "HNLCManager.h"
#import "HNCentralManager.h"
#import "HNLightingControllerStore.h"
#import "HNPublic.h"
#import "HNLight.h"

@interface HNHomeViewControllerOC ()<UITextFieldDelegate>

#pragma mark 通过一个数组保存每个HNLight的identifier(字符串)，进而知道控制那些灯
/**
 *  保存哪些硬件需要发送指令
 */
@property (copy, nonatomic) NSMutableArray *identifierStrings;

@property (weak, nonatomic) IBOutlet UITextField *textFieldOne;
@property (weak, nonatomic) IBOutlet UITextField *textFieldTwo;
@property (weak, nonatomic) IBOutlet UITextField *textFieldThree;
@property (weak, nonatomic) IBOutlet UITextField *textFieldFour;

@property (weak, nonatomic) IBOutlet UITextField *enterNameTextFild;

@property (weak, nonatomic) IBOutlet UISlider *brightnessSlider;
@property (weak, nonatomic) IBOutlet UISlider *rSlider;
@property (weak, nonatomic) IBOutlet UISlider *gSlider;
@property (weak, nonatomic) IBOutlet UISlider *bSlider;

@end

@implementation HNHomeViewControllerOC

- (void)viewDidLoad {
    [super viewDidLoad];

    _identifierStrings = [[NSMutableArray alloc] init];

#if _INTERNAL_IUL11_USE_
    [[NSNotificationCenter defaultCenter] addObserverForName:kNotificationDeviceAllConnected object:nil queue:nil usingBlock:^(NSNotification * _Nonnull note) {
        // 如果是断开重新链接要清空
        [_identifierStrings removeAllObjects];
        
        // 检查有几个light被链接，将链接的light的identifier加入_identifierStrings，达到同时控制的木的
        int total = [HNLCManager shareManager].store.connectedLightTotal;
        for (int i = 0; i < total; i++) {
            HNLight *light = [HNLCManager shareManager].store.allLights[i];
            [_identifierStrings addObject:light.identifier];
        }
    }];
    
    // 收到硬件数据，更新UI
    [[NSNotificationCenter defaultCenter] addObserverForName:kNotificationReceiveDeviceData object:nil queue:nil usingBlock:^(NSNotification * _Nonnull note) {
        // TODO:更新8个UI控件的界面数据
        HNLight *light = [HNLCManager shareManager].store.allLights[0];
        _brightnessSlider.value = light.brightness;
        _rSlider.value = light.colorR;
        _gSlider.value = light.colorG;
        _bSlider.value = light.colorB;
    }];
#else
    // 显示测试板名称
    NSArray *allLight = [HNLCManager shareManager].store.allLights;
    
    HNLight *oneLight = allLight[_textFieldOne.tag];
    _textFieldOne.text = oneLight.name;
    
    HNLight *twoLight = allLight[_textFieldTwo.tag];
    _textFieldTwo.text = twoLight.name;
    
    HNLight *threeLight = allLight[_textFieldThree.tag];
    _textFieldThree.text = threeLight.name;
    
    HNLight *fourLight = allLight[_textFieldFour.tag];
    _textFieldFour.text = fourLight.name;
    
    [[NSNotificationCenter defaultCenter] addObserverForName:kNotificationDeviceAllConnected object:nil queue:nil usingBlock:^(NSNotification * _Nonnull note) {
        NSLog(@"已经链接的light有%ld台", (long)[HNLCManager shareManager].store.connectedLightTotal);
        // 检查有几个light被链接，将链接的light的identifier加入_identifierStrings，达到同时控制的木的
        int total = [HNLCManager shareManager].store.connectedLightTotal;
        for (int i = 0; i < total; i++) {
            HNLight *light = [HNLCManager shareManager].store.allLights[i];
            [_identifierStrings addObject:light.identifier];
        }
    }];
#endif

}

#pragma mark - 亮度、颜色指令发送
- (IBAction)brightnessSliderValueDidChange:(UISlider *)sender {
    // 现在只有一台设备,就将key为0的light作为默认的链接设备
    // 多台设备，界面8个按钮的tag值0-7转为字符串作为key索引allLights字典(这样就可以将UI8个按钮对应得上8个硬件了)
    [[HNLCManager shareManager].manager setupLights:_identifierStrings brightnessValue:sender.value];
}

- (IBAction)sendYellowColor:(UIButton *)sender {
    [[HNLCManager shareManager].manager setupLights:_identifierStrings colourR:255 colourG:255 colourB:0];
}

- (IBAction)sendMagentaColor:(UIButton *)sender {
    [[HNLCManager shareManager].manager setupLights:_identifierStrings colourR:255 colourG:0 colourB:255];
}

- (IBAction)sendOrangeColor:(UIButton *)sender {
    [[HNLCManager shareManager].manager setupLights:_identifierStrings colourR:255 colourG:127 colourB:0];
}

- (IBAction)sendPurpleColor:(UIButton *)sender {
    [[HNLCManager shareManager].manager setupLights:_identifierStrings colourR:127 colourG:0 colourB:127];
}

- (IBAction)rSliderValueDidChanged:(UISlider *)sender {
    [[HNLCManager shareManager].manager setupLights:_identifierStrings colourR:sender.value colourG:_gSlider.value colourB:_bSlider.value];
}
- (IBAction)gSliderValueDidChanged:(UISlider *)sender {
    [[HNLCManager shareManager].manager setupLights:_identifierStrings colourR:_rSlider.value colourG:sender.value colourB:_bSlider.value];
}
- (IBAction)bSliderValueDidChanged:(UISlider *)sender {
    [[HNLCManager shareManager].manager setupLights:_identifierStrings colourR:_rSlider.value colourG:_gSlider.value colourB:sender.value];
}

- (IBAction)sendSyncCommand:(UIButton *)sender {
    [[HNLCManager shareManager].manager sendSyncCommand:_identifierStrings];
}

#pragma mark - 发送4个灯的开关等灯指令
// 开关
- (IBAction)switchLight:(UIButton *)sender {
    [self setupOnOff:sender];
}

// 选择灯是否被控制
- (IBAction)selectLight:(UISwitch *)sender {
    switch (sender.tag) {
        case 0:
            if (sender.on) {
                if (![_identifierStrings containsObject:[[HNLCManager shareManager].store.allLights[0] identifier]]) {
                    [_identifierStrings addObject:[[HNLCManager shareManager].store.allLights[0] identifier]];
                }
            } else {
                if ([_identifierStrings containsObject:[[HNLCManager shareManager].store.allLights[0] identifier]]) {
                    [_identifierStrings removeObject:[[HNLCManager shareManager].store.allLights[0] identifier]];
                }
            }
            break;
            
        case 1:
            if (sender.on) {
                if (![_identifierStrings containsObject:[[HNLCManager shareManager].store.allLights[1] identifier]]) {
                    [_identifierStrings addObject:[[HNLCManager shareManager].store.allLights[1] identifier]];
                }
            } else {
                if ([_identifierStrings containsObject:[[HNLCManager shareManager].store.allLights[1] identifier]]) {
                    [_identifierStrings removeObject:[[HNLCManager shareManager].store.allLights[1] identifier]];
                }
            }
            break;
            
        case 2:
            if (sender.on) {
                if (![_identifierStrings containsObject:[[HNLCManager shareManager].store.allLights[2] identifier]]) {
                    [_identifierStrings addObject:[[HNLCManager shareManager].store.allLights[2] identifier]];
                }
            } else {
                if ([_identifierStrings containsObject:[[HNLCManager shareManager].store.allLights[2] identifier]]) {
                    [_identifierStrings removeObject:[[HNLCManager shareManager].store.allLights[2] identifier]];
                }
            }
            break;
            
        case 3:
            if (sender.on) {
                if (![_identifierStrings containsObject:[[HNLCManager shareManager].store.allLights[3] identifier]]) {
                    [_identifierStrings addObject:[[HNLCManager shareManager].store.allLights[3] identifier]];
                }
            } else {
                if ([_identifierStrings containsObject:[[HNLCManager shareManager].store.allLights[3] identifier]]) {
                    [_identifierStrings removeObject:[[HNLCManager shareManager].store.allLights[3] identifier]];
                }
            }
            break;
            
        default:
            break;
    }
}

- (void)setupOnOff:(UIButton *)sender {
    sender.selected = !sender.selected;
    if (_identifierStrings.count == 0) {
        NSLog(@"没有选择设备，发送不了指令");
    } else {
        [[HNLCManager shareManager].manager setupLights:_identifierStrings state:sender.selected ? YES : NO];
    }
   
}

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == _enterNameTextFild) {
        // 保存数据
        HNLight *light = [HNLCManager shareManager].store.allLights[0];
        light.name = textField.text;
        
        HNLight *light1 = [HNLCManager shareManager].store.allLights[1];
        light1.name = textField.text;
        // 发送指令
        [[HNLCManager shareManager].manager setupNewnameForLights:_identifierStrings];
    } else {
        NSArray *allLight = [HNLCManager shareManager].store.allLights;
        HNLight *light = allLight[textField.tag];
        light.name = textField.text;
    }

    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    if (textField == _enterNameTextFild) {
    } else {
        NSArray *allLight = [HNLCManager shareManager].store.allLights;
        HNLight *light = allLight[textField.tag];
        light.name = textField.text;
    }
}

@end
