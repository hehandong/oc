//
//  HNPublic.h
//  Lighting Controller
//
//  Created by YuHeng_Antony on 12/7/15.
//  Copyright © 2015 Homni Electron Inc. All rights reserved.
//

#ifndef HNPublic_h
#define HNPublic_h


#endif /* HNPublic_h */

#define _INTERNAL_IUL11_USE_ 1
// 识别灯的位置(索引)
typedef NS_ENUM(NSUInteger, HN_LIGHT) {
    HN_LIGHT_TOP_LEFT,
    HN_LIGHT_TOP_CENTRE,
    HN_LIGHT_TOP_RIGHT,
    HN_LIGHT_MIDDLE_LEFT,
    HN_LIGHT_MIDDLE_CENTRE,
    HN_LIGHT_MIDDLE_RIGHT,
    HN_LIGHT_BOTTOM_LEFT,
    HN_LIGHT_BOTTOM_CENTRE,
};

#pragma mark - 硬件相关参数
// 扫描设备的时间
static NSTimeInterval scanTime = 1.5;

// 要連接設備的數量
static NSUInteger defaultConnectDeviceTotal = 4;

// 掃描不到設備時，再重新掃描的次數
static NSUInteger defaultScanDeviceTimesTotal = 3;

// 滑动亮度slider时发送指令的时间间隔(200ms发送一次)
static float intervalTimeForSlider = 0.2;

#pragma mark “服务”
// 通过此“服务”的UUID字符串，判断是我们要链接的设备
#if _INTERNAL_IUL11_USE_
// IUL11用于读写数据的"服务"UUID字符串:FF12
static NSString *kOurServiceUUIDString = @"FF12";

// IUL11名字前綴(用於判斷是不是我們的設備)
static NSString *kIUL11NamePrefix = @"HOMNI";

#else
// WLT2541(测试板)用于读写数据的"服务"UUID字符串:FF0
static NSString *kOurServiceUUIDString = @"FFF0";
#endif

#pragma mark “特征”
// IUL11回传数据到APP用到的「特徵」字符串FF02 (8个模块都一样)
static NSString *kCharacteristicDataOutUUIDString   = @"FF02";

#if _INTERNAL_IUL11_USE_
// IUL11用于数据写入的「特徵」字符串FF01 (8个模块都一样)
static NSString *kCharacteristicDataInUUIDString    = @"FF01";
#else
// WLT2541(测试板)用于写入数据(开关灯)的「特徵」UUIDString
static NSString *kCharacteristicDataInUUIDString = @"FFF1";
#endif

static NSString *kConnectedPeripheralsTotal = @"connectedPeripheralsTotal";

#pragma mark - HNLight对象若干属性初始值
static int kLightsTotal             = 8;
static int kGroupTotal              = 4;
static NSString *kLights            = @"lights";
static NSString *kDefaultIdentifier = @"00000000-0000-0000-0000-000000000000";
static uint8_t kDefultBrightness    = 255;
static uint8_t kDefultColorR        = 255;
static uint8_t kDefultColorG        = 255;
static uint8_t kDefultColorB        = 255;
static NSString *kDefultLightName   = @"Enter Name";
static int kDefultGroupIndex        = 4;// 表示没有被群组

static NSString *kGroups            = @"groups";

#pragma mark - 通知
// 扫描时间结束通知
static NSString *kNotificationScanTimeout             = @"com.Homni.LightingController.ScanTimeout";
// 所有设备连接上了，通知加载提示消失
static NSString *kNotificationDeviceAllConnected      = @"com.Homni.LightingController.DeviceAllConnected";
// 没有扫描到设备的通知
static NSString *kNotificationScanNonDevice           = @"com.Homni.LightingController.ScanNonDevice";
// 通知收到硬件数据，要更新界面了
static NSString *kNotificationReceiveDeviceData       = @"com.Homni.LightingController.ReceiveDeviceData";
// 通知:蓝牙模块名字被改了，要更新界面
static NSString *kNotificationPeripheralDidUpdateName = @"com.Homni.LightingController.PeripheralDidUpdateName";
// 通知:所有連接的模塊已經完成數據的同步
static NSString *kNotificationSyncDataDidCompleted    = @"com.Homni.LightingController.SyncDataDidCompleted";
// 通知:沒有掃描到足夠的設備
static NSString *kNotificationScanNotEnoughDevices    = @"com.Homni.LightingController.ScanNotEnoughDevices";
// 通知:重新掃描設備
static NSString *kNotificationReScaning               = @"com.Homni.LightingController.ReScaning";
// 通知:所有設備斷開了連接
static NSString *kNotificationDevicesAllDisConnected  = @"com.Homni.LightingController.DevicesAllDisConnected";