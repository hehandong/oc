//
//  HNCentralManager.h
//  Lighting Controller
//
//  Created by YuHeng_Antony on 12/3/15.
//  Copyright © 2015 Homni Electron Inc. All rights reserved.
//

/**
 *  Central:用于扫描并监听蓝牙模块设备
 */

#import <Foundation/Foundation.h>

@interface HNCentralManager : NSObject

/**
 *  直接開燈的接口方法(原來調整量度的要200毫秒發一次，所以將開關燈的指令分開)
 *
 *  @param lightIdentifiers 用于确定需要发送给哪些设备
 */
- (void)turnOnLights:(NSArray *)lightIdentifiers withBrihtnessValue:(uint8_t)brightnessValue;

/**
 *  直接關燈的接口方法(原來調整量度的要200毫秒發一次，所以將開關燈的指令分開)
 *
 *  @param lightIdentifiers 用于确定需要发送给哪些设备
 */
- (void)turnOffLights:(NSArray *)lightIdentifiers;

/**
 *  亮度指令接口方法
 *
 *  @param lightIdentifiers 用于确定需要发送给哪些设备
 *  @param brightnessValue  亮度值:0-255:0为灭,255为最亮
 */
- (void)setupLights:(NSArray *)lightIdentifiers brightnessValue:(uint8_t)brightnessValue;

/**
 *  顏色指令接口方法
 *
 *  @param lightIdentifiers 用于确定需要发送给哪些设备
 *  @param colourR          colourR R:0~255
 *  @param colourG          colourG G:0~255
 *  @param colourB          colourB B:0~255
 */
- (void)setupLights:(NSArray *)lightIdentifiers
            colourR:(uint8_t)colourR
            colourG:(uint8_t)colourG
            colourB:(uint8_t)colourB;

/**
 *  立即发送颜色指令(用于手指离开颜色选择器时发送指令)
 *
 *  @param lightIdentifiers 用于确定需要发送给哪些设备
 *  @param colourR          colourR R:0~255
 *  @param colourG          colourG G:0~255
 *  @param colourB          colourB B:0~255
 */
- (void)setupImmediatelyLights:(NSArray *)lightIdentifiers
            colourR:(uint8_t)colourR
            colourG:(uint8_t)colourG
            colourB:(uint8_t)colourB;

/**
 *  设置蓝牙模块名称接口方法
 *
 *  @param lightIdentifiers 用于确定需要发送给哪些设备(注意:新名称通过identifier从HNLight对象的name属性中拿,不需要传入)
 */
- (void)setupNewnameForLights:(NSArray *)lightIdentifiers;

/**
 *  帮助硬件保存数据(所有链接了的硬件都要upload)
 */
- (void)upload;

/**
 *  "擦除"所有硬件的数据(也就是颜色、亮度全部变回255)
 */
- (void)erase;

/**
 *  退出APP後,手動斷開藍牙連接
 */
- (void)disConnectDevice;

// TODO:新增了upload、erase方法，这个后续可以删除?
/**
 *  帮助硬件保存数据
 *
 *  @param lightIdentifiers 用于确定需要发送给哪些设备
 *  @param colourR          R
 *  @param colourG          G
 *  @param colourB          B
 *  @param brightnessValue  亮度
 */
- (void)setupLights:(NSArray *)lightIdentifiers
            colourR:(uint8_t)colourR
            colourG:(uint8_t)colourG
            colourB:(uint8_t)colourB
    brightnessValue:(uint8_t)brightnessValue;

/// TODO:此方法为临时用于测试同步指令的发送(后期可以删除)
- (void)sendSyncCommand:(NSArray *)lightIdentifiers;


// TODO:测试板WLT2541的开关灯指令(后期可以删除)
/**
 *  测试的开关灯指令
 *
 *  @param lightIdentifiers 用于确定需要发送给哪些设备
 *  @param open       开关状态
 */
- (void)setupLights:(NSArray *)lightIdentifiers state:(BOOL)open;

@end
