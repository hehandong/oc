//
//  HNEditViewController.swift
//  Lighting Controller
//
//  Created by YuHeng_Antony on 12/2/15.
//  Copyright © 2015 Homni Electron Inc. All rights reserved.
//

import UIKit


class HNEditViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate {
    
    @IBOutlet weak var tableView: UITableView!
 
    @IBOutlet var groupButtons: [UIButton]!
    
    @IBOutlet weak var saveButton: UIButton!
    
    @IBOutlet weak var cancelButton: UIButton!
    
    @IBOutlet weak var uploadButton: UIButton!
    
    @IBOutlet weak var eraseButton: UIButton!
    
    
    var textFieldAlertView:UIAlertView!
    
    var cell:HNTableViewCell!
    //MARK: cell 标识符
    let CellIdentifierNib = "cellNib"
    
    var currentGroupIndex:Int = 0  //初始化为群组1
    
    var oldLigthsNameDic = [Int:String]()

    var oldLigthsGroupIndexDic = [Int:Int]()
    
    var lightsOldData = [NSObject : AnyObject]()
    
    var identifierArray = [String]()
    
    var nameIdentifiers = [String!]()
    
    var selectedFirstTag:Int!
    
    var selectedCount:Int!
    
   
    

    override func viewWillAppear(animated: Bool) {
        print("-----------------------viewWillAppear")
        //重新加载
        tableView.reloadData()
        
        //保存一份旧数据，cancel时用
        var i:Int
        for i = 0; i < 8; ++i {
            oldLigthsNameDic[i] = light(i).name
            oldLigthsGroupIndexDic[i] = light(i).groupIndex
        }
        
        //切换组时,要让当前组内的灯处于被选择状态
        toggleLightButton()
        
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initData()
    }
    
    //数据初始化
    func initData(){
        
        NSBundle.mainBundle().loadNibNamed("HNEditViewController", owner: self, options: nil)
        
        tableView.registerNib(UINib(nibName: "HNTableViewCell", bundle: nil), forCellReuseIdentifier: "myCell")
        
        cell = tableView.dequeueReusableCellWithIdentifier("myCell") as! HNTableViewCell
        
        groupButtons[0].selected = true
        
    }

   
    // MARK: - 群组按钮点击事件
    @IBAction func groupEdit(sender: UIButton) {
        // TODO:切换组时,要让当前组内的灯处于被选择状态
        tableView.reloadData()
        
        setupButtonsState(groupButtons, sender: sender)

        if sender.selected {
        currentGroupIndex = sender.tag
        }
        
        for (index, light) in HNLCManager.shareManager().store.allLights.enumerate() {
            if (light as! HNLight).groupIndex == sender.tag {
                let indexPath = NSIndexPath(forRow: index, inSection: 0)
                tableView.selectRowAtIndexPath(indexPath, animated: false, scrollPosition: UITableViewScrollPosition.None)
                print("点亮\(index)")
            }
        }
        
    }
    
    
    //MARK: Helper Method: Group按钮互斥，只能有一个选中
    func setupButtonsState(buttons:[UIButton],sender:UIButton){
        sender.selected = true
        
        for i in buttons {
            if sender.selected {
                if !(i == sender) {
                    i.selected = false
                    
                }
            }
        }
    }

    // MARK: - 保存按钮点击事件
    @IBAction func save(sender: UIButton) {
        
        
        print("\(identifierStrings.count)")
        nameIdentifiers.removeAll()
        //检查是否改名字
        for (index, light) in HNLCManager.shareManager().store.allLights.enumerate() {
            if (light as! HNLight).isConnect {
                let indexPath:NSIndexPath = NSIndexPath(forRow: index, inSection: 0)
                let cell = tableView.cellForRowAtIndexPath(indexPath) as! HNTableViewCell
                if cell.lightName.text! != oldLigthsNameDic[index] {
                    nameIdentifiers.append(light.identifier)
                }
            }
        }
        
        //MAKR:-发送改名字指令
        if nameIdentifiers.count != 0 {
            HNLCManager.shareManager().manager.setupNewnameForLights(nameIdentifiers)
        }
        
        //获取选中灯中的第一个灯的index
        getSelectedLightIndex()
        
        if identifierStrings.count != 0{
           selectedCount = identifierStrings.count
        }
        
        
        //1.获取选中灯的组别
        //2.遍历所有灯，与选中灯组别相同的灯，改为选中状态
        //3.发指令更新硬件
        if identifierStrings.count != 0 && selectedFirstTag != nil{
            for (index, onelight) in HNLCManager.shareManager().store.allLights.enumerate() {
                if (onelight as! HNLight).isConnect {
                    if light(selectedFirstTag).groupIndex == (onelight as! HNLight).groupIndex{
                       (onelight as! HNLight).isSelected = true
                       (onelight as! HNLight).brightness = light(selectedFirstTag).brightness
                       (onelight as! HNLight).colorR = light(selectedFirstTag).colorR
                       (onelight as! HNLight).colorG = light(selectedFirstTag).colorG
                       (onelight as! HNLight).colorB = light(selectedFirstTag).colorB
                       identifierStrings[index] = (onelight as! HNLight).identifier
                    }
                }
            }
        }
        
        print("\(identifierStrings.count)")
        //判断是否有新灯加入群组
        if (selectedCount != nil) && (identifierStrings.count != selectedCount) && (selectedFirstTag != nil)  {
          
            DicToArray()
            //发送颜色指令
            HNLCManager.shareManager().manager.setupLights(identifierArray, colourR: light(selectedFirstTag).colorR, colourG: light(selectedFirstTag).colorG, colourB: light(selectedFirstTag).colorB)
            
            //发送亮度指令
            if light(selectedFirstTag).isOn{
               HNLCManager.shareManager().manager.setupLights(identifierArray, brightnessValue: light(selectedFirstTag).brightness)
            }else{
               HNLCManager.shareManager().manager.setupLights(identifierArray, brightnessValue: 0)
            }
            
        
        }
       
        self.dismissViewControllerAnimated(true, completion: nil)

    }
    
    // MARK: - 取消按钮点击事件
    @IBAction func cancel(sender: UIButton) {
        
        //还原旧数据
        var i:Int
        for i = 0; i < 8; ++i {
            light(i).name = oldLigthsNameDic[i]
            light(i).groupIndex = oldLigthsGroupIndexDic[i]!
        }
        
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    //更新硬件颜色亮度
    @IBAction func upload(sender: UIButton) {
//        getSelectedLightIndex()
//        DicToArray()
//        HNLCManager.shareManager().manager.setupLights(identifierArray, colourR: light(selectedFirstTag).colorR, colourG: light(selectedFirstTag).colorG, colourB: light(selectedFirstTag).colorB, brightnessValue: light(selectedFirstTag).brightness)
        
        HNLCManager.shareManager().manager.upload()
        
    }
    
    //擦写硬件颜色亮度
    @IBAction func erase(sender: UIButton) {
        //发送指令
//        DicToArray()
//        HNLCManager.shareManager().manager.setupLights(identifierArray, colourR: 255, colourG: 255, colourB: 255, brightnessValue: 255)
        
        HNLCManager.shareManager().manager.erase()
        
        //更新UI
        for light in HNLCManager.shareManager().store.allLights{
            if (light as! HNLight).isConnect{
               (light as! HNLight).colorR = 255
               (light as! HNLight).colorG = 255
               (light as! HNLight).colorB = 255
            }
        }
        
        tableView.reloadData()
        
        //切换组时,要让当前组内的灯处于被选择状态
        toggleLightButton()
        
    }
    
    
    
    // MARK: - UITableViewDataSource
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 8
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        cell = tableView.dequeueReusableCellWithIdentifier("myCell", forIndexPath: indexPath) as! HNTableViewCell
        
        cell.lightName.tag = indexPath.row
        cell.lightName.delegate = self
        
        
        cell.lightColorView.backgroundColor = UIColor(red: CGFloat(Double(light(indexPath.row).colorR)/255.0), green: CGFloat(Double(light(indexPath.row).colorG)/255.0), blue: CGFloat(Double(light(indexPath.row).colorB)/255.0), alpha: 1.0)
        
        cell.lightName.text = light(indexPath.row).name
        
        //没有连接的设备不能有触控事件
        cell.userInteractionEnabled = light(indexPath.row).isConnect
        
        switch light(indexPath.row).groupIndex {
        case 0:
            cell.lightGroupImageView.image = UIImage(named: "box_group1")
        case 1:
            cell.lightGroupImageView.image = UIImage(named: "box_group2")
        case 2:
            cell.lightGroupImageView.image = UIImage(named: "box_group3")
        case 3:
            cell.lightGroupImageView.image = UIImage(named: "box_group4")
        default:
            cell.lightGroupImageView.image = UIImage(named: "box_empty")
        }
        
//      print("cell的高度是\(cell.frame.height),cell的宽度是\( cell.frame.width)")
        
        return cell
    
    }
    
    // MARK: - UITableViewDelegate
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return tableView.frame.size.height / 8
    }
    
    
    //MARK:-tableView选中
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        let selectedCell = tableView.cellForRowAtIndexPath(indexPath) as! HNTableViewCell
//      selectedCell.setSelectedBackground()
        
        light(indexPath.row).groupIndex = currentGroupIndex
        
        switch currentGroupIndex {
        case 0:  
          selectedCell.lightGroupImageView.image = UIImage(named: "box_group1")
        case 1:
          selectedCell.lightGroupImageView.image = UIImage(named: "box_group2")
        case 2:
          selectedCell.lightGroupImageView.image = UIImage(named: "box_group3")
        case 3:
          selectedCell.lightGroupImageView.image = UIImage(named: "box_group4")
        default:
          selectedCell.lightGroupImageView.image = UIImage(named: "box_empty")
        }
        
        selectedCell.lightColorView.backgroundColor = UIColor(red: CGFloat(Double(light(indexPath.row).colorR)/255.0), green: CGFloat(Double(light(indexPath.row).colorG)/255.0), blue: CGFloat(Double(light(indexPath.row).colorB)/255.0), alpha: 1.0)
        
        print("选中了\(indexPath.row)")
    }
    
    //MARK:-tableView取消选中
    func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
        
        light(indexPath.row).groupIndex = 4 //4为没有群组
        let cell = tableView.cellForRowAtIndexPath(indexPath) as! HNTableViewCell
        cell.lightGroupImageView.image = UIImage(named: "box_empty")
        cell.lightColorView.backgroundColor = UIColor(red: CGFloat(Double(light(indexPath.row).colorR)/255.0), green: CGFloat(Double(light(indexPath.row).colorG)/255.0), blue: CGFloat(Double(light(indexPath.row).colorB)/255.0), alpha: 1.0)
        
        print("取消中\(indexPath.row)")
    }
    
   
    // MARK: - UITextFieldDelegate
    func textFieldDidEndEditing(textField: UITextField) {
        // 没有按下Done,在textField之间切换,也要保存name
        light(textField.tag).name = textField.text
        print("调用textFieldDidEndEditing")
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        // 保存新名字都数据模型
        light(textField.tag).name = textField.text
        textField.resignFirstResponder()
        print("调用textFieldShouldReturn")
        return true
    }
    
    //MARK:-判断是否超过15个字节
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {

        let name :NSString = (textField.text! as NSString).stringByReplacingCharactersInRange(range, withString: string)
        
         if name.length > 15{
            if (textFieldAlertView == nil){
                textFieldAlertView = UIAlertView()
                textFieldAlertView.title = "NOTE"
                textFieldAlertView.message = "You can not type beyond 15 characters"
                textFieldAlertView.addButtonWithTitle("OK")
            }
            textFieldAlertView.show()
            
          return false
       }
        
        return true
    }
    
    //MARK：-字典转数组
    func DicToArray(){
        identifierArray.removeAll()
        if identifierStrings.count != 0{
            for (_,identifier) in identifierStrings{
                identifierArray.append(identifier)
            }
        }
    }
    
    //MARK:-获取选中灯中的第一个灯的index
    func getSelectedLightIndex(){
        
        var flag:Bool = true
        if identifierStrings.count != 0{
            for (key,_) in identifierStrings{
                if flag {
                   selectedFirstTag = key
                   flag = false
                }
            }
        }
    }
    //MARK-切换组时,要让当前组内的灯处于被选择状态
    func toggleLightButton(){
        
        for groupButton in groupButtons{
            if groupButton.selected{
                for (index, light) in HNLCManager.shareManager().store.allLights.enumerate() {
                    if (light as! HNLight).groupIndex == groupButton.tag {
                        let indexPath = NSIndexPath(forRow: index, inSection: 0)
                        tableView.selectRowAtIndexPath(indexPath, animated: false, scrollPosition: UITableViewScrollPosition.None)
                    }
                }
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
