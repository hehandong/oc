//
//  HNLightingControllerStore.m
//  Lighting Controller
//
//  Created by YuHeng_Antony on 12/7/15.
//  Copyright © 2015 Homni Electron Inc. All rights reserved.
//

#import "HNLightingControllerStore.h"
#import "HNPublic.h"
#import "HNLight.h"
#import "HNGroup.h"

@interface HNLightingControllerStore ()
@property (copy, nonatomic) NSDictionary *privateAllDatas;
@property (copy, nonatomic) NSMutableArray *privateLights;
@property (copy, nonatomic) NSMutableArray *privateGroups;
@end

@implementation HNLightingControllerStore

+ (instancetype)shareStore {
    static id shareStore = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shareStore = [[self alloc] init];
    });
    return shareStore;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        // 从沙盒拿回数据
        _privateAllDatas = [NSKeyedUnarchiver unarchiveObjectWithFile:[self dataArchivePath]];
        if (_privateAllDatas) {
//            NSLog(@"拿回了数据:_privateAllDatas:%@", _privateAllDatas);
            // TODO:是否需要将light的isOn统一设置为NO？因为要在链接成功后才能设置为YES
        }
        
        // 创建8个灯,4个group
        if (!_privateAllDatas) {
            _privateLights = [[NSMutableArray alloc] init];
            for (int i = 0; i < kLightsTotal; i++) {
                HNLight *light = [[HNLight alloc] init];
                [_privateLights addObject:light];
            }
            _privateGroups = [[NSMutableArray alloc] init];
            for (int i = 0; i < kGroupTotal; i++) {
                HNGroup *group = [[HNGroup alloc] init];
                group.groupTag = i;
                [_privateGroups addObject:group];
            }
            _privateAllDatas = @{kLights:_privateLights, kGroups:_privateGroups};
//            NSLog(@"创建了数据:_privateAllDatas:%@", _privateAllDatas);
        }
    }
    return self;
}

- (NSString *)dataArchivePath
{
    NSArray *documentDirectories = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDirectory = [documentDirectories firstObject];
    return [documentDirectory stringByAppendingPathComponent:@"lightingControllerData.archive"];
}

- (BOOL)saveData {
    NSLog(@"保存了数据");
    return [NSKeyedArchiver archiveRootObject:_privateAllDatas toFile:[self dataArchivePath]];
}

- (NSArray *)allLights {
    return [_privateAllDatas[kLights] copy];
}

- (int)connectedLightTotal {
    _connectedLightTotal = 0;
    [_privateAllDatas[kLights] enumerateObjectsUsingBlock:^(HNLight  *_Nonnull light, NSUInteger idx, BOOL * _Nonnull stop) {
        if (light.isConnect) {
            _connectedLightTotal++;
        }
    }];
    return _connectedLightTotal;
}

- (NSArray *)allGroups {
    return [_privateAllDatas[kGroups] copy];
}

@end
