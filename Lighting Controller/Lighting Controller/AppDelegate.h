//
//  AppDelegate.h
//  Lighting Controller
//
//  Created by YuHeng_Antony on 11/30/15.
//  Copyright © 2015 Homni Electron Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

