//
//  HNLCManager.h
//  Lighting Controller
//
//  Created by YuHeng_Antony on 12/3/15.
//  Copyright © 2015 Homni Electron Inc. All rights reserved.
//

/**
 *  此单例用作CentralManager的代理
 */

#import <Foundation/Foundation.h>
@class HNCentralManager;
@class HNLightingControllerStore;

@interface HNLCManager : NSObject

@property (strong, nonatomic) HNCentralManager *manager;

@property (strong, nonatomic) HNLightingControllerStore *store;

+ (instancetype)shareManager;

@end
