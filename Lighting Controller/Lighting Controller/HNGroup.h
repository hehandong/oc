//
//  HNGroup.h
//  Lighting Controller
//
//  Created by Antony on 15/12/6.
//  Copyright © 2015年 Homni Electron Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HNGroup : NSObject<NSCoding>

@property (copy, nonatomic) NSArray *lights;

@property (nonatomic) BOOL isEmpty;

@property (nonatomic) BOOL isSelected;

@property (nonatomic) NSInteger groupTag;
@end
