//
//  HNLCManager.m
//  Lighting Controller
//
//  Created by YuHeng_Antony on 12/3/15.
//  Copyright © 2015 Homni Electron Inc. All rights reserved.
//

#import "HNLCManager.h"
#import "HNCentralManager.h"
#import "HNLightingControllerStore.h"

@implementation HNLCManager

+ (instancetype)shareManager {
    static id shareManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shareManager = [[[self class] alloc] init];
    });
    return shareManager;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        _manager = [[HNCentralManager alloc] init];
        _store   = [[HNLightingControllerStore alloc] init];
    }
    return self;
}

@end
