//
//  HNDataStorage.swift
//  Lighting Controller
//
//  Created by homni_rd01 on 15/12/10.
//  Copyright © 2015年 Homni Electron Inc. All rights reserved.
//

import Foundation
import UIKit

func light(tag:Int)->HNLight{
    return HNLCManager.shareManager().store.allLights![tag] as! HNLight
}
//identifier
func saveLightIdentifier(tag:Int,identifier:String){
    (HNLCManager.shareManager().store.allLights![tag] as! HNLight).identifier = identifier
}
//isConnect
func saveLightConnected(tag:Int,isConnect:Bool){
    (HNLCManager.shareManager().store.allLights![tag] as! HNLight).isConnect = isConnect
}

//func saveLightColor(tag:Int,color:UIColor){
//    (HNLCManager.shareManager().store.allLights![String(tag)] as! HNLight).color = color
//}

func saveLightBrightness(tag:Int,brightness:UInt8){
    (HNLCManager.shareManager().store.allLights![tag] as! HNLight).brightness = brightness
}

func saveLightColorR(tag:Int,colorR:UInt8){
    (HNLCManager.shareManager().store.allLights![tag] as! HNLight).colorR = colorR
}

func saveLightColorG(tag:Int,colorG:UInt8){
    (HNLCManager.shareManager().store.allLights![tag] as! HNLight).colorG = colorG
}

func saveLightColorB(tag:Int,colorB:UInt8){
    (HNLCManager.shareManager().store.allLights![tag] as! HNLight).colorB = colorB
}



func saveLightName(tag:Int,name:String){
    (HNLCManager.shareManager().store.allLights![tag] as! HNLight).name = name
}

func saveLightSelected(tag:Int,isSelected:Bool){
    (HNLCManager.shareManager().store.allLights![tag] as! HNLight).isSelected = isSelected
}

func saveLightIsGrouped(tag:Int,isGrouped:Bool){
    (HNLCManager.shareManager().store.allLights![tag] as! HNLight).isGrouped = isGrouped
}


func saveLightGroupIndex(tag:Int,groupIndex:Int){
    (HNLCManager.shareManager().store.allLights![tag] as! HNLight).groupIndex = groupIndex
}

func saveLightIsOn(tag:Int,isOn:Bool){
    (HNLCManager.shareManager().store.allLights![tag] as! HNLight).isOn = isOn
}








