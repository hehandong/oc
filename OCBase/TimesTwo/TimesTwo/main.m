//
//  main.m
//  TimesTwo
//
//  Created by homni_rd01 on 16/2/23.
//  Copyright © 2016年 Homni Electron Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        // insert code here...
//        NSLog(@"Hello, World!");
        NSDate *currentTime = [NSDate date];
        NSLog(@"currentTime value is %p",currentTime);
        NSDate *startTime = currentTime;
        
        sleep(2);
        currentTime = [NSDate date];
//        NSLog(@"currentTime value is %p",currentTime);
//        NSLog(@"The address of the original object is %p",startTime);
        
        currentTime = nil;
        NSLog(@"currenTime value is %p",currentTime);
    }
    
    return 0;
}
