//
//  AppDelegate.m
//  BMITime3
//
//  Created by homni_rd01 on 16/2/23.
//  Copyright © 2016年 Homni Electron Inc. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    // Insert code here to initialize your application
}

- (void)applicationWillTerminate:(NSNotification *)aNotification {
    // Insert code here to tear down your application
}

@end
