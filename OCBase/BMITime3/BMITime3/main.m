//
//  main.m
//  BMITime3
//
//  Created by homni_rd01 on 16/2/23.
//  Copyright © 2016年 Homni Electron Inc. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "BNRPerson.h"

int main(int argc, const char * argv[]) {
    
    BNRPerson *mikey = [[BNRPerson alloc] init];
    
    [mikey setWeightInkilos:96];
    [mikey setHeightInMeters:1.8];
    
    float height = [mikey heightInMeters];
    int weight = [mikey weightInkilos];
    NSLog(@"mikey is %.2f meters tall and weighs %d kilograms",height,weight);
    
    float bmi = [mikey bodyMassIndex];
    NSLog(@"mikey has a BMI of %f",bmi);
    
    return NSApplicationMain(argc, argv);
}
