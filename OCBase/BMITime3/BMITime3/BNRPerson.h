//
//  BNRPerson.h
//  BMITime3
//
//  Created by homni_rd01 on 16/2/23.
//  Copyright © 2016年 Homni Electron Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BNRPerson : NSObject

@property (nonatomic) float heightInMeters;
@property (nonatomic) int weightInkilos;

//{
//    
////    float _heightInMeters;
////    int _weightInKilos;
//    
//}

//- (float)heightInMeters;
//- (void)setHeightInMeters:(float)h;
//- (int)weightInkilos;
//- (void)setWeightInkilos:(int)w;

- (float)bodyMassIndex;
@end
