//
//  BNRPerson.m
//  BMITime3
//
//  Created by homni_rd01 on 16/2/23.
//  Copyright © 2016年 Homni Electron Inc. All rights reserved.
//

#import "BNRPerson.h"

@implementation BNRPerson

//- (float)heightInMeters
//{
//    return _heightInMeters;
//}
//
//- (void)setHeightInMeters:(float)h
//{
//    _heightInMeters = h;
//}
//
//- (int)weightInkilos
//{
//    return _weightInKilos;
//}
//
//- (void)setWeightInkilos:(int)w
//{
//    _weightInKilos = w;
//}
//

- (float)bodyMassIndex
{

//  return _weightInKilos / (_heightInMeters * _heightInMeters);
    float h = [self heightInMeters];
    return ([self weightInkilos] / (h * h));
}

@end
