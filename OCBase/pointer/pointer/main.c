//
//  main.c
//  pointer
//
//  Created by homni_rd01 on 16/2/23.
//  Copyright © 2016年 Homni Electron Inc. All rights reserved.
//

#include <stdio.h>

int main(int argc, const char * argv[]) {
    // insert code here...
    int i = 17;
    int *addressOfI = &i;
    
    printf("i stores its value at %p\n",addressOfI);
//    printf("this function starts at %p\n",main);
//    printf("this int stored at addressofI is %d\n",*addressOfI);
    
    *addressOfI = 89;
    printf("Now i is %d\n",i);
    printf("An int is %zu bytes\n",sizeof(int));
    printf("A pointer is %zu bytes\n",sizeof(addressOfI));
    
    
    
    return 0;
}
