//
//  main.m
//  TimeAfterTime
//
//  Created by homni_rd01 on 16/2/23.
//  Copyright © 2016年 Homni Electron Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        NSDate *now = [[NSDate alloc] init];
        NSLog(@"This NSDate object lives at %p",now);
        NSLog(@"Tht date is %@",now);
        
        double seconds = [now timeIntervalSince1970];
        NSLog(@"It has been %f seconds since the start of 1970",seconds);
        
//        double testSeconds = [NSDate timeIntervalSince1970];
//        NSDate *testNow = [now date];
        NSDate *latet = [now dateByAddingTimeInterval:10000];
        NSLog(@"In 10000 seconds it will be %@",latet);
        
        NSCalendar *cal = [NSCalendar currentCalendar];
        NSLog(@"My calendar is %@",[cal calendarIdentifier]);
        
        unsigned long day = [cal ordinalityOfUnit:NSDayCalendarUnit
                                           inUnit:NSMonthCalendarUnit
                                          forDate:now];
        
        NSLog(@"This is day %lu of the month",day);
        
    }
    
    return 0;
}
