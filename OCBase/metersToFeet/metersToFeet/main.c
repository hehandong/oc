//
//  main.c
//  metersToFeet
//
//  Created by homni_rd01 on 16/2/23.
//  Copyright © 2016年 Homni Electron Inc. All rights reserved.
//

#include <stdio.h>
#include <math.h>

void meterToFeetAndInches(double meters,unsigned int *ftPtr,double *inPtr)
{
    double rawFeet = meters * 3.281;
    unsigned int feet = (unsigned int)floor(rawFeet);
    
    if (ftPtr) {
        printf("Storing %u to hte address %p\n",feet,ftPtr);
        *ftPtr = feet;
    }
    
    
    double fractionalFoot = rawFeet - feet;
    double inches = fractionalFoot * 12;
    if (inPtr) {
        printf("Storing %.2f to the address %p\n",inches,inPtr);
        *inPtr = inches;
    }
    
}

int main(int argc, const char * argv[]) {
    // insert code here...
    
    double meters = 3.0;
    unsigned int feet;
    double inches;
    meterToFeetAndInches(meters, &feet, &inches);
    printf("%.1f meters is equal to %d feet and %.1f inches.",meters,feet,inches);
    return 0;
}
