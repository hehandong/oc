//
//  ViewController.m
//  SocketDemo
//
//  Created by homni_rd01 on 16/3/16.
//  Copyright © 2016年 Homni Electron Inc. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () <UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *content;
@property (nonatomic,copy) NSString *sendMsgString;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _content.delegate =self;
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
   
        _sendMsgString = textField.text;
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
      _sendMsgString = textField.text;
    
    [textField resignFirstResponder];
    
   
    return YES;
}

- (IBAction)sendMsg:(id)sender {
    
    NSLog(@"发送的信息是%@",_sendMsgString);
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
