//
//  AppDelegate.h
//  SocketDemo
//
//  Created by homni_rd01 on 16/3/16.
//  Copyright © 2016年 Homni Electron Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

