//
//  TextFieldViewController.h
//  LessonBlockValue
//
//  Created by lanou3g on 14-7-14.
//  Copyright (c) 2014年 Winann. All rights reserved.
//

#import <UIKit/UIKit.h>
//没有返回参数的
typedef void (^ReturnTextBlock)(NSString *showText);

@interface TextFieldViewController : UIViewController

@property (nonatomic, copy) ReturnTextBlock returnTextBlock;

- (void)returnText:(ReturnTextBlock)block;

@end
